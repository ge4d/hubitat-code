
# Hubitat Drivers

All drivers and applications in this repo are free for use. 

## Notes

All drivers depend on libraries to a different degree. **<h3>The whole bundle is [here](https://bitbucket.org/ge4d/hubitat-code/src/main/Bundles/Bundle1.zip).</h3>**

**It is highly recommended to install the whole bundle. It is a lot simpler and does not require messing with libraries and drivers one by one.
Just take the bundle and import it.** There are no conflicts expected as all the files are guarded by the namespace.

## Drivers

Vendor        | Device | Status
---           | ---    | ---
Aeotec        | [Smart Switch 7 (F-plug)](https://bitbucket.org/ge4d/hubitat-code/src/main/Drivers/Aeotec/drozovyk.AeotecSmartSwitch7Fplug.groovy) | Ready to use
Aeotec        | [Dual Nano Switch (ZW132/ZW140)](https://bitbucket.org/ge4d/hubitat-code/src/main/Drivers/Aeotec/drozovyk.AeotecDualNanoSwitchZW132ZW140.groovy) | Ready to use
              | - depends on [Generic Component Metered Switch](https://bitbucket.org/ge4d/hubitat-code/src/main/Drivers/Virtual/drozovyk.GenericComponentMeteredSwitch.groovy) for child devices |
Fibaro        | [Heat Controller (FGT-001 v4.7)](https://bitbucket.org/ge4d/hubitat-code/src/main/Drivers/Fibaro/drozovyk.FibaroTRVFGT0014.7.groovy) | Ready to use
              | - depends on [Virtual battery powered device](https://bitbucket.org/ge4d/hubitat-code/src/main/Drivers/Virtual/drozovyk.VirtualBatteryPoweredDevice.groovy) for external sensor |
Fibaro        | [RGBW Controller 2 (FGRGBW-442)] | Work in progress
Heatit        | [Thermostat (Z-TRM3)](https://bitbucket.org/ge4d/hubitat-code/src/main/Drivers/Heatit/drozovyk.HeatitThermostatZTRM3.groovy) | Ready to use
Shelly        | [Plus 1, Plus 1 PM, Plus 2 PM, Plus I4, Plus Plug Plug (IT, S, UK, US), Pro 1, Pro 1 PM, Pro 2, Pro 2 PM, Pro 3, Pro 3 EM, Pro 4 PM](https://bitbucket.org/ge4d/hubitat-code/src/main/Drivers/Shelly/drozovyk.ShellyPlusProxPM.groovy) | Ready to use
              | - depends on [Generic Component Energy Meter](https://bitbucket.org/ge4d/hubitat-code/src/main/Drivers/Virtual/drozovyk.GenericComponentEnergyMeter.groovy) for child devices of Pro 3 EM |
              | - depends on [Generic Component Illuminance Sensor](https://bitbucket.org/ge4d/hubitat-code/src/main/Drivers/Virtual/drozovyk.GenericComponentIlluminanceSensor.groovy) for child devices of Wall Display |
              | - depends on [Generic Component Input Event](https://bitbucket.org/ge4d/hubitat-code/src/main/Drivers/Virtual/drozovyk.GenericComponentInputEvent.groovy) for child devices |
              | - depends on [Generic Component Metered Switch](https://bitbucket.org/ge4d/hubitat-code/src/main/Drivers/Virtual/drozovyk.GenericComponentMeteredSwitch.groovy) for child devices |
              | - depends on [Generic Component Metered Window Shade](https://bitbucket.org/ge4d/hubitat-code/src/main/Drivers/Virtual/drozovyk.GenericComponentMeteredWindowShade.groovy) for child devices of Plus/Pro 2 PM |
              | - depends on [Generic Component Script](https://bitbucket.org/ge4d/hubitat-code/src/main/Drivers/Virtual/drozovyk.GenericComponentScript.groovy) for child devices |
              

## License

Everithing in this repository is licensed under the MIT License.
