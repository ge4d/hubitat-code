metadata {
    definition (name: "Virtual Battery Powered Device", namespace: "drozovyk", author: "Dmytro Rozovyk") {
        capability("Battery")
        capability("PowerSource")               // powerSource - ENUM ["battery", "dc", "mains", "unknown"]
        
        attribute("batteryState", "STRING")     // low, empty, discharging, charging, idle
        attribute("batteryVoltage", "NUMBER")
    }
    preferences {
        input(name: "logEnable", type: "bool", title: "Enable logging", defaultValue: false)
    }
}

void installed() {
    parse([
        [name: "battery",        value: "unknown",     descriptionText: "Initial value"],
        [name: "batteryState",   value: "discharging", descriptionText: "Initial value"],
        [name: "batteryVoltage", value: "unknown",     descriptionText: "Initial value"],
        [name: "powerSource",    value: "battery",     descriptionText: "Initial value"]
    ])
}

void parse(String description) { 
    log.warn "Virtual Battery Powered Device: parse(String description) not implemented." 
}

void parse(List<Map> description) {
    description.each {
        if (logEnable && it.descriptionText) {
            log.info it.descriptionText
        }
        sendEvent(it)
    }
}