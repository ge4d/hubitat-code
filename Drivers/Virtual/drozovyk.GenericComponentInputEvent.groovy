metadata {
    definition(name: "Generic Component Input Event", namespace: "drozovyk", author: "Dmytro Rozovyk", component: true) {
        capability("ContactSensor")
        capability("Refresh")
        capability("Sensor")
        
        attribute("event", "STRING")       // "btn_down", "btn_up", "single_push", "double_push", "triple_push", "long_push"
        
        capability("DoubleTapableButton")  // doubleTapped
        capability("HoldableButton")       // held
        capability("PushableButton")       // numberOfButtons, pushed
        capability("ReleasableButton")     // released
        
        attribute("frequency",          "NUMBER")
        attribute("frequencyEvaluated", "NUMBER")
        attribute("level",              "NUMBER")
        attribute("levelEvaluated",     "NUMBER")
        attribute("pulses",             "NUMBER")
        attribute("pulsesEvaluated",    "NUMBER")
        
        attribute("healthStatus","ENUM", ["offline", "online"]) // basically a "link" attribute with shorter options list
    }
    preferences {
        input(name: "logEnable", type: "bool", title: "Enable logging", defaultValue: false)
    }
}

void updated() {
    if(logEnable) {
        log.info("Generic Component Input Event: Updated.")
    }
}

void installed() {
    if(logEnable) {
        log.info "Generic Component Input Event: Installed."
    }
    
    device.updateSetting("logEnable", [type: "bool", value: false])
    
    parse([
        [name: "contact",            value: "n/a", descriptionText: "Initial value"],
        [name: "doubleTapped",       value: 1,     descriptionText: "Initial value"],
        [name: "held",               value: 1,     descriptionText: "Initial value"],
        [name: "numberOfButtons",    value: 4,     descriptionText: "Initial value"],
        [name: "pushed",             value: 1,     descriptionText: "Initial value"],
        [name: "released",           value: 1,     descriptionText: "Initial value"],
        [name: "event",              value: "n/a", descriptionText: "Initial value"],        
        [name: "frequency",          value: 0,     descriptionText: "Initial value"],
        [name: "frequencyEvaluated", value: "n/a", descriptionText: "Initial value"],
        [name: "level",              value: 0,     descriptionText: "Initial value"],
        [name: "levelEvaluated",     value: "n/a", descriptionText: "Initial value"],
        [name: "pulses",             value: 0,     descriptionText: "Initial value"],
        [name: "pulsesEvaluated",    value: "n/a", descriptionText: "Initial value"],
    ])
}

void parse(String description) { 
    log.warn "Generic Component Input Event: parse(String description) not implemented." 
}

void parse(List<Map> description) {
    description.each {
        if (logEnable && it.descriptionText) {
            log.info it.descriptionText
        }
        sendEvent(it)
    }
}

void doubleTap(buttonNumber) {
    parse([
        [name: "doubleTapped", value: 1, descriptionText: "Button was tapped twice [UI]", isStateChange: true]
    ])
}

void hold(buttonNumber) {
    parse([
        [name: "held", value: 1, descriptionText: "Button is held [UI]", isStateChange: true]
    ])
}

void push(buttonNumber) {
    parse([
        [name: "pushed", value: 1, descriptionText: "Button was pushed [UI]", isStateChange: true]
    ])
}

void release(buttonNumber) {
    parse([
        [name: "released", value: 1, descriptionText: "Button was released [UI]", isStateChange: true]
    ])
}

void refresh() {
    parent?.componentRefresh(this.device)
}