import groovy.transform.Field

@Field static parameterDescription = [
    (0):   [title: "Current threshold (A)",
            help: "",
            style: "color:red; font-weight:bold;"],
    (1):   [title: "Energy threshold (kWh)",
            help: "",
            style: "color:green; font-weight:bold;"],
    (2):   [title: "Frequency threshold (Hz)",
            help: "",
            style: "color:slateblue;font-weight:bold;"],
    (3):   [title: "Power threshold (W)",
            help: "",
            style: "color:green; font-weight:bold;"],
    (4):   [title: "Voltage threshold (V)",
            help: "",
            style: "color:slateblue;font-weight:bold;"]
]

static def getParameterDesc(int parameter) {
    def parameterDesc = parameterDescription[parameter]
    
    if(null == parameterDesc) {
        return [title: "", help: "", style: ""]
    }
    
    return parameterDesc
}

static String getParameterHelp(int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return parameterDesc.help
}

static String styleText(String text, String style = "") {
    return "<span style='${style}'>${text}</span>"
}

static String decorateString(String str, int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return styleText(str, parameterDesc.style)
}

static String getParameterTitle(int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return styleText(parameterDesc.title, parameterDesc.style)
}

metadata {
    definition(name: "Generic Component Metered Dimmer", namespace: "drozovyk", author: "Dmytro Rozovyk", component: true) {
        capability "Actuator"
        capability "CurrentMeter"           // add 'amperage' attribute
        capability "EnergyMeter"            // add 'energy' attribute
        capability "PowerMeter"             // add 'power' attribute
        capability "Refresh"
        capability "Switch"                 // add 'switch' attribute and 'on','off' commands
        capability "SwitchLevel"            // add 'level' attribute and 'setLevel' command
        capability "VoltageMeasurement"     // add 'voltage' and 'frequency' attributes
        
        attribute("healthStatus","ENUM", ["offline", "online"]) // basically a "link" attribute with shorter options list
        
        input(name: "amperageThreshold",    type: "number", title:getParameterTitle(0), description: getParameterHelp(0), range: "0.0..10.0",   defaultValue: "0.0", required: false, displayDuringSetup: false)
        input(name: "energyThreshold",      type: "number", title:getParameterTitle(1), description: getParameterHelp(1), range: "0.0..10.0",   defaultValue: "0.0", required: false, displayDuringSetup: false)
        input(name: "frequencyThreshold",   type: "number", title:getParameterTitle(2), description: getParameterHelp(2), range: "0.0..1.0",    defaultValue: "0.0", required: false, displayDuringSetup: false)
        input(name: "powerThreshold",       type: "number", title:getParameterTitle(3), description: getParameterHelp(3), range: "0.0..1000.0", defaultValue: "0.0", required: false, displayDuringSetup: false)
        input(name: "voltageThreshold",     type: "number", title:getParameterTitle(4), description: getParameterHelp(4), range: "0.0..20.0",   defaultValue: "0.0", required: false, displayDuringSetup: false)
    }
    preferences {
        input(name: "logEnable", type: "bool", title: "Enable logging", defaultValue: false)
    }
}

void updated() {
    if(logEnable) {
        log.info "Generic Component Metered Dimmer: Updated."
    }
}

void installed() {
    if(logEnable) {
        log.info "Generic Component Metered Dimmer: Installed."
    }
    
    device.updateSetting("logEnable", [type: "bool", value: false])
    
    parse([
        [name: "amperage", value: "0.0",     descriptionText: "Initial value"],
        [name: "energy",   value: "0.0",     descriptionText: "Initial value"],
        [name: "frequency",value: "0.0",     descriptionText: "Initial value"],
        [name: "power",    value: "0.0",     descriptionText: "Initial value"],
        [name: "voltage",  value: "0.0",     descriptionText: "Initial value"],
        [name: "level",    value: "unknown", descriptionText: "Initial value"],
        [name: "switch",   value: "unknown", descriptionText: "Initial value"]
    ])
}

void parse(String description) { 
    log.warn "Generic Component Metered Dimmer: parse(String description) not implemented." 
}

void parse(List<Map> description) {
    description.each {
        def lastValue = null
        def threshold = null
        
        if(it.name == "amperage") {
            lastValue = device.currentValue("amperage")
            threshold = amperageThreshold
        }
        else if(it.name == "energy") {
            lastValue = device.currentValue("energy")
            threshold = energyThreshold
        }
        else if(it.name == "frequency") {
            lastValue = device.currentValue("frequency")
            threshold = frequencyThreshold
        }
        else if(it.name == "power") {
            lastValue = device.currentValue("power")
            threshold = powerThreshold
        }
        else if(it.name == "voltage") {
            lastValue = device.currentValue("voltage")
            threshold = voltageThreshold
        }

        // if previus value and threshold are available we can test and skip annoing event spam
        if(null != lastValue && null != threshold) {
            def delta = Math.abs(it.value - lastValue)
            if(delta < Float.valueOf(threshold)) {
                if (logEnable) {
                    log.info("Generic Component Metered Dimmer: The " + it.name + " change is below threshold. The ${it.name} threshold is ${threshold} but the change is ${delta}; skipping event")
                }
                return
            }
        }
        
        if (logEnable && it.descriptionText) {
            log.info it.descriptionText
        }
        sendEvent(it)
    }
}

void on() {
    parent?.componentOn(this.device)
}

void off() {
    parent?.componentOff(this.device)
}

void refresh() {
    parent?.componentRefresh(this.device)
}

void setLevel(level, transitionTime = null) {
    parent?.componentSetLevel(this.device, level, transitionTime)
}