import groovy.transform.Field

@Field static parameterDescription = [
    (0):   [title: "Current threshold (lx)",
            help: "",
            style: "font-weight:bold;"]
]

static def getParameterDesc(int parameter) {
    def parameterDesc = parameterDescription[parameter]
    
    if(null == parameterDesc) {
        return [title: "", help: "", style: ""]
    }
    
    return parameterDesc
}

static String getParameterHelp(int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return parameterDesc.help
}

static String styleText(String text, String style = "") {
    return "<span style='${style}'>${text}</span>"
}

static String decorateString(String str, int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return styleText(str, parameterDesc.style)
}

static String getParameterTitle(int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return styleText(parameterDesc.title, parameterDesc.style)
}

metadata {
    definition(name: "Generic Component Illuminance Sensor", namespace: "drozovyk", author: "Dmytro Rozovyk", component: true) {
        capability("IlluminanceMeasurement") // add 'illuminance' attribute
        capability("Refresh")
        
        attribute("healthStatus","ENUM", ["offline", "online"]) // basically a "link" attribute with shorter options list
        
        input(name: "illuminanceThreshold",    type: "number", title:getParameterTitle(0), description: getParameterHelp(0), range: "0.0..20.0",   defaultValue: "0.0", required: false, displayDuringSetup: false)
    }
    preferences {
        input(name: "logEnable", type: "bool", title: "Enable logging", defaultValue: false)
    }
}

void updated() {
    if(logEnable) {
        log.info "Generic Component Illuminance Sensor: Updated."
    }
}

void installed() {
    if(logEnable) {
        log.info "Generic Component Illuminance Sensor: Installed."
    }
    
    device.updateSetting("logEnable", [type: "bool", value: false])
    
    parse([
        [name: "illuminance",     value: "0.0", descriptionText: "Initial value"]
    ])
}

void parse(String description) { 
    log.warn "Generic Component Illuminance Sensor: parse(String description) not implemented." 
}

void parse(List<Map> description) {
    description.each {
        def lastValue = null
        def threshold = null
        
        if(it.name == "illuminance") {
            lastValue = device.currentValue("illuminance")
            threshold = illuminanceThreshold
        }

        // if previus value and threshold are available we can test and skip annoing event spam
        if(null != lastValue && null != threshold) {
            def delta = Math.abs(it.value - lastValue)
            if(delta < Float.valueOf(threshold)) {
                if (logEnable) {
                    log.info("Generic Component Illuminance Sensor: The " + it.name + " change is below threshold. The ${it.name} threshold is ${threshold} but the change is ${delta}; skipping event")
                }
                return
            }
        }
        
        if (logEnable && it.descriptionText) {
            log.info(it.descriptionText)
        }
        sendEvent(it)
    }
}

void refresh() {
    parent?.componentRefresh(this.device)
}