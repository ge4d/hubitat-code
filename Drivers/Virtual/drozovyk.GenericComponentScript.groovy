metadata {
    definition(name: "Generic Component Script", namespace: "drozovyk", author: "Dmytro Rozovyk", component: true) {
        capability("Refresh")
        capability("Switch")      // 'switch' - not the best interface naming for the script. But logically fits
        capability("Variable")    // variable - STRING, setVariable(valueToSet) valueToSet required (STRING)
        
        attribute("customNumericAttribute", "NUMBER")
        attribute("customTextAttribute", "STRING")
        
        attribute("healthStatus","ENUM", ["offline", "online"]) // basically a "link" attribute with shorter options list
    }
    preferences {
        input(name: "logEnable", type: "bool", title: "Enable logging", defaultValue: false)
    }
}

void updated() {
    if(logEnable) {
        log.info "Generic Component Script: Updated."
    }
}

void installed() {
    if(logEnable) {
        log.info "Generic Component Script: Installed."
    }
    
    device.updateSetting("logEnable", [type: "bool", value: false])
    
    parse([
        [name: "switch",                 value: "unknown", descriptionText: "Initial value"],
        [name: "variable",               value: "",        descriptionText: "Initial value"],
        
        [name: "customNumericAttribute", value: "",        descriptionText: "Initial value"],
        [name: "customTextAttribute",    value: "",        descriptionText: "Initial value"]
    ])
}

void parse(String description) { 
    log.warn "Generic Component Script: parse(String description) not implemented." 
}

void parse(List<Map> description) {
    description.each {
        if (logEnable && it.descriptionText) {
            log.info it.descriptionText
        }
        sendEvent(it)
    }
}

void setVariable(String value) {
    parse([[name: "variable", value: value, descriptionText: "Command"]])
}

void on() {
    parent?.componentStartScript(this.device)
}

void off() {
    parent?.componentStopScript(this.device)
}

void refresh() {
    parent?.componentRefresh(this.device)
}