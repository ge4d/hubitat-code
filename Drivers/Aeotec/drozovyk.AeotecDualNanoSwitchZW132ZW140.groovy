/*
    https://help.aeotec.com/support/solutions/articles/6000178928-dual-nano-switch-zw140-user-guide
    https://help.aeotec.com/helpdesk/attachments/6051849576    Spec PDF
    https://manual.zwave.eu/backend/make.php?lang=en&sku=AEOEZW140&cert=ZC10-17035518

    https://help.aeotec.com/support/solutions/articles/6000247457-dual-nano-switch-with-energy-zw132-user-guide
    https://aeotec.freshdesk.com/helpdesk/attachments/6051849575    Spec PDF

    Common mandatory command classes
        Basic (controlled)

    Device specific command classes:
        Association Grp Info
        Association V2
        Clock
        Configuration
        Device Reset Locally
        Firmware Update Md V3
        Manufacturer Specific V2
        Meter V3 (ZW132)
        Multi Channel V4
        Multi Channel Association V2
        Notification V4 (controlled)
        Powerlevel        
        Scene Activation (controlled)
        Scene Actuator Conf
        Security
        Switch All
        Switch Binary (controlled)        
        Transport Service V2
        Version V2
        Zwaveplus Info V2

     Device endpoint 1 specific command classes:
        Association Grp Info
        Basic
        Security
        Zwaveplus Info V2

     Device endpoint 2 specific command classes:
        Association Grp Info
        Basic
        Security
        Zwaveplus Info V2
*/

#include drozovyk.association1
#include drozovyk.clock1
#include drozovyk.common1
#include drozovyk.encapsulation1
#include drozovyk.meter1
#include drozovyk.notification1
#include drozovyk.version1
#include drozovyk.zwave1

import groovy.transform.Field

@Field static List<String> param20options = ["Last state","On","Off"]
@Field static List<String> param80options = ["None","Hail (obsolete)","Basic","Hail (obsolete) when using the manual switch to change the load state"]
@Field static List<String> param81options = ["None","Basic"]
@Field static List<String> param83options = ["On/Off","On/Off for 5 seconds","Night light"]
@Field static List<String> param120options = ["Unknown","Two state switch","Three way switch", "Push button", "Auto"]
@Field static List<String> param122options = ["Own load","Other nodes","Own load and other nodes"]

@Field static parameterDescription = [
    (  3): [title: "Overload protection",
            help: "Output load will be closed after 30 seconds if the current exceeds 10.5A",
            size: 1,
            state: "Overload protection",
            style: "color:red; font-weight:bold;"],
    (  4): [title: "Overheat protection",
            help: "Output load will be closed after 30 seconds if the temperature inside the product exceeds 100℃",
            size: 1,
            state: "Overheat protection",
            style: "color:red; font-weight:bold;"],
    ( 20): [title: "Action in case of power out",
            help: "",
            size: 1,
            state: "Action in case of power out: ",
            style: "font-weight:bold;"],
    ( 80): [title: "Report to send on state change (Lifeline group)",
            help: "Avoid using manual-only option as driver relies on internal device state",
            size: 1,
            state: "Report to send on state change: ",
            style: "color:orangered; font-weight:bold;"],
    ( 81): [title: "External switch report of S1 to Switch 1 group",
            help: "",
            size: 1,
            state: "Report to send to Switch 1 group: ",
            style: "font-weight:bold;"],
    ( 82): [title: "External switch report of S2 to Switch 2 group",
            help: "",
            size: 1,
            state: "Report to send to Switch 2 group: ",
            style: "font-weight:bold;"],
    ( 83): [title: "State indicator mode",
            help: "Hides/Shows schedule control when applicable",
            size: 1,
            state: "Load indicator mode: ",
            style: "color:slateblue; font-weight:bold;"],
    ( 84): [title: "",
            help: "",
            size: 4,
            state: "Night light mode schedule: ",
            style: "color:slateblue; font-weight:bold;"],
    ( 86): [title: "Enable auto turn ON",
            help: "Device local weekly schedule; hides/shows related controls",
            size: 4,
            state: "Turn ON at ",
            style: "color:mediumorchid; font-weight:bold;"],
    ( 87): [title: "Enable auto turn OFF",
            help: "Device local weekly schedule; hides/shows related controls",
            size: 4,
            state: "Turn OFF at ",
            style: "color:mediumorchid; font-weight:bold;"],
    ( 90): [title: "Enable wattage report by threshold",
            help: "Hides/shows related controls",
            size: 1,
            state: "Enable wattage report by threshold: ",
            style: "color:green; font-weight:bold;"],
    ( 91): [title: "Power usage report threshold (W)",
            help: "",
            scale: 1,
            size: 2,
            state: "Power usage report threshold (W): ",
            style: "color:green; font-weight:bold;"],
    ( 92): [title: "Power usage report threshold (%)",
            help: "",
            scale: 1,
            size: 1,
            state: "Power usage report threshold (%): ",
            style: "color:green; font-weight:bold;"],
    (101): [title: "Timed report interval 1 measurements",
            help: "",
            scale: 1,
            size: 4,
            state: "Timed report interval 1 measurements: ",
            style: "color:green; font-weight:bold;"],
    (102): [title: "Timed report interval 2 measurements",
            help: "",
            scale: 1,
            size: 4,
            state: "Timed report interval 2 measurements: ",
            style: "color:green; font-weight:bold;"],
    (103): [title: "Timed report interval 3 measurements",
            help: "",
            scale: 1,
            size: 4,
            state: "Timed report interval 3 measurements: ",
            style: "color:green; font-weight:bold;"],
    (111): [title: "Timed report interval 1 (S)",
            help: "",
            scale: 1,
            size: 4,
            state: "Timed report interval 1 (S): ",
            style: "color:green; font-weight:bold;"],
    (112): [title: "Timed report interval 2 (S)",
            help: "",
            scale: 1,
            size: 4,
            state: "Timed report interval 2 (S): ",
            style: "color:green; font-weight:bold;"],
    (113): [title: "Timed report interval 3 (S)",
            help: "",
            scale: 1,
            size: 4,
            state: "Timed report interval 3 (S): ",
            style: "color:green; font-weight:bold;"],
    (120): [title: "External switch mode of S1",
            help: "",
            state: "External switch mode of S1: ",
            style: "font-weight:bold;"],
    (121): [title: "External switch mode of S2",
            help: "",
            size: 1,
            state: "External switch mode of S2: ",
            style: "font-weight:bold;"],
    (122): [title: "External switch control destination",
            help: "",
            size: 1,
            state: "External switch control destination: ",
            style: "font-weight:bold;"],
    (144): [title: "WallSwipe connection status (read-only)",
            help: "",
            size: 1,
            state: "WallSwipe connection status: ",
            style: "font-weight:bold;"]
]

static def getParameterDesc(int parameter) {
    def parameterDesc = parameterDescription[parameter]
    
    if(null == parameterDesc) {
        return [title: "", help: "", state: "", style: ""]
    }
    
    return parameterDesc
}

static String getParameterHelp(int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return parameterDesc.help
}

static String decorateState(String text, int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return styleText(parameterDesc.state + text, parameterDesc.style)
}

static String getParameterTitle(int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return styleText(parameterDesc.title, parameterDesc.style)
}

static def scaleParameterFromInput(int parameter, def value) {
    def parameterDesc = getParameterDesc(parameter)
    return value * parameterDesc.scale
}

static def scaleParameterToInput(int parameter, def value) {
    def parameterDesc = getParameterDesc(parameter)
    return value / parameterDesc.scale
}

// Association group 1 is reserved for the Z-Wave Plus Lifeline association group. Group 1 MUST NOT be 
// assigned to any other use than the Lifeline group. The actual Device Type specifies a mandatory list of 
// commands which the device MUST send to all targets associated to the Lifeline group. A manufacturer 
// MAY add additional commands to the Lifeline group.
@Field static List<String> nodeGroups = [
    "Forward/Retransmit (Basic, Scene Activation)",
    "Switch 1 (Basic)",
    "Switch 2 (Basic)"
]
@Field static Map<Short, String> associationGroups = [
    (1): "groupLifeline", 
    (2): "groupRetransmit",
    (3): "groupSwitch1",
    (4): "groupSwitch2"
]

boolean isZW132() {
    def deviceId = getDataValue("deviceId")
    
    if(null != deviceId) {
        return 132 == (deviceId as int)
    }
    
    return false
}

metadata {
    definition (name: "Aeotec Dual Nano Switch (ZW132/ZW140)", namespace: "drozovyk", author: "Dmytro Rozovyk") {
        capability "Actuator"
        capability "Configuration"
        capability "CurrentMeter"           // add 'amperage' attribute
        capability "EnergyMeter"            // add 'energy' attribute
        capability "Initialize"             // add 'initialize' command
        capability "PowerMeter"             // add 'power' attribute
        capability "Refresh"
        capability "RelaySwitch"            // same as "Switch"; device controls internal relay
        capability "Switch"                 // add 'switch' attribute and 'on','off' commands
        capability "VoltageMeasurement"     // add 'voltage' and 'frequency' attributes
        
        fingerprint (deviceId: "132", inClusters: "0x5E,0x25,0x27,0x32,0x81,0x71,0x60,0x8E,0x2C,0x2B,0x70,0x86,0x72,0x73,0x85,0x59,0x98,0x7A,0x5A", mfr: "134", deviceJoinName: "name")
        fingerprint (deviceId: "140", inClusters: "0x5E,0x25,0x27,0x81,0x71,0x60,0x8E,0x2C,0x2B,0x70,0x86,0x72,0x73,0x85,0x59,0x98,0x7A,0x5A", mfr: "134", deviceJoinName: "name")
        
        command("resetAllParameters")
        command("resetMeter")
        command("validateClockSettings")
        command("updatePreferencesFromDevice")
        command("updateVersionInfo")
        
        command("zwaveGroupAddNodeToGroup", [[name: "group", description: "", type: "ENUM", constraints: nodeGroups],[name: "node", description: "", type: "NUMBER"],[name: "enpoint", description: "", type: "NUMBER"]])
        command("zwaveGroupRemoveNodeFromGroup", [[name: "group", description: "", type: "ENUM", constraints: nodeGroups],[name: "node", description: "", type: "NUMBER"],[name: "endpoint", description: "", type: "NUMBER"]])
        
        attribute("alarmOverHeat", "STRING")
        attribute("alarmOverLoad", "STRING")
    }
    preferences {
        boolean isMetered = isZW132()
        
        if(isMetered) {
            input(name: "param3", type: "bool", title: getParameterTitle(3), description: getParameterHelp(3), defaultValue: "false", required: false, displayDuringSetup: false)
        }        
        input(name: "param4", type: "bool", title: getParameterTitle(4),  description: getParameterHelp(4),  defaultValue: "false", required: false, displayDuringSetup: false)        
        input(name: "param20", type: "enum", title: getParameterTitle(20), description: getParameterHelp(20), options: param20options, defaultValue: param20options[0], required: false, displayDuringSetup: false)
        input(name: "param80", type: "enum", title: getParameterTitle(80), description: getParameterHelp(80), options: param80options, defaultValue: param80options[0], required: false, displayDuringSetup: false)        
        input(name: "param83", type: "enum", title: getParameterTitle(83), description: getParameterHelp(83), options: param83options, defaultValue: param83options[0], required: false, displayDuringSetup: false)        
        if(2 == param83options.indexOf(param83)) {
            def parameterDesc = getParameterDesc(84)
            input(name: "param84from", type: "time", title: styleText("Night light mode start", parameterDesc.style), description: "", defaultValue: "18:00", required: false, displayDuringSetup: false)
            input(name: "param84to", type: "time", title: styleText("Night light mode end", parameterDesc.style), description: "", defaultValue: "08:00", required: false, displayDuringSetup: false)
        }
        
        input(name: "param86", type: "bool", title: getParameterTitle(86), description: getParameterHelp(86), defaultValue: "false", required: false, displayDuringSetup: false)
        if(param86) {
            def parameterDesc = getParameterDesc(86)
            input(name: "param86mon", type: "bool", title: styleText("Turn ON on Monday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param86tue", type: "bool", title: styleText("Turn ON on Tuesday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param86wed", type: "bool", title: styleText("Turn ON on Wednesday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param86thu", type: "bool", title: styleText("Turn ON on Thursday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param86fri", type: "bool", title: styleText("Turn ON on Friday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param86sat", type: "bool", title: styleText("Turn ON on Saturday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param86sun", type: "bool", title: styleText("Turn ON on Sunday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param86time", type: "time",title: styleText("Turn ON at", parameterDesc.style), description: "", defaultValue: "18:00", required: false, displayDuringSetup: false)
        }
        
        input(name: "param87", type: "bool", title: getParameterTitle(87), description: getParameterHelp(87), defaultValue: "false", required: false, displayDuringSetup: false)
        if(param87) {
            def parameterDesc = getParameterDesc(87)
            input(name: "param87mon", type: "bool", title: styleText("Turn OFF on Monday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param87tue", type: "bool", title: styleText("Turn OFF on Tuesday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param87wed", type: "bool", title: styleText("Turn OFF on Wednesday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param87thu", type: "bool", title: styleText("Turn OFF on Thursday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param87fri", type: "bool", title: styleText("Turn OFF on Friday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param87sat", type: "bool", title: styleText("Turn OFF on Saturday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param87sun", type: "bool", title: styleText("Turn OFF on Sunday", parameterDesc.style), description: "", defaultValue: "false", required: false, displayDuringSetup: false)
            input(name: "param87time", type: "time",title: styleText("Turn OFF at", parameterDesc.style), description: "", defaultValue: "23:00", required: false, displayDuringSetup: false)
        }
        
        if(isMetered) {
            input(name: "param90", type: "bool", title: getParameterTitle(90), description: getParameterHelp(90), defaultValue: "false", required: false, displayDuringSetup: false)
            if(param90) {
                input(name: "param91", type: "number", title: getParameterTitle(91), description: getParameterHelp(91), range: "0..60000", defaultValue: "0", required: false, displayDuringSetup: false)
                input(name: "param92", type: "number", title: getParameterTitle(92), description: getParameterHelp(92), range: "0..100", defaultValue: "0", required: false, displayDuringSetup: false)
            }

            input(name: "param101", type: "number", title: getParameterTitle(101), description: getParameterHelp(101), range: "0..1776399", defaultValue: "0", required: false, displayDuringSetup: false)
            input(name: "param111", type: "number", title: getParameterTitle(111), description: getParameterHelp(111), range: "0..2592000", defaultValue: "0", required: false, displayDuringSetup: false)
            input(name: "param102", type: "number", title: getParameterTitle(102), description: getParameterHelp(102), range: "0..1776399", defaultValue: "0", required: false, displayDuringSetup: false)
            input(name: "param112", type: "number", title: getParameterTitle(112), description: getParameterHelp(112), range: "0..2592000", defaultValue: "0", required: false, displayDuringSetup: false)
            input(name: "param103", type: "number", title: getParameterTitle(103), description: getParameterHelp(103), range: "0..1776399", defaultValue: "0", required: false, displayDuringSetup: false)
            input(name: "param113", type: "number", title: getParameterTitle(113), description: getParameterHelp(113), range: "0..2592000", defaultValue: "0", required: false, displayDuringSetup: false)
            
            //0x64 (100) Set 101-103 to default. N/A 1            
            //0x6E (110) Set 111-113 to default. N/A 1             
        }
        
        input(name: "param120", type: "enum", title: getParameterTitle(120), description: getParameterHelp(120), options: param120options, defaultValue: param120options[0], required: false, displayDuringSetup: false)
        input(name: "param81", type: "enum", title: getParameterTitle(81), description: getParameterHelp(81), options: param81options, defaultValue: param81options[0], required: false, displayDuringSetup: false)
        input(name: "param121", type: "enum", title: getParameterTitle(121), description: getParameterHelp(121), options: param120options, defaultValue: param120options[0], required: false, displayDuringSetup: false)
        input(name: "param82", type: "enum", title: getParameterTitle(82), description: getParameterHelp(82), options: param81options, defaultValue: param81options[0], required: false, displayDuringSetup: false)  
        input(name: "param122", type: "enum", title: getParameterTitle(122), description: getParameterHelp(122), options: param122options, defaultValue: param122options[0], required: false, displayDuringSetup: false)

        // param255 -> factory reset
        
        inputLogLevel()
    }
}

//=============================================================================================================================================================
// Common
//=============================================================================================================================================================

String getSwitchDeviceLabel(Short switchNumber)      { return "${device.label} switch ${switchNumber}" }
String getSwitchDeviceName(Short switchNumber)       { return "${device.name}-switch-${switchNumber}" }
String getSwitchDeviceNetworkId(Short switchNumber)  { return "${device.deviceNetworkId}-switch-${switchNumber}" }
def    getSwitchDevice(Short switchNumber)           { return getChildDevice(getSwitchDeviceNetworkId(switchNumber)) }

void configureChildDevices() {
    getChildDevices().each { deleteChildDevice("${it.deviceNetworkId}") }
    
    for(Short switchIndex = 1; switchIndex < 3; ++switchIndex) {
        switchDevice = getSwitchDevice(switchIndex)
        if(null == switchDevice) {            
            switchDevice = addChildDevice("drozovyk", "Generic Component Metered Switch", getSwitchDeviceNetworkId(switchIndex),  [isComponent: true, name: getSwitchDeviceName(switchIndex), label: getSwitchDeviceLabel(switchIndex)])
            switchDevice.updateDataValue("EP", switchIndex as String)
            switchDevice.refresh()
            logInfo("Spawned child device for the switch ${switchIndex}")
        }          
    }    
}

def getEndpointDevice(Short ep = 0) {
    if(ep > 0) {
        return getSwitchDevice(ep)
    }
    else {
        return this
    }    
}

void removeStateHint() {
    state.remove("hint")
}

//=============================================================================================================================================================
// Z-Wave common
//=============================================================================================================================================================

// Basic V1
void zwaveEvent(hubitat.zwave.commands.basicv1.BasicReport cmd, Short ep = 0) {
    logDebug("Received ${cmd} for ${ep}")
    
    parse([[name: "switch", value: (cmd.value > 0) ? "on" : "off"]], ep)
    
    // Request meter report (Most probaly state has been changed; but due to inertia of power sensor delaying report request)
    runIn(4 + 2 * ep, getMeterReport, [data: ep, overwrite: false])
}

String getBasicReportString(Short ep = 0) {
    return encapsulate(zwave.basicV1.basicGet(), ep)
}

void getBasicReport(Short ep = 0) {
    sendHubCommand(new hubitat.device.HubAction(getBasicReportString(ep), hubitat.device.Protocol.ZWAVE))
}

void setBasic(Boolean state, Short ep = 0) {
    String cmd = encapsulate(zwave.basicV1.basicSet(value: state ? 0xFF : 0x00), ep)
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

// Hail (obsolete)
void zwaveEvent(hubitat.zwave.commands.hailv1.Hail cmd, Short ep = 0) {
    logDebug("Received ${cmd} for ${ep}")
    
    // Check state
    getBasicReport(ep)
}

//=============================================================================================================================================================
// Z-Wave device-specific
//=============================================================================================================================================================

// Association grp info
//     not used atm

// Association V2
//    see 'association1'

// Clock
//    see 'clock1'

// Configuration V1
void zwaveEvent(hubitat.zwave.commands.configurationv1.ConfigurationReport cmd, Short ep = 0) {
    logDebug("Received ${cmd} for ${ep}")
    
    def parameterState = [value: cmd.scaledConfigurationValue]
    
    def options = null
    if(cmd.parameterNumber == 20) {
        options = param20options
    } else if(cmd.parameterNumber == 80) {
        options = param80options
    } else if(cmd.parameterNumber == 81) {
        options = param81options
    } else if(cmd.parameterNumber == 82) {
        options = param81options
    } else if(cmd.parameterNumber == 83) {
        options = param83options
    } else if(cmd.parameterNumber == 120) {
        options = param120options
    } else if(cmd.parameterNumber == 121) {
        options = param120options
    } else if(cmd.parameterNumber == 122) {
        options = param122options
        cmd.scaledConfigurationValue -= 1
    }
    
    if(cmd.parameterNumber == 84) {
        // decode time interval
        def to = Calendar.instance
        def tminute  = ((cmd.scaledConfigurationValue as int) >> 0) & 255
        def thour    = ((cmd.scaledConfigurationValue as int) >> 8) & 255
        to.set(hourOfDay: thour, minute: tminute)
        
        def from = Calendar.instance
        def fminute  = ((cmd.scaledConfigurationValue as int) >> 16) & 255
        def fhour    = ((cmd.scaledConfigurationValue as int) >> 24) & 255
        from.set(hourOfDay: fhour, minute: fminute)
        
        parameterState.desc = decorateState("${from.format("HH:mm")} - ${to.format("HH:mm")}", cmd.parameterNumber)
        device.updateSetting("param${cmd.parameterNumber}from", [type: "time",value: from.getTime()])
        device.updateSetting("param${cmd.parameterNumber}to", [type: "time",value: to.getTime()])        
    } else if(cmd.parameterNumber == 86 || cmd.parameterNumber == 87) {
        // decode time
        def time = Calendar.instance
        def minute  = ((cmd.scaledConfigurationValue as int) >> 0) & 255
        def hour    = ((cmd.scaledConfigurationValue as int) >> 8) & 255
        time.set(hourOfDay: hour, minute: minute)
        
        def monday    = (((cmd.scaledConfigurationValue as int) >> 16) & 1) as boolean
        def tuesday   = (((cmd.scaledConfigurationValue as int) >> 17) & 1) as boolean
        def wednesday = (((cmd.scaledConfigurationValue as int) >> 18) & 1) as boolean
        def thursday  = (((cmd.scaledConfigurationValue as int) >> 19) & 1) as boolean
        def friday    = (((cmd.scaledConfigurationValue as int) >> 20) & 1) as boolean
        def saturday  = (((cmd.scaledConfigurationValue as int) >> 21) & 1) as boolean
        def sunday    = (((cmd.scaledConfigurationValue as int) >> 22) & 1) as boolean
            
        def enabled = (((cmd.scaledConfigurationValue as int) >> 24) & 1) as boolean
         
        device.updateSetting("param${cmd.parameterNumber}mon" as String, [type: "bool", value: monday])
        device.updateSetting("param${cmd.parameterNumber}tue" as String, [type: "bool", value: tuesday])
        device.updateSetting("param${cmd.parameterNumber}wed" as String, [type: "bool", value: wednesday])
        device.updateSetting("param${cmd.parameterNumber}thu" as String, [type: "bool", value: thursday])
        device.updateSetting("param${cmd.parameterNumber}fri" as String, [type: "bool", value: friday])
        device.updateSetting("param${cmd.parameterNumber}sat" as String, [type: "bool", value: saturday])
        device.updateSetting("param${cmd.parameterNumber}sun" as String, [type: "bool", value: sunday])
        
        String days = ""    
        if(monday) {days += "Mon, "}
        if(tuesday) {days += "Tue, "}
        if(wednesday) {days += "Wed, "}
        if(thursday) {days += "Thu, "}
        if(friday) {days += "Fri, "}
        if(saturday) {days += "Sat, "}
        if(sunday) {days += "Sun, "}
            
        parameterState.desc = decorateState(days + "${time.format("HH:mm")} (Active : ${enabled})", cmd.parameterNumber)
    } else {
        if(options != null) {
            parameterState.desc = decorateState(options[cmd.scaledConfigurationValue], cmd.parameterNumber)
            device.updateSetting("param${cmd.parameterNumber}" as String, [type: "enum", value: options[cmd.scaledConfigurationValue]])
        }
        else {
            parameterState.desc = decorateState("", cmd.parameterNumber)
            if (cmd.parameterNumber != 144) {
                if(cmd.parameterNumber == 3 || cmd.parameterNumber == 4 || cmd.parameterNumber == 90) {
                    device.updateSetting("param${cmd.parameterNumber}" as String, [type: "bool", value: (cmd.scaledConfigurationValue > 0 ? "true" : "false")])
                }
                else {
                    parameterState.desc = decorateState("${scaleParameterToInput(cmd.parameterNumber, cmd.scaledConfigurationValue)}", cmd.parameterNumber)
                    device.updateSetting("param${cmd.parameterNumber}" as String, [type: "number", value: scaleParameterToInput(cmd.parameterNumber, cmd.scaledConfigurationValue)])
                }
            }
        }
    }
    
    state["parameter${cmd.parameterNumber}"] = parameterState
}

String setParameter(paramId, size, value) {
    return encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
}

String setParameter(paramId) {
    def options = null
    if(paramId == 20) {
        options = param20options
    } else if(paramId == 80) {
        options = param80options
    } else if(paramId == 81) {
        options = param81options
    } else if(paramId == 82) {
        options = param81options
    } else if(paramId == 83) {
        options = param83options
    } else if(paramId == 120) {
        options = param120options
    } else if(paramId == 121) {
        options = param120options
    } else if(paramId == 122) {
        options = param122options
    }
    
    def String cmd = ""
    if(paramId == 84) {
        if(param84from != null && param84to != null) {
            def from  = Date.parse("yyy-MM-dd'T'HH:mm:ss.SSSZ", param84from).toCalendar()
            def to    = Date.parse("yyy-MM-dd'T'HH:mm:ss.SSSZ", param84to).toCalendar()

            def byte[] bytes = [ from.get(Calendar.HOUR_OF_DAY) as byte, from.get(Calendar.MINUTE) as byte, to.get(Calendar.HOUR_OF_DAY) as byte, to.get(Calendar.MINUTE) as byte ]
            def Number value = new BigInteger(bytes).intValue()

            if(state["parameter${paramId}"]?.value != value) {
                def Number size = parameterDescription[paramId].size
                cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
            }

        }
    } else if(paramId == 86 || paramId == 87) {
        def inputValue = this.settings["param${paramId}"]
        def inputValueMon = this.settings["param${paramId}mon"]
        def inputValueTue = this.settings["param${paramId}tue"]
        def inputValueWed = this.settings["param${paramId}wed"]
        def inputValueThu = this.settings["param${paramId}thu"]
        def inputValueFri = this.settings["param${paramId}fri"]
        def inputValueSat = this.settings["param${paramId}sat"]
        def inputValueSun = this.settings["param${paramId}sun"]
        def inputValueTime = this.settings["param${paramId}time"]
        
        if(inputValue != null && inputValueMon != null && inputValueTue != null && inputValueWed != null && inputValueThu != null && inputValueFri != null && inputValueSat != null && inputValueSun != null && inputValueTime != null) {
            def time  = Date.parse("yyy-MM-dd'T'HH:mm:ss.SSSZ", inputValueTime).toCalendar()

            def Number value = 0
            value += (time.get(Calendar.MINUTE) as int)
            value += (time.get(Calendar.HOUR_OF_DAY) as int) << 8
            value += (inputValueMon as boolean) ? (1 << 16) : 0
            value += (inputValueTue as boolean) ? (1 << 17) : 0
            value += (inputValueWed as boolean) ? (1 << 18) : 0
            value += (inputValueThu as boolean) ? (1 << 19) : 0
            value += (inputValueFri as boolean) ? (1 << 20) : 0
            value += (inputValueSat as boolean) ? (1 << 21) : 0
            value += (inputValueSun as boolean) ? (1 << 22) : 0
            value += (inputValue as boolean) ? (1 << 24) : 0

            if(state["parameter${paramId}"]?.value != value) {
                def Number size = parameterDescription[paramId].size
                cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
            } 

        }
    } else {
        def inputValue = this.settings["param${paramId}"]
        if(inputValue != null) {
            if(options != null) {
                def Number value = options.indexOf(inputValue)
                if(paramId == 122) {
                    value += 1
                }                
                if(state["parameter${paramId}"]?.value != value) {
                    def Number size = parameterDescription[paramId].size
                    cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
                }
            } else if(paramId == 3 || paramId == 4 || paramId == 90) {
                def Number value = (inputValue as boolean) ? 1 : 0
                if(state["parameter${paramId}"]?.value != value) {
                    def Number size = parameterDescription[paramId].size
                    cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
                }
            }
            else {
                def Number value = scaleParameterFromInput(paramId, inputValue as BigDecimal)
                if(state["parameter${paramId}"]?.value != value) {
                    def Number size = parameterDescription[paramId].size
                    cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
                }
            }
        }
    }
    
    return cmd
}

String getParameterReport(paramId) {
    return encapsulate(zwave.configurationV1.configurationGet(parameterNumber: paramId))    
}

// Device reset locally
//     not used atm

// Firmware update md V3
//     not used atm

// Manufacturer specific V2
//     not used atm

// Meter V3
List<String> getMeterReportStringList(int ep = 0) {
    return [
        encapsulate(zwave.meterV3.meterGet(scale: 5), ep as Short),
        encapsulate(zwave.meterV3.meterGet(scale: 4), ep as Short),
        encapsulate(zwave.meterV3.meterGet(scale: 2), ep as Short),
        encapsulate(zwave.meterV3.meterGet(scale: 0), ep as Short)
    ]    
}

void getMeterReport(int ep = 0) {
    def cmds = delayBetween(getMeterReportStringList(ep), 500)
    sendHubCommand(new hubitat.device.HubMultiAction(cmds, hubitat.device.Protocol.ZWAVE))
}

void resetMeter() {
    runIn(1, refresh)
    String cmd = encapsulate(zwave.meterV3.meterReset())
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))    
}

// Notification V4

// NOTIFICATION_TYPE_HEAT
//     State idle           0x00
//     Over-heat detected	0x02
// NOTIFICATION_TYPE_POWER_MANAGEMENT
//    State idle            0x00
//    Over-current detected 0x06
void zwaveEvent(hubitat.zwave.commands.notificationv4.NotificationReport cmd, Short ep = 0) {
    zwaveNotificationEvent("V4", cmd, ep)
    
    if(cmd.notificationType == cmd.NOTIFICATION_TYPE_HEAT) {
        if(cmd.event == 0x00) {
            parse([[name:"alarmOverHeat", value: "idle"]])
        } else if(cmd.event == 0x02) {
            parse([[name:"alarmOverHeat", value: "alarm"]])
        }
    }
    else if(cmd.notificationType == cmd.NOTIFICATION_TYPE_POWER_MANAGEMENT) {
        if(cmd.event == 0x00) {
            parse([[name:"alarmOverLoad", value: "idle"]])
        } else if(cmd.event == 0x06) {
            parse([[name:"alarmOverLoad", value: "alarm"]])
        }
    }
}

// Power level
//    not used atm (radio power level info)

// Protection V2
//    not used atm

// Scene activation
//    not used atm

// Scene activation conf
//    not used atm

// Switch all (obsolete in favor of S2 multicast; S2 is not supported by this devices)
//    not used atm

// Switch binary
void zwaveEvent(hubitat.zwave.commands.switchbinaryv1.SwitchBinaryReport cmd, Short ep = 0) {
    logDebug("Received ${cmd} for ${ep}")
    
    if(cmd.value == 0xFF) {
        parse([[name: "switch", value: "on"]], ep)        
    } else if(cmd.value == 0x00) {
        parse([[name: "switch", value: "off"]], ep)        
    } else {
        logWarn("Switch binary report unknown state ${cmd.value}")
    }
    
    // Request meter report (Most probaly state has been changed; but due to inertia of power sensor delaying report request)
    runIn(4 + 2 * ep, getMeterReport, [data: ep, overwrite: false])
}

String getSwitchBinaryReportString(Short ep = 0) {
    return encapsulate(zwave.switchBinaryV1.switchBinaryGet(), ep)    
}

void getSwitchBinaryReport(Short ep = 0) {
    sendHubCommand(new hubitat.device.HubAction(getSwitchBinaryReportString(ep), hubitat.device.Protocol.ZWAVE))
}

void setSwitchBinary(boolean state, Short ep = 0) {
    String cmd = encapsulate(zwave.switchBinaryV1.switchBinarySet(switchValue: state ? 0xFF : 0x00), ep)
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

// Transport service V2

// Version V2
//    see 'version1'
void updateVersionInfo() {
    sendHubCommand(new hubitat.device.HubAction(getVersionReportCommand(), hubitat.device.Protocol.ZWAVE))
}

// Zwaveplus info V2
//    not used atm

void zwaveGroupAddNodeToGroup(String enumValue, def nodeId, def endpointId = null) {
    int groupNumber = (nodeGroups.indexOf(enumValue) as int) + 2 // offset by one (groups start from '1') and skip 'Lifeline' group
    if(null == endpointId) {
        addDeviceNodeToGroup(groupNumber, nodeId)
    }
    else {
        addDeviceEndpointToGroup(groupNumber, nodeId, endpointId)
    }
}

void zwaveGroupRemoveNodeFromGroup(String enumValue, def nodeId, def endpointId = null) {
    int effectnumber = (nodeGroups.indexOf(enumValue) as int) + 2 // offset by one (groups start from '1') and skip 'Lifeline' group
    if(null == endpointId) {
        removeDeviceNodeFromGroup(groupNumber, nodeId)
    }
    else {
        removeDeviceEndpointFromGroup(groupNumber, nodeId, endpointId)
    }
}

void resetAllParameters() { 
    // not a factory reset
    
    // Any value 
    //    except 0x55555555 which is full factory reset!!
    //
    String cmd = setParameter(0xFF, 4, 0x00000000)
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
    runIn(1, updatePreferencesFromDevice)    
}

void updatePreferencesFromDevice() {
    def configurationCommands = []
    
    configurationCommands << getParameterReport(  4)
    configurationCommands << getParameterReport( 20)
    configurationCommands << getParameterReport( 80)
    configurationCommands << getParameterReport( 81)
    configurationCommands << getParameterReport( 82)
    configurationCommands << getParameterReport( 83)
    configurationCommands << getParameterReport( 84)
    configurationCommands << getParameterReport( 86)
    configurationCommands << getParameterReport( 87)
    configurationCommands << getParameterReport(120)
    configurationCommands << getParameterReport(121)
    configurationCommands << getParameterReport(122)
    configurationCommands << getParameterReport(144)
    
    boolean isMetered = isZW132()
    if(isMetered) {
        configurationCommands << getParameterReport(  3)
        configurationCommands << getParameterReport( 90)
        configurationCommands << getParameterReport( 91)
        configurationCommands << getParameterReport( 92)
        configurationCommands << getParameterReport(101)
        configurationCommands << getParameterReport(102)
        configurationCommands << getParameterReport(103)
        configurationCommands << getParameterReport(111)
        configurationCommands << getParameterReport(112)
        configurationCommands << getParameterReport(113)
    }
    
    configurationCommands.addAll(getDeviceGroupNodesCommandList())
    configurationCommands.addAll(getDeviceMultiChannelGroupNodesCommandList())
    
    state.clear()
    state.hint = "<b style='color:red;'>Not all parameters are reported. Preferences might get updated according to device settings. Please, refresh this page in a few seconds</b>"
    runIn(20, removeStateHint)    
    
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(configurationCommands, 500), hubitat.device.Protocol.ZWAVE))
}

void validateClockSettings() {
    sendHubCommand(new hubitat.device.HubAction(getClockReportString(), hubitat.device.Protocol.ZWAVE))
}

//=============================================================================================================================================================
// Own interface
//=============================================================================================================================================================

void configure() {
    def preset = [[name: "alarmOverHeat", value: "idle", descriptionText: "Initial value"]]
    if(isZW132()) {
        preset << [name: "alarmOverLoad", value: "idle", descriptionText: "Initial value"]
    }
    
    preset << [name: "switch",   value: "unknown", descriptionText: "Initial value"]
    preset << [name: "amperage", value: "0",       descriptionText: "Initial value"]
    preset << [name: "energy",   value: "0",       descriptionText: "Initial value"]
    preset << [name: "power",    value: "0",       descriptionText: "Initial value"]
    preset << [name: "voltage",  value: "0",       descriptionText: "Initial value"]
    
    parse(preset)

    updatePreferencesFromDevice()
    updateVersionInfo()
    configureChildDevices()
}

void initialize() {    
    refresh()
}

def installed() {
    configure()
}

void refresh() {
    getBasicReport()
    getChildDevices().each { it.refresh() }
    
    runIn(2, validateClockSettings)
}

void off() {
    setBasic(false)
}

void on() {
    setBasic(true)    
}

void updated() {
    // Send all parameters one by one
    def configurationCommands = []
    
    configurationCommands << setParameter(  4)
    configurationCommands << setParameter( 20)
    configurationCommands << setParameter( 80)
    configurationCommands << setParameter( 83)
    configurationCommands << setParameter( 84)
    configurationCommands << setParameter( 86)
    configurationCommands << setParameter( 87)
    configurationCommands << setParameter(120)
    configurationCommands << setParameter(121)
    configurationCommands << setParameter(122)
    configurationCommands.removeAll("")
    
    boolean isMetered = isZW132()
    if(isMetered) {
        configurationCommands << setParameter(  3)
        configurationCommands << setParameter( 90)
        configurationCommands << setParameter( 91)
        configurationCommands << setParameter( 92)
        configurationCommands << setParameter(101)
        configurationCommands << setParameter(102)
        configurationCommands << setParameter(103)
        configurationCommands << setParameter(111)
        configurationCommands << setParameter(112)
        configurationCommands << setParameter(113)
        configurationCommands.removeAll("")
        
        configurationCommands << getParameterReport(  3)
        configurationCommands << getParameterReport( 90)
        configurationCommands << getParameterReport( 91)
        configurationCommands << getParameterReport( 92)
        configurationCommands << getParameterReport(101)
        configurationCommands << getParameterReport(102)
        configurationCommands << getParameterReport(103)
        configurationCommands << getParameterReport(111)
        configurationCommands << getParameterReport(112)
        configurationCommands << getParameterReport(113)
    }
    
    configurationCommands << getParameterReport(  4)
    configurationCommands << getParameterReport( 20)
    configurationCommands << getParameterReport( 80)
    configurationCommands << getParameterReport( 81)
    configurationCommands << getParameterReport( 82)
    configurationCommands << getParameterReport( 83)
    configurationCommands << getParameterReport( 84)
    configurationCommands << getParameterReport( 86)
    configurationCommands << getParameterReport( 87)
    configurationCommands << getParameterReport(120)
    configurationCommands << getParameterReport(121)
    configurationCommands << getParameterReport(122)
    configurationCommands << getParameterReport(144)
    
    configurationCommands.addAll(getDeviceGroupNodesCommandList())
    configurationCommands.addAll(getDeviceMultiChannelGroupNodesCommandList())
    
    state.clear()
    state.hint = "<b style='color:red;'>Not all parameters are reported. Preferences might get updated according to device settings. Please, refresh this page in a few seconds</b>"
    runIn(20, removeStateHint)
    
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(configurationCommands, 500), hubitat.device.Protocol.ZWAVE))    
}

//=============================================================================================================================================================
// Component devices interface
//=============================================================================================================================================================

void componentParse(cd, List<Map> events) {
    // stub
}

void componentRefresh(cd) {
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'refresh' request from ${cd.displayName} (EP${endPoint})")
        getBasicReport(endPoint)
    }
}

void componentOn(cd) {
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'on' request from ${cd.displayName} (EP${endPoint})")
        setBasic(true, endPoint)
    }
}

void componentOff(cd){
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'off' request from ${cd.displayName} (EP${endPoint})")
        setBasic(false, endPoint)
    }
}
