/*
    https://aeotec.freshdesk.com/support/solutions/articles/6000219911-smart-switch-7-f-plug-user-guide-

    Common mandatory command classes
        Basic (controlled)

    Aeotec Smart Switch 7 (F-plug) command classes:        
        Application Status
        Association Grp Info
        Association V2
        Clock
        Color Switch
        Configuration
        Device Reset Locally
        Firmware Update Md V4
        Manufacturer Specific V2
        Meter V4
        Notification V4 (controlled)
        Powerlevel
        Protection V2
        Scene Activation (controlled)
        Scene Actuator Conf
        Security
        Security 2
        Supervision
        Switch Binary (controlled)
        Switch Color V1 (works only when parameter 81 is set to "Night light" or "On/Off" mode).
        Switch Multilevel V2 (works only when parameter 81 is set to "Night light" or "On/Off" mode).
        Transport Service V2
        Version V2
        Zwaveplus Info V2

        Documentation inconsistency:
            * Parameter 92 seem to have no effect (firmware issue?)

            * Documentation says (for parameter 101):
                1	kWh (Accumulated Power)
                2	Watt (Power used at the time of the report)
                4	Current
                8	Voltage
              Device reacts (for parameter 101):
                1	kWh (Accumulated Power)
                2	Watt (Power used at the time of the report)
                8	Current
                4	Voltage
*/

#include drozovyk.association1
#include drozovyk.clock1
#include drozovyk.common1
#include drozovyk.encapsulation1
#include drozovyk.meter1
#include drozovyk.notification1
#include drozovyk.protection1
#include drozovyk.version1
#include drozovyk.zwave1

import groovy.transform.Field

@Field static List<String> param8options =  ["None","Turn on","Turn off","Cycle on/off"]
@Field static List<String> param10options = ["Tap action button 3 times within a second","Get idle or reversal state","Wait for timeout"]
@Field static List<String> param20options = ["Last state","On","Off"]
@Field static List<String> param80options = ["None","Basic","Binary switch"]
@Field static List<String> param81options = ["Disabled","Night light","On/Off"]

@Field static parameterDescription = [
    (  4): [title: "Overload protection (W)",
            help: "",
            scale: 1,
            size: 2,
            state: "Overload protection (W): ",
            style: "color:red; font-weight:bold;"],
    (  8): [title: "Alarm action",
            help:  "Hides/Shows related controls when applicable",
            size: 1,
            state: "Alarm action: ",
            style: "color:peru; font-weight:bold;"],
    (  9): [title: "",
            help: "",
            size: 2,
            state: "",
            style: "color:peru; font-weight:bold;"],
    ( 10): [title: "Cancel alarm",
            help: "",
            size: 2,
            state: "Cancel alarm: ",
            style: "color:peru; font-weight:bold;"],
    ( 18): [title: "LED blinking frequency (Hz)",
            help: "",
            scale: 1,
            size: 2,
            state: "LED blinking frequency (Hz): ",
            style: "font-weight:bold;"],
    ( 20): [title: "Action in case of power out",
            help: "",
            size: 1,
            state: "Action in case of power out: ",
            style: "font-weight:bold;"],
    ( 80): [title: "Report to send on state change (Lifeline group)",
            help:  "Avoid disabling it as driver relies on internal device state",
            size: 1,
            state: "Report to send on state change: ",
            style: "color:orangered; font-weight:bold;"],
    ( 81): [title: "Load indicator mode",
            help:  "Hides/Shows schedule control when applicable",
            size: 1,
            state: "Load indicator mode: ",
            style: "color:slateblue;font-weight:bold;"],
    ( 82): [title: "",
            help: "",
            size: 4,
            state: "Night light schedule: ",
            style: "color:slateblue;font-weight:bold;"],
    ( 91): [title: "Power usage report threshold (W)",
            help: "The value is compared against the total power level, not the delta since the last report",
            scale: 1,
            size: 2,
            state: "Power usage report threshold (W): ",
            style: "color:green; font-weight:bold;"],
    ( 92): [title: "Consumed energy report threshold (kWh)",
            help: "The value is compared against the total accumulated energy, not the delta since the last report",
            scale: 1,
            size: 2,
            state: "Consumed energy report threshold (kWh): ",
            style: "color:green; font-weight:bold;"],
    ( 93): [title: "Current report threshold (x100 mA)",
            help: "The value is compared against the total current level, not the delta since the last report",
            scale: 1,
            size: 1,
            state: "Current report threshold (x100 mA): ",
            style: "color:green; font-weight:bold;"],
    (101): [title: "",
            help: "",
            size: 4,
            state: "Timed report measurements: ",
            style: "color:green; font-weight:bold;"],
    (111): [title: "Timed report interval (S)",
            help: "",
            scale: 1,
            size: 4,
            state: "Timed report interval (S): ",
            style: "color:green; font-weight:bold;"]
]

static def getParameterDesc(int parameter) {
    def parameterDesc = parameterDescription[parameter]
    
    if(null == parameterDesc) {
        return [title: "", help: "", state: "", style: ""]
    }
    
    return parameterDesc
}

static String getParameterHelp(int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return parameterDesc.help
}

static String decorateState(String text, int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return styleText(parameterDesc.state + text, parameterDesc.style)
}

static String getParameterTitle(int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return styleText(parameterDesc.title, parameterDesc.style)
}

static def scaleParameterFromInput(int parameter, def value) {
    def parameterDesc = getParameterDesc(parameter)
    return value * parameterDesc.scale
}

static def scaleParameterToInput(int parameter, def value) {
    def parameterDesc = getParameterDesc(parameter)
    return value / parameterDesc.scale
}

// Association group 1 is reserved for the Z-Wave Plus Lifeline association group. Group 1 MUST NOT be 
// assigned to any other use than the Lifeline group. The actual Device Type specifies a mandatory list of 
// commands which the device MUST send to all targets associated to the Lifeline group. A manufacturer 
// MAY add additional commands to the Lifeline group.
@Field static List<String> nodeGroups = [
    "Forward/Retransmit"
]
@Field static Map<Short, String> associationGroups = [
    (1): "groupLifeline", 
    (2): "groupRetransmit"
]

metadata {
    definition (name: "Aeotec Smart Switch 7 (F-plug)", namespace: "drozovyk", author: "Dmytro Rozovyk") {
        capability("Actuator")
        capability("Configuration")
        capability("CurrentMeter")          // add 'amperage' attribute
        capability("EnergyMeter")           // add 'energy' attribute
        capability("Initialize")            // add 'initialize' command
        capability("Outlet")                // add 'switch' attribute and 'on','off' commands
        capability("PowerMeter")            // add 'power' attribute
        capability("Refresh")
        capability("RelaySwitch")           // same as "Outlet"; device controls internal relay
        capability("Switch")                // same as "Outlet"; used to show device in the 'Devices' tab of the mobile app
        capability("VoltageMeasurement")    // add 'voltage' and 'frequency' attributes
        
        fingerprint(deviceId: "175", inClusters: "0x5E,0x55,0x22,0x98,0x9F,0x6C", mfr: "881", deviceJoinName: "name")
        
        command("blinkWithStatusLight", ["number"])
        command("buttonLightSetBrightness", [[name: "brightness*", type: "NUMBER", description: "Indicator brightness for the current state [0..100].\nWorks only for 'Night light' and 'On/Off' indicator modes"]])
        command("buttonLightSetColor", [[name: "color*", type: "COLOR_MAP", description: "Indicator color for the current state.\nWorks only for 'Night light' and 'On/Off' indicator modes"]])
        command("buttonLock")
        command("buttonUnlock")
        command("resetAllParameters")
        command("resetMeter")
        command("validateClockSettings")
        command("updatePreferencesFromDevice")
        command("updateVersionInfo")
        
        for(int type = 1; type < 8; ++type) {
            command("zwaveSend${notificationTypeEvents[type].name}Notification", [
                [name: "notificationEvent", description: "", type: "ENUM", constraints: notificationTypeEvents[type].events]
            ])
        }
        // Node compatibility MUST be respected.
        command("zwaveGroupAddNodeToGroup", [[name: "group", description: "", type: "ENUM", constraints: nodeGroups],[name: "node", description: "", type: "NUMBER"]])
        command("zwaveGroupRemoveNodeFromGroup", [[name: "group", description: "", type: "ENUM", constraints: nodeGroups],[name: "node", description: "", type: "NUMBER"]])
        
        attribute("alarmOverCurrent",     "STRING")
        attribute("alarmOverLoad",        "STRING")
        attribute("alarmHardwareFailure", "STRING")
        attribute("protectionLocal",      "STRING")
		attribute("protectionRf",         "STRING")
    }
    preferences {
        input(name: "param4", type: "number", title: getParameterTitle(4), description: getParameterHelp(4), range: "0..2415", defaultValue: "2415", required: false, displayDuringSetup: false)

        input(name: "param8", type: "enum",   title: getParameterTitle(8), description: getParameterHelp(8), options: param8options, defaultValue: param8options[0], required: false, displayDuringSetup: false)
        if(0 < param8options.indexOf(param8)) {
            def parameterDesc = getParameterDesc(9)
            input(name: "param9state",    type: "bool", title: styleText("React when Access control: Window/door is 'closed'", parameterDesc.style), description: "or 'open' if control set disabled", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param9smoke",    type: "bool", title: styleText("React to smoke alarm", parameterDesc.style), description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param9co",       type: "bool", title: styleText("React to CO alarm", parameterDesc.style), description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param9co2",      type: "bool", title: styleText("React to CO₂ alarm", parameterDesc.style), description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param9heat",     type: "bool", title: styleText("React to heat alarm", parameterDesc.style), description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param9water",    type: "bool", title: styleText("React to water alarm", parameterDesc.style), description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param9access",   type: "bool", title: styleText("React to access control alarm", parameterDesc.style), description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param9security", type: "bool", title: styleText("React to home security alarm", parameterDesc.style), description: "", defaultValue: false, required: false, displayDuringSetup: false)
            
            input(name: "param10", type: "enum", title: getParameterTitle(10), description: getParameterHelp(10), options: param10options, multiple: false, defaultValue: param10options[0], required: false, displayDuringSetup: false)
            if(2 == param10options.indexOf(param10)) {
                def parameterDesc10 = getParameterDesc(10)
                input(name: "param10time", type: "number", title: styleText("Alarm timeout (S)", parameterDesc10.style), description: "", range: "10..255", defaultValue: "10", required: false, displayDuringSetup: false)
            }
        }

        input(name: "param18", type: "number", title:getParameterTitle(18), description: getParameterHelp(18), range: "1..9", defaultValue: "2", required: false, displayDuringSetup: false)
        input(name: "param20", type: "enum", title: getParameterTitle(20), description: getParameterHelp(20), options: param20options, defaultValue: param20options[0], required: false, displayDuringSetup: true)
        input(name: "param80", type: "enum", title: getParameterTitle(80), description: getParameterHelp(80), options: param80options, defaultValue: param80options[0], required: false, displayDuringSetup: false)

        input(name: "param81", type: "enum", title: getParameterTitle(81), description: getParameterHelp(81), options: param81options, defaultValue: param81options[0], required: false, displayDuringSetup: true)
        if(1 == param81options.indexOf(param81)) {
            def parameterDesc = getParameterDesc(82)
            input(name: "param82from", type: "time", title: styleText("Night light mode start", parameterDesc.style), description: "", defaultValue: "18:00", required: false, displayDuringSetup: false)
            input(name: "param82to", type: "time", title: styleText("Night light mode end", parameterDesc.style), description: "", defaultValue: "08:00", required: false, displayDuringSetup: false)
        }

        input(name: "param91", type: "number", title: getParameterTitle(91), description: getParameterHelp(91), range: "0..2300", defaultValue: "0", required: false, displayDuringSetup: false)
        input(name: "param92", type: "number", title: getParameterTitle(92), description: getParameterHelp(92), range: "0..10000", defaultValue: "0", required: false, displayDuringSetup: false)
        input(name: "param93", type: "number", title: getParameterTitle(93), description: getParameterHelp(93), range: "0..100", defaultValue: "0", required: false, displayDuringSetup: false)

        if(true) {
            def parameterDesc = getParameterDesc(101)
            input(name: "param101energy", type: "bool", title: styleText("Timed report of energy", parameterDesc.style), description: "", defaultValue: true, required: false, displayDuringSetup: false)
            input(name: "param101power", type: "bool", title: styleText("Timed report of power", parameterDesc.style), description: "", defaultValue: true, required: false, displayDuringSetup: false)
            input(name: "param101voltage", type: "bool", title: styleText("Timed report of voltage", parameterDesc.style), description: "", defaultValue: true, required: false, displayDuringSetup: false)
            input(name: "param101amperage", type: "bool", title: styleText("Timed report of amperage", parameterDesc.style), description: "", defaultValue: true, required: false, displayDuringSetup: false)
        }
        input(name: "param111", type: "number", title: getParameterTitle(111), description: getParameterHelp(111), range: "600..2592000", defaultValue: "600", required: false, displayDuringSetup: false)
        // param255 -> factory reset
        
        inputLogLevel()
    }
}

def getEndpointDevice(Short ep = 0) {
    // Single endpoint only for this device
    return this
}

// ================  Parameters  =====================
void removeStateHint() {
    state.remove("hint")
}

void Blink(Number b) {
    def Number value = Math.max(Math.min(b as Integer, 255), 0);
    String cmd = setParameter( 19,  2,  value)
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

void blinkWithStatusLight(duration) {
    Blink(duration)
}

void buttonLightSetBrightness(level) {
    String cmd = encapsulate(zwave.switchMultilevelV2.switchMultilevelSet(value: level, dimmingDuration: 0))
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

void buttonLightSetColor(hsv) {
    def rgb = hubitat.helper.ColorUtils.hsvToRGB([hsv.hue, hsv.saturation, hsv.level])
    String cmd = encapsulate(zwave.switchColorV1.switchColorSet(red: rgb[0], green: rgb[1], blue: rgb[2]))
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

void buttonLock() {
    def commandList = []
    commandList << setProtectionV2String(true)
    commandList << getProtectionReportV2String()
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(commandList, 500), hubitat.device.Protocol.ZWAVE))
}

void buttonUnlock() {
    def commandList = []
    commandList << setProtectionV2String(false)
    commandList << getProtectionReportV2String()
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(commandList, 500), hubitat.device.Protocol.ZWAVE))
}

//=============================================================================================================================================================
// Z-Wave common
//=============================================================================================================================================================

// Basic V1
void zwaveEvent(hubitat.zwave.commands.basicv1.BasicReport cmd, Short ep = 0) {
    if(cmd.value > 0) {
        parse([[name:"switch", value: "on"]])
    } else {
        parse([[name:"switch", value: "off"]])
    }
    // Request meter report (Most probaly state has been changed; but due to inertia of power sensor delaing report request)
    runIn(4, getMeterReport)
}

void getBasicReport() {
    String cmd = encapsulate(zwave.basicV1.basicGet())
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

void setBasic(Boolean state) {
    String cmd = encapsulate(zwave.basicV1.basicSet(value: state ? 0xFF : 0x00))
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

//=============================================================================================================================================================
// Z-Wave device-specific
//=============================================================================================================================================================

// Application status
//    see 'encapsulation1'

// Association grp info
//     not used atm

// Association V2
//    see 'association1'

// Clock
//    see 'clock1'

// Configuration V1
void zwaveEvent(hubitat.zwave.commands.configurationv1.ConfigurationReport cmd, Short ep = 0) {
    logDebug("(${ep}) Configuration V1 report: ${cmd}")
    
    def parameterState = [value: cmd.scaledConfigurationValue]
    
    def options = null
    if(cmd.parameterNumber == 8) {
        options = param8options
    } else if(cmd.parameterNumber == 10) {
        options = param10options
    } else if(cmd.parameterNumber == 20) {
        options = param20options
    } else if(cmd.parameterNumber == 80) {
        options = param80options
    } else if(cmd.parameterNumber == 81) {
        options = param81options
    }
    
    if(cmd.parameterNumber == 9) {
        def boolean trigger    = (cmd.scaledConfigurationValue & 1) > 0
        def boolean smokeAlarm = (cmd.scaledConfigurationValue & 256) > 0
        def boolean coAlarm    = (cmd.scaledConfigurationValue & 512) > 0
        def boolean co2Alarm   = (cmd.scaledConfigurationValue & 1024) > 0
        def boolean heatAlarm  = (cmd.scaledConfigurationValue & 2048) > 0
        def boolean waterAlarm = (cmd.scaledConfigurationValue & 4096) > 0
        def boolean acAlarm    = (cmd.scaledConfigurationValue & 8192) > 0
        def boolean hsAlarm    = (cmd.scaledConfigurationValue & 16384) > 0        
        parameterState.desc = decorateState("On ${trigger?"closed":"open"} state, Smoke=${smokeAlarm}, CO=${coAlarm}, CO2=${co2Alarm}, Heat=${heatAlarm}, Water=${waterAlarm}, Access=${acAlarm}, Security=${hsAlarm}", cmd.parameterNumber)
        device.updateSetting("param${cmd.parameterNumber}state" as String, [type: "bool", value: trigger])
        device.updateSetting("param${cmd.parameterNumber}smoke" as String, [type: "bool", value: smokeAlarm])
        device.updateSetting("param${cmd.parameterNumber}co" as String, [type: "bool", value: coAlarm])
        device.updateSetting("param${cmd.parameterNumber}co2" as String, [type: "bool", value: co2Alarm])
        device.updateSetting("param${cmd.parameterNumber}heat" as String, [type: "bool", value: heatAlarm])
        device.updateSetting("param${cmd.parameterNumber}water" as String, [type: "bool", value: waterAlarm])
        device.updateSetting("param${cmd.parameterNumber}access" as String, [type: "bool", value: acAlarm])
        device.updateSetting("param${cmd.parameterNumber}security" as String, [type: "bool", value: hsAlarm])
    } else if(cmd.parameterNumber == 10) {
        def option = cmd.scaledConfigurationValue < 2 
                        ? "${options[cmd.scaledConfigurationValue]}"
                        : "${options[2]} ${cmd.scaledConfigurationValue} S"
        parameterState.desc = decorateState("${option}", cmd.parameterNumber)
        device.updateSetting("param${cmd.parameterNumber}" as String, [type: "enum", value: options[Math.min(2, cmd.scaledConfigurationValue)]])
        device.updateSetting("param${cmd.parameterNumber}time" as String, [type: "number", value: Math.max(10, cmd.scaledConfigurationValue)])
    } else if(cmd.parameterNumber == 82) {
        // decode time interval
        def from = Calendar.instance
        def to = Calendar.instance
        def bytes = cmd.scaledConfigurationValue.toByteArray();
        from.set(hourOfDay: bytes[0] as Integer, minute: bytes[1] as Integer);
        to.set(hourOfDay: bytes[2] as Integer, minute: bytes[3] as Integer);
        parameterState.desc = decorateState("${from.format("HH:mm")} - ${to.format("HH:mm")}", cmd.parameterNumber)
        device.updateSetting("param${cmd.parameterNumber}from", [type: "time",value: from.getTime()])
        device.updateSetting("param${cmd.parameterNumber}to", [type: "time",value: to.getTime()]) 
    } else if(cmd.parameterNumber == 101) {
        def boolean energy   = (cmd.scaledConfigurationValue & 1) > 0
        def boolean power    = (cmd.scaledConfigurationValue & 2) > 0
        def boolean amperage = (cmd.scaledConfigurationValue & 8) > 0
        def boolean voltage  = (cmd.scaledConfigurationValue & 4) > 0
        parameterState.desc = decorateState("Energy=${energy}, Power=${power}, Amperage=${amperage}, Voltage=${voltage}", cmd.parameterNumber)
        device.updateSetting("param${cmd.parameterNumber}energy" as String, [type: "bool", value: energy])
        device.updateSetting("param${cmd.parameterNumber}power" as String, [type: "bool", value: power])
        device.updateSetting("param${cmd.parameterNumber}amperage" as String, [type: "bool", value: amperage])
        device.updateSetting("param${cmd.parameterNumber}voltage" as String, [type: "bool", value: voltage])
    } else {
        if(options != null) {
            parameterState.desc = decorateState(options[cmd.scaledConfigurationValue], cmd.parameterNumber)
            device.updateSetting("param${cmd.parameterNumber}" as String, [type: "enum", value: options[cmd.scaledConfigurationValue]])
        }
        else {
            parameterState.desc = decorateState("", cmd.parameterNumber)
            if(cmd.parameterNumber == 3 || cmd.parameterNumber == 4 || cmd.parameterNumber == 90) {
                device.updateSetting("param${cmd.parameterNumber}" as String, [type: "bool", value: cmd.scaledConfigurationValue])
            }
            else {
                device.updateSetting("param${cmd.parameterNumber}" as String, [type: "number", value: cmd.scaledConfigurationValue])
            }
        }
    }
    
    state["parameter${cmd.parameterNumber}"] = parameterState
}

String setParameter(paramId, size, value) {
    return encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
}

String setParameter(paramId) {
    def options = null
    if(paramId == 8) {
        options = param8options
    } else if(paramId == 10) {
        options = param10options
    } else if(paramId == 20) {
        options = param20options
    } else if(paramId == 80) {
        options = param80options
    } else if(paramId == 81) {
        options = param81options
    }
 
    def String cmd = ""
    if(paramId == 9) {
        if(param9state != null &&  param9smoke != null && 
           param9co != null &&     param9co2 != null && 
           param9heat != null &&   param9water != null && 
           param9access != null && param9security != null) {

            def Number value = ((param9state as boolean) ? 1 : 0) +
                ((param9smoke as boolean) ? 256 : 0) +
                ((param9co as boolean) ? 512 : 0) +
                ((param9co2 as boolean) ? 1024 : 0) +
                ((param9heat as boolean) ? 2048 : 0) +
                ((param9water as boolean) ? 4096 : 0) +
                ((param9access as boolean) ? 8192 : 0) +
                ((param9security as boolean) ? 16384 : 0)

            if(state["parameter${paramId}"]?.value != value) {
                def Number size = parameterDescription[paramId].size
                cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
            }
        } 
    } else if(paramId == 82) {
        if(param82from != null && param82to != null) {
            def from  = Date.parse("yyy-MM-dd'T'HH:mm:ss.SSSZ", param82from).toCalendar()
            def to    = Date.parse("yyy-MM-dd'T'HH:mm:ss.SSSZ", param82to).toCalendar()

            def byte[] bytes = [ from.get(Calendar.HOUR_OF_DAY) as byte, from.get(Calendar.MINUTE) as byte, to.get(Calendar.HOUR_OF_DAY) as byte, to.get(Calendar.MINUTE) as byte ]
            def Number value = new BigInteger(bytes).intValue()

            if(state["parameter${paramId}"]?.value != value) {
                def Number size = parameterDescription[paramId].size
                cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
            }

        }
    } else if(paramId == 101) {
        if(param101energy != null && param101power != null && param101voltage != null && param101amperage != null) {
            def Number value = ((param101energy as boolean) ? 1 : 0) +
                ((param101power as boolean) ? 2 : 0) +                
                ((param101amperage as boolean) ? 8 : 0) +
                ((param101voltage as boolean) ? 4 : 0)

            if(state["parameter${paramId}"]?.value != value) {
                def Number size = parameterDescription[paramId].size
                cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
            }
        }
    } else {
        //--------- ToDo: --------------  
        def inputValue = this.settings["param${paramId}"]
        if(inputValue != null) {
            if(options != null) {
                def Number value = options.indexOf(inputValue)
                if(paramId == 10) {
                    if(value > 1) {
                        if(param10time != null) {
                            value = param10time as Number
                        }      
                    }
                }
                if(state["parameter${paramId}"]?.value != value) {
                    def Number size = parameterDescription[paramId].size
                    cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
                }
            } else {
                def Number value = scaleParameterFromInput(paramId, inputValue as BigDecimal)
                if(state["parameter${paramId}"]?.value != value) {
                    def Number size = parameterDescription[paramId].size
                    cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
                }
            }
        }
    }
    
    return cmd
}

String getParameterReport(paramId) {
    return encapsulate(zwave.configurationV1.configurationGet(parameterNumber: paramId))    
}

// Device reset locally
//     not used atm

// Firmware update md V4
//     not used atm

// Manufacturer specific V2
//     not used atm

// Meter V4
void getMeterReport() {
    def cmds = delayBetween([
        encapsulate(zwave.meterV4.meterGet(scale: 5)),
        encapsulate(zwave.meterV4.meterGet(scale: 4)),
        encapsulate(zwave.meterV4.meterGet(scale: 2)),
        encapsulate(zwave.meterV4.meterGet(scale: 0))
    ], 500)
    sendHubCommand(new hubitat.device.HubMultiAction(cmds, hubitat.device.Protocol.ZWAVE))
}

void resetMeter() {
    runIn(1, getMeterReport)
    String cmd = encapsulate(zwave.meterV4.meterReset())
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))    
}

// Notification V4

// NOTIFICATION_TYPE_POWER_MANAGEMENT
//    State idle            0x00
//    Over-current detected 0x06
//    Over-load detected    0x08
// NOTIFICATION_TYPE_SYSTEM
//     State idle                                                                   0x00
//     System hardware failure (manufacturer proprietary failure code provided)	    0x03
void zwaveEvent(hubitat.zwave.commands.notificationv4.NotificationReport cmd, Short ep = 0) {
    zwaveNotificationEvent("V4", cmd, ep)
    
    if(cmd.notificationType == cmd.NOTIFICATION_TYPE_POWER_MANAGEMENT) {
        if(cmd.event == 0x00) {
            parse([
                 [name:"alarmOverCurrent", value: "idle"],
                 [name:"alarmOverLoad", value: "idle"]
            ])
        } else if(cmd.event == 0x06) {
            parse([[name:"alarmOverCurrent", value: "alarm"]])
        } else if(cmd.event == 0x08) {
            parse([[name:"alarmOverLoad", value: "alarm"]])
        }
    } else if (cmd.notificationType == cmd.NOTIFICATION_TYPE_SYSTEM) {
        if(cmd.event == 0x03) {
            // Issued when built-in unrecoverable temperature fuse detected the internal temperature exceeds the limit and disconnect.
            parse([[name:"alarmHardwareFailure", value: "failure"]])
        }
    }
}

private void zwaveAlarmSendNotification(def type, def event) {
    def typeIndex = type
    def stateIndex = getNotificationEvent(typeIndex, event)
    
    String cmd = encapsulate(zwave.notificationV4.notificationReport(notificationType: typeIndex, event: stateIndex))
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

void zwaveSendSmokeAlarmNotification(def event)    { zwaveAlarmSendNotification(1, event) }
void zwaveSendCOAlarmNotification(def event)       { zwaveAlarmSendNotification(2, event) }
void zwaveSendCO2AlarmNotification(def event)      { zwaveAlarmSendNotification(3, event) }
void zwaveSendHeatAlarmNotification(def event)     { zwaveAlarmSendNotification(4, event) }
void zwaveSendWaterAlarmNotification(def event)    { zwaveAlarmSendNotification(5, event) }
void zwaveSendAccessControlNotification(def event) { zwaveAlarmSendNotification(6, event) }
void zwaveSendHomeSecurityNotification(def event)  { zwaveAlarmSendNotification(7, event) }

// Power level
//    not used atm (radio power level info)

// Protection V2
//    not used atm

// Scene activation
//    not used atm

// Scene activation conf
//    not used atm

// Security
//    see 'encapsulation1' library

// Security 2
//    not used atm; need more info

// Supervision
//    see 'encapsilation1'

// Switch binary
void zwaveEvent(hubitat.zwave.commands.switchbinaryv1.SwitchBinaryReport cmd, Short ep = 0) {
    if(cmd.value == 0xFF) {
        parse([[name:"switch", value: "on"]])
    } else if(cmd.value == 0x00) {
        parse([[name:"switch", value: "off"]])
    } else {
        logWarn "Switch binary report unknown state ${cmd.value}"
    }
    // Request meter report (Most probaly state has been changed; but due to inertia of power sensor delaying report request)
    runIn(4, getMeterReport)
}


void setSwitchBinary(boolean state) {
    String cmd = encapsulate(zwave.switchBinaryV1.switchBinarySet(switchValue: state ? 0xFF : 0x00))
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

void getSwitchBinaryReport() {
    String cmd = encapsulate(zwave.switchBinaryV1.switchBinaryGet())
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

// Switch color
//    not used atm; commands get rejected - looks like has no effect

// Switch multilevel V2
//    not used atm

// Transport service V2

// Version V2
//    see 'version1'
void updateVersionInfo() {
    sendHubCommand(new hubitat.device.HubAction(getVersionReportCommand(), hubitat.device.Protocol.ZWAVE))
}

// Zwaveplus info V2
//    not used atm

void zwaveGroupAddNodeToGroup(String enumValue, def nodeId) {
    int groupNumber = (nodeGroups.indexOf(enumValue) as int) + 2 // offset by one (groups start from '1') and skip 'Lifeline' group
    addDeviceNodeToGroup(groupNumber, nodeId)    
}

void zwaveGroupRemoveNodeFromGroup(String enumValue, def nodeId) {
    int effectnumber = (nodeGroups.indexOf(enumValue) as int) + 2 // offset by one (groups start from '1') and skip 'Lifeline' group
    removeDeviceNodeFromGroup(groupNumber, nodeId)    
}

void resetAllParameters() { 
    // not a factory reset
    
    // Any value 
    //    except 0x55555555 which is full factory reset!!
    //
    String cmd = setParameter(0xFF, 4, 0x00000001)
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
    runIn(1, updatePreferencesFromDevice)    
}

void updatePreferencesFromDevice() {
    def configurationCommands = []
    
    for ( entry in parameterDescription ) {
        configurationCommands << getParameterReport(entry.key) 
    }
    
    configurationCommands.addAll(getDeviceGroupNodesCommandList())
    
    state.clear()
    state.hint = "<b style='color:red;'>Not all parameters are reported. Please, refresh this page in a few seconds</b>"
    state.info = "<b style='color:black;'>Button light brightness/color can be changed only if indicator mode is NOT set to 'Disabled'. <p>The change is applied to the current state only (either ON or OFF, Day or Night).</b>"
    runIn(20, removeStateHint)    
    
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(configurationCommands, 500), hubitat.device.Protocol.ZWAVE))
}

void validateClockSettings() {
    sendHubCommand(new hubitat.device.HubAction(getClockReportString(), hubitat.device.Protocol.ZWAVE))
}


//=============================================================================================================================================================
// Own interface
//=============================================================================================================================================================

void configure() {
    parse([
        [name: "alarmOverCurrent",     value: "idle",    descriptionText: "Initial value"],
        [name: "alarmOverLoad",        value: "idle",    descriptionText: "Initial value"],
        [name: "alarmHardwareFailure", value: "idle",    descriptionText: "Initial value"],
        [name: "protectionLocal",      value: "unknown", descriptionText: "Initial value"],
        [name: "protectionRf",         value: "unknown", descriptionText: "Initial value"],
        [name: "switch",               value: "unknown", descriptionText: "Initial value"],
        [name: "amperage",             value: "0",       descriptionText: "Initial value"],
        [name: "energy",               value: "0",       descriptionText: "Initial value"],
        [name: "power",                value: "0",       descriptionText: "Initial value"],
        [name: "voltage",              value: "0",       descriptionText: "Initial value"]
    ])
    
    updateVersionInfo()
    updatePreferencesFromDevice()    
}

void initialize() {    
    refresh()
}

void checkProtection() {
    def cmd = getProtectionReportV2String()
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

void refresh() {
    // depending on configured report type use either 'Basic' class or 'Switch Binary' class (just for consistency). 'Scene Activation'?
    if(param80 == param80options[2]) {
        getSwitchBinaryReport()
    } else {
        getBasicReport()
    }
    
    runIn(2, validateClockSettings)
	
	runIn(4, checkProtection)
}

void off() {
    // depending on configured report type use either 'Basic' class or 'Switch Binary' class (just for consistency). 'Scene Activation'?
    if(param80 == param80options[2]) {
        setSwitchBinary(false)
    } else {
        setBasic(false)
    }
}

void on() {
    // depending on configured report type use either 'Basic' class or 'Switch Binary' class (just for consistency). 'Scene Activation'?
    if(param80 == param80options[2]) {
        setSwitchBinary(true)
    } else {
        setBasic(true)
    }
}

void updated() {
    // Send all parameters one by one
    def configurationCommands = []
    
    for ( entry in parameterDescription ) {
        configurationCommands << setParameter(entry.key)
    }
    
    configurationCommands.removeAll("")
            
    for ( entry in parameterDescription ) {
        configurationCommands << getParameterReport(entry.key) 
    }
    
    configurationCommands.addAll(getDeviceGroupNodesCommandList())
    
    state.clear()
    state.hint = "<b style='color:red;'>Not all parameters are reported. Please, refresh this page in a few seconds</b>"
    state.info = "<b style='color:black;'>Button light brightness/color can be changed only if indicator mode is NOT set to 'Disabled'. <p>The change is applied to the current state only (either ON or OFF, Day or Night).</b>"
    runIn(20, removeStateHint)
    
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(configurationCommands, 500), hubitat.device.Protocol.ZWAVE))    
}
