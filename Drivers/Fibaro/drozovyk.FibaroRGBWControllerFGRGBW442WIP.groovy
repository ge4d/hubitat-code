// RGBW mode (R,G,B,W) * Brightness (brightness is a separate 5th attribute)
//   Input can control output only with the same number (e.g. switch connected to IN1 controls load connected to output OUT1). Perform following actions on inputs to change state of the connected load:
//        1xclick – change to the opposite one (ON/OFF)
//            A single click switches between the OFF state and the last non-OFF state. It means it may set the colour back, but the light may not light up as it doesn’t change the brigthness.
//        2xclick – set colour to 100%
//            hold/release – dimm/brighten colour
//
// HSB mode ([H,S -> R, G, B], W) * Brightness (brightness is a HSB component)
//    Inputs correspond to different components in HSB colour space: 
//        IN1 – Hue
//        IN2 – Saturation 
//        IN3 – Brightness
//        IN4 input controls OUT4 output. 
//    Perform following actions on inputs to change values of the components:
//        1xclick when value is 0 – restore last set value
//        1xclick when value is not 0 – set value to 0
//        2xclick – set value to max
//        hold/release – increase/decrease value
//        When you hold the button connected to IN1, you revolve around the cone on the H (Hue) axis.
//        When you hold the button connected to IN2, you change the saturation (S).
//        When you hold the button connected to the IN3, you change the brightness (B).
//
// The device provides the association of 10 groups:
//    1st association group – “Lifeline” reports the device status and allows for assigning single device only (main controller by default).
//    2nd association group – “RGBW Sync” allows to synchronize state of other FIBARO RGBW Controller devices (FGRGBW-442 and FGRGBWM-441) – do not use with other devices.
//    3rd association group – “On/Off (IN1)” is used to turn the associated devices on/off reflecting IN1 operation.
//    4th association group – “Dimmer (IN1)” is used to change level of associated devices reflecting IN1 operation.
//    5th association group – “On/Off (IN2)” is used to turn the associated devices on/off reflecting IN2 operation.
//    6th association group – “Dimmer (IN2)” is used to change level of associated devices reflecting IN2 operation.
//    7th association group – “On/Off (IN3)” is used to turn the associated devices on/off reflecting IN3 operation.
//    8th association group – “Dimmer (IN3)” is used to change level of associated devices reflecting IN3 operation.
//    9th association group – “On/Off (IN4)” is used to turn the associated devices on/off reflecting IN4 operation.
//   10th association group – “Dimmer (IN4)” is used to change level of associated devices reflecting IN4 operation.
// The device allows to control 5 regular or multichannel devices per an association group, with the exception of “LifeLine” that is reserved solely for the controller and hence only 1 node can be assigned.
//  Assocation groups mapping
//    Root                    Endpoint        Association group in endpoint
// Association Group 1     Endpoint 1-9*          Association Group 1
// Association Group 2     Endpoint 1             Association Group 2
// Association Group 3     Endpoint 2             Association Group 2
// Association Group 4     Endpoint 2             Association Group 3
// Association Group 5     Endpoint 3             Association Group 2
// Association Group 6     Endpoint 3             Association Group 3
// Association Group 7     Endpoint 4             Association Group 2
// Association Group 8     Endpoint 4             Association Group 3
// Association Group 9     Endpoint 5             Association Group 2
// Association Group 10    Endpoint 5             Association Group 3
//
// RGBW mode: commands sent to association groups for parameter 150 set to 0
//                1,2 click                        Hold                                        Release
// Input 1
//            Switch Color Set: 2nd                                                Switch Color Set: 2nd
//            Basic Set: 3rd
//            Multilevel Set: 4th        Multilevel Start Level Change: 4th        Multilevel Stop Level Change: 4th
// Input 2
//            Switch Color Set: 2nd                                                Switch Color Set: 2nd
//            Basic Set: 5th, 
//            Multilevel Set: 6th        Multilevel Start Level Change: 6th        Multilevel Stop Level Change: 6th
// Input 3
//            Switch Color Set: 2nd                                                Switch Color Set: 2nd
//            Basic Set: 7th
//            Multilevel Set: 8th        Multilevel Start Level Change: 8th        Multilevel Stop Level Change: 8th
// Input 4
//            Switch Color Set: 2nd                                                Switch Color Set: 2nd
//            Basic Set: 9th
//            Multilevel Set: 10th       Multilevel Start Level Change: 10th       Multilevel Stop Level Change: 10th
//
// HSB mode: commands sent to association groups for parameter 150 set to 1
//
// Input 1, 2, 3
//            Switch Color Set: 2nd                                                Switch Color Set: 2nd
// Input 4
//            Switch Color Set: 2nd                                                Switch Color Set: 2nd
//            Basic Set: 9th
//            Multilevel Set: 10th       Multilevel Start Level Change: 10th       Multilevel Stop Level Change: 10th
//
// Supported Command Classes:
//        COMMAND_CLASS_APPLICATION_STATUS          [0x22] V1
//        COMMAND_CLASS_ASSOCIATION                 [0x85] V2 YES
//        COMMAND_CLASS_ASSOCIATION_GRP_INFO        [0x59] V2 YES
//        COMMAND_CLASS_BASIC                       [0x20] V1 YES
//        COMMAND_CLASS_CENTRAL_SCENE               [0x5B] V3 YES
//        COMMAND_CLASS_COLOR_SWITCH                [0x33] V3 YES
//        COMMAND_CLASS_CONFIGURATION               [0x70] V1 YES
//        COMMAND_CLASS_CRC_16_ENCAP                [0x56] V1
//        COMMAND_CLASS_DEVICE_RESET_LOCALLY        [0x5A] V1 YES
//        COMMAND_CLASS_FIRMWARE_UPDATE_MD          [0x7A] V4 YES
//        COMMAND_CLASS_MANUFACTURER_SPECIFIC       [0x72] V2 YES
//        COMMAND_CLASS_METER                       [0x32] V3 YES
//        COMMAND_CLASS_MULTI_CHANNEL               [0x60] V4 YES
//        COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION   [0x8E] V3 YES
//        COMMAND_CLASS_NOTIFICATION                [0x71] V8 YES
//        COMMAND_CLASS_POWERLEVEL                  [0x73] V1 YES
//        COMMAND_CLASS_PROTECTION                  [0x75] V2 YES
//        COMMAND_CLASS_SECURITY                    [0x98] V1
//        COMMAND_CLASS_SECURITY_2                  [0x9F] V1
//        COMMAND_CLASS_SENSOR_MULTILEVEL           [0x31] V11 YES
//        COMMAND_CLASS_SUPERVISION                 [0x6C] V1
//        COMMAND_CLASS_SWITCH_MULTILEVEL           [0x26] V4 YES
//        COMMAND_CLASS_TRANSPORT_SERVICE           [0x55] V2
//        COMMAND_CLASS_VERSION                     [0x86] V2 YES
//        COMMAND_CLASS_ZWAVEPLUS_INFO              [0x5E] V2
//    ROOT (Endpoint 1)
//        COMMAND_CLASS_APPLICATION_STATUS
//        COMMAND_CLASS_ASSOCIATION
//        COMMAND_CLASS_ASSOCIATION_GRP_INFO
//        COMMAND_CLASS_COLOR_CONTROL/SWITCH
//        COMMAND_CLASS_METER
//        COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION
//        COMMAND_CLASS_NOTIFICATION
//        COMMAND_CLASS_PROTECTION
//        COMMAND_CLASS_SECURITY
//        COMMAND_CLASS_SECURITY_2
//        COMMAND_CLASS_SUPERVISION
//        COMMAND_CLASS_SWITCH_MULTILEVEL
//        COMMAND_CLASS_ZWAVEPLUS_INFO
//    Endpoint 2, 3 ,4 and 5
//        COMMAND_CLASS_APPLICATION_STATUS
//        COMMAND_CLASS_ASSOCIATION
//        COMMAND_CLASS_ASSOCIATION_GRP_INFO
//        COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION
//        COMMAND_CLASS_SECURITY
//        COMMAND_CLASS_SECURITY_2
//        COMMAND_CLASS_SUPERVISION
//        COMMAND_CLASS_SWITCH_MULTILEVEL
//        COMMAND_CLASS_ZWAVEPLUS_INFO
//    Endpoint 6, 7, 8 and 9
//        COMMAND_CLASS_APPLICATION_STATUS
//        COMMAND_CLASS_ASSOCIATION
//        COMMAND_CLASS_ASSOCIATION_GRP_INFO
//        COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION
//        COMMAND_CLASS_SECURITY
//        COMMAND_CLASS_SECURITY_2
//        COMMAND_CLASS_SENSOR_MULTILEVEL
//        COMMAND_CLASS_SUPERVISION
//        COMMAND_CLASS_ZWAVEPLUS_INFO
//
// Basic commands mapping: 
// Basic Set     Root=Ep1    Switch Multilevel Set  1–5
// Basic Get     Root=Ep1    Switch Multilevel Get  1-5, Sensor Multilevel Get 6-9
// Basic Report  Root=Ep1    Switch Multilevel Report 1-5, Sensor Multilevel Report 6-9

#include drozovyk.association1
#include drozovyk.color1
#include drozovyk.common1
#include drozovyk.encapsulation1
#include drozovyk.meter1
#include drozovyk.notification1
#include drozovyk.sensor1
#include drozovyk.version1
#include drozovyk.zwave1

import groovy.transform.Field

// Makes sense to use 2 configurations:
//  RGBW and Multichannel
//
//  RGBW can operate as RGBW, RGB, CT

@Field static List<String> colorChannelNames = ["channel1", "channel2", "channel3", "channel4"]
@Field static List<String> inputChannelNames = ["channel1", "channel2", "channel3", "channel4"]
@Field static List<String> outputModes = ["Dimmer x4", "CT", "CT/RGB", "CT/RGBW"]
@Field static List<String> param1options = ["Off", "Last state"]
@Field static List<String> param20options = ["Analog input (Sensor Multilevel)", "Analog input with pull-up (Sensor Multilevel)", "Momentary switch (Central Scene)", "Toggle switch: input change (Central Scene)", "Toggle switch: On/Off (Central Scene)"]
@Field static List<String> param30options = ["None", "Turn off", "Turn on", "Blink"]
@Field static List<String> param150options = ["R+G+B+W", "HSV+W"]
@Field static List<String> param157options = ["None", "User 1", "User 2", "User 3", "User 4", "User 5", "Fireplace", "Storm", "Rainbow", "Aurora", "Police"]

static List CalibrationWhite() {
    // Should be 6500K or [255, 255, 255]. But 4100 gives better result
    // ToDo: check for different RGB LED strips; check for RGBW strips 
    return temperatureColorRGB(4100, 100)
}

@Field static parameterDescription = [
    (  1): [title: "Action in case of power out",
            help: "",
            size: 1,
            state: "Action in case of power out",
            style: "font-weight:bold;"],
    ( 20): [title: "Input 1 mode",
            help: "'Analog input' mode spawns child device for a channel<br>'* switch' mode controls central scene",
            size: 1,
            state: "Input 1 mode: ",
            style: "color:mediumorchid; font-weight:bold;"],
    ( 21): [title: "Input 2 mode",
            help: "'Analog input' mode spawns child device for a channel<br>'* switch' mode controls central scene",
            size: 1,
            state: "Input 2 mode: ",
            style: "color:mediumorchid; font-weight:bold;"],
    ( 22): [title: "Input 3 mode",
            help: "'Analog input' mode spawns child device for a channel<br>'* switch' mode controls central scene",
            size: 1,
            state: "Input 3 mode: ",
            style: "color:mediumorchid; font-weight:bold;"],
    ( 23): [title: "Input 4 mode",
            help: "'Analog input' mode spawns child device for a channel<br>'* switch' mode controls central scene",
            size: 1,
            state: "Input 4 mode: ",
            style: "color:mediumorchid; font-weight:bold;"],
    ( 30): [title: "Alarm type (1st slot)",
            help: "Choose alarm type to react to<br>(save to show available state list)",
            size: 4,
            state: "Alarm type (1st slot): ",
            style: "color:peru; font-weight:bold;"],
    ( 31): [title: "Alarm type (2nd slot)",
            help: "Choose alarm type to react to<br>(save to show available state list)",
            size: 4,
            state: "Alarm type (2nd slot): ",
            style: "color:peru; font-weight:bold;"],
    ( 32): [title: "Alarm type (3rd slot)",
            help: "Choose alarm type to react to<br>(save to show available state list)",
            size: 4,
            state: "Alarm type (3rd slot): ",
            style: "color:peru; font-weight:bold;"],
    ( 33): [title: "Alarm type (4th slot)",
            help: "Choose alarm type to react to<br>(save to show available state list)",
            size: 4,
            state: "Alarm type (4th slot): ",
            style: "color:peru; font-weight:bold;"],
    ( 34): [title: "Alarm type (5th slot)",
            help: "Choose alarm type to react to<br>(save to show available state list)",
            size: 4,
            state: "Alarm type (5th slot): ",
            style: "color:peru; font-weight:bold;"],
    ( 35): [title: "Duration of alarm signalization (S)",
            help: "Hides/shows related controls",
            size: 1,
            state: "Duration of alarm signalization (S): ",
            style: "color:green; font-weight:bold;"],
    ( 40): [title: "Input 1 - sent scenes",
            help: "",
            size: 1,
            state: "Input 1 - sent scenes: ",
            style: "color:mediumorchid; font-weight:bold;"],
    ( 41): [title: "Input 2 - sent scenes",
            help: "",
            size: 1,
            state: "Input 2 - sent scenes: ",
            style: "color:mediumorchid; font-weight:bold;"],
    ( 42): [title: "Input 3 - sent scenes",
            help: "",
            size: 1,
            state: "Input 3 - sent scenes: ",
            style: "color:mediumorchid; font-weight:bold;"],
    ( 43): [title: "Input 4 - sent scenes",
            help: "",
            size: 1,
            state: "Input 4 - sent scenes: ",
            style: "color:mediumorchid; font-weight:bold;"],
    ( 62): [title: "Power reports - periodic (S)",
            help: "",
            scale: 1,
            size: 2,
            state: "Power reports - periodic (S): ",
            style: "color:green; font-weight:bold;"],
    ( 63): [title: "Analog inputs reports and output change on input change (x100mV)",
            help: "",
            scale: 1,
            size: 2,
            state: "Analog inputs reports and output change on input change (x100mV): ",
            style: "color:green; font-weight:bold;"],
    ( 64): [title: "Analog inputs reports - periodic (S)",
            help: "",
            scale: 1,
            size: 2,
            state: "Analog inputs reports - periodic (S): ",
            style: "color:green; font-weight:bold;"],
    ( 65): [title: "Energy reports - on change (x10W)",
            help: "",
            scale: 2,
            size: 4,
            state: "Energy reports - on change (x10W): ",
            style: "color:green; font-weight:bold;"],
    ( 66): [title: "Energy reports - periodic (S)",
            help: "",
            scale: 1,
            size: 2,
            state: "Energy reports - periodic (S): ",
            style: "font-weight:bold;"],
    (150): [title: "Inputs - LED colour control mode",
            help: "",
            size: 1,
            state: "Inputs - LED colour control mode: ",
            style: "font-weight:bold;"],
    (151): [title: "Local control - transition time",
            help: "",
            size: 2,
            state: "Local control - transition time: ",
            style: "font-weight:bold;"],
    (152): [title: "Remote control - transition time",
            help: "",
            size: 2,
            state: "Remote control - transition time: ",
            style: "font-weight:bold;"],
    (154): [title: "ON frame value for single click",
            help: "",
            size: 4,
            state: "ON frame value for single click: ",
            style: "font-weight:bold;"],
    (155): [title: "OFF frame value for single click",
            help: "",
            size: 4,
            state: "OFF frame value for single click: ",
            style: "font-weight:bold;"],
    (156): [title: "ON frame value for double click",
            help: "",
            size: 4,
            state: "ON frame value for double click: ",
            style: "font-weight:bold;"]/*,
    (157): [title: "Start programmed sequence",
            help: "",
            size: 1,
            state: "Start programmed sequence: ",
            style: "font-weight:bold;"]*/
]

static def getParameterDesc(int parameter) {
    def parameterDesc = parameterDescription[parameter]
    
    if(null == parameterDesc) {
        return [title: "", help: "", state: "", style: ""]
    }
    
    return parameterDesc
}

static String getParameterHelp(int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return parameterDesc.help
}

static String decorateState(String text, int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return styleText(parameterDesc.state + text, parameterDesc.style)
}

static String decorateString(String str, int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return styleText(str, parameterDesc.style)
}

static String getParameterTitle(int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return styleText(parameterDesc.title, parameterDesc.style)
}

static def scaleParameterFromInput(int parameter, def value) {
    def parameterDesc = getParameterDesc(parameter)
    return value * parameterDesc.scale
}

static def scaleParameterToInput(int parameter, def value) {
    def parameterDesc = getParameterDesc(parameter)
    return value / parameterDesc.scale
}

@Field static List<String> alarmStates = [
    "Idle",
    "Active"
]

@Field static List<String> nodeGroups = [
    "Sync (Switch Color; for FGRGBW-441/442 nodes only)",
    "Switch 1 (Basic)",
    "Dimmer 1 (Switch Multilevel)",
    "Switch 2 (Basic)",
    "Dimmer 2 (Switch Multilevel)",
    "Switch 3 (Basic)",
    "Dimmer 3 (Switch Multilevel)",
    "Switch 4 (Basic)",
    "Dimmer 4 (Switch Multilevel)"
]
@Field static Map<Short, String> associationGroups = [
    ( 1): "groupLifeline", 
    ( 2): "groupSync",
    ( 3): "groupSwitch1",
    ( 4): "groupDimmer1",
    ( 5): "groupSwitch2",
    ( 6): "groupDimmer2",
    ( 7): "groupSwitch3",
    ( 8): "groupDimmer3",
    ( 9): "groupSwitch4",
    (10): "groupDimmer4",
]

static def getInputMode(def mode) {
    if(null != mode) {
        return param20options.indexOf(mode)
    }
    else {
        return 0
    }
}

metadata {
    definition (name: "Fibaro RGBW Controller FGRGBW-442 (WIP)", namespace: "drozovyk", author: "dmytro.rozovyk") {
        capability "Actuator"
        capability "Configuration"
        capability "EnergyMeter"
        capability "LightEffects"           // add 'effectName', 'lightEffects' attributes and 'setEffect', 'setNextEffect', 'setPreviousEffect' commands
        capability "PowerMeter"             // add 'power' attribute
        capability "Refresh"
        capability "Switch"                 // add 'switch' attribute [on, off]
        capability "SwitchLevel"            // add 'setLevel(level, duration)' command
        
        command("setEffect", [[name: "effect", description: "", type: "ENUM", constraints: param157options]])
        command("updatePreferencesFromDevice")
        command("updateVersionInfo")
        
        notificationTypeEvents.each({ id, type ->
            command("zwaveSend${type.name}Notification", [
                [name: "notificationEvent", description: "", type: "ENUM", constraints: type.events]
            ])
        })

        command("zwaveGroupAddNodeToGroup", [[name: "group", description: "", type: "ENUM", constraints: nodeGroups],[name: "node", description: "", type: "NUMBER"],[name: "enpoint", description: "", type: "NUMBER"]])
        command("zwaveGroupRemoveNodeFromGroup", [[name: "group", description: "", type: "ENUM", constraints: nodeGroups],[name: "node", description: "", type: "NUMBER"],[name: "endpoint", description: "", type: "NUMBER"]])
        
        attribute(colorChannelNames[0], "NUMBER")
        attribute(colorChannelNames[1], "NUMBER")
        attribute(colorChannelNames[2], "NUMBER")
        attribute(colorChannelNames[3], "NUMBER")
        
        attribute(inputChannelNames[0], "NUMBER")
        attribute(inputChannelNames[1], "NUMBER")
        attribute(inputChannelNames[2], "NUMBER")
        attribute(inputChannelNames[3], "NUMBER")
    }

    preferences {
        input(name: "paramOutputMode", type: "enum", title: "<b>Output mode</b>", description: "How channels are split across child devices", options: outputModes, defaultValue: outputModes[2], required: false, displayDuringSetup: true)
        input(name: "paramWhiteBalance", type: "number", title: "<b>White balance</b>", description: "Set white color temperature (for installed RGB leds)", range: "1000..12000", defaultValue: "10000", required: true, displayDuringSetup: true)

        input(name: "param01", type: "enum", title: getParameterTitle(1), description: getParameterHelp(1), options: param1options, defaultValue: param1options[0], required: false, displayDuringSetup: true)

        input(name: "param20", type: "enum", title: getParameterTitle(20), description: getParameterHelp(20), options: param20options, defaultValue: param20options[0], required: false, displayDuringSetup: false)
        input(name: "param21", type: "enum", title: getParameterTitle(21), description: getParameterHelp(21), options: param20options, defaultValue: param20options[0], required: false, displayDuringSetup: false)
        input(name: "param22", type: "enum", title: getParameterTitle(22), description: getParameterHelp(22), options: param20options, defaultValue: param20options[0], required: false, displayDuringSetup: false)
        input(name: "param23", type: "enum", title: getParameterTitle(23), description: getParameterHelp(23), options: param20options, defaultValue: param20options[0], required: false, displayDuringSetup: false)
        
        def notificationTypes = getNotificationLabelMap()
        
        input(name: "param30type", type: "enum", title: getParameterTitle(30), description: getParameterHelp(30), options: notificationTypes, defaultValue: notificationTypes[0], required: false, displayDuringSetup: false)
        if(getNotificationTypeByLabel(param30type) > 0) {
            input(name: "param30state", type: "enum", title: decorateString("Alarm state (1st slot)", 30), description: "Choose alarm state to react to<br>(choose type and save to update this list)", options: notificationTypeEvents[getNotificationType(param30type)], defaultValue: notificationTypeEvents[getNotificationType(param30type)][0xFF], required: false, displayDuringSetup: false)
            input(name: "param30parameter", type: "number", title: decorateString("Alarm state parameter (1st slot)", 30), description: "Set alarm state parameter value to react to<br>(see z-wave notification spec for details)", range: "0..255", defaultValue: "0", required: false, displayDuringSetup: false)
            input(name: "param30action", type: "enum", title: decorateString("Alarm reaction (1st slot)", 30), description: "", options: param30options, defaultValue: param30options[0], required: false, displayDuringSetup: false)
        }
        
        input(name: "param31type", type: "enum", title: getParameterTitle(31), description: getParameterHelp(31), options: notificationTypes, defaultValue: notificationTypes[0], required: false, displayDuringSetup: false)
        if(getNotificationTypeByLabel(param31type) > 0) {
            input(name: "param31state", type: "enum", title: decorateString("Alarm state (2nd slot)", 31), description: "Choose alarm state to react to<br>(choose type and save to update this list)", options: notificationTypeEvents[getNotificationType(param31type)], defaultValue: notificationTypeEvents[getNotificationType(param31type)][0xFF], required: false, displayDuringSetup: false)
            input(name: "param31parameter", type: "number", title: decorateString("Alarm state parameter (2nd slot)", 31), description: "Set alarm state parameter value to react to<br>(see z-wave notification spec for details)", range: "0..255", defaultValue: "0", required: false, displayDuringSetup: false)
            input(name: "param31action", type: "enum", title: decorateString("Alarm reaction (2nd slot)", 31), description: "", options: param30options, defaultValue: param30options[0], required: false, displayDuringSetup: false)
        }
        
        input(name: "param32type", type: "enum", title: getParameterTitle(32), description: getParameterHelp(32), options: notificationTypes, defaultValue: notificationTypes[0], required: false, displayDuringSetup: false)
        if(getNotificationTypeByLabel(param32type) > 0) {
            input(name: "param32state", type: "enum", title: decorateString("Alarm state (3rd slot)", 32), description: "Choose alarm state to react to<br>(choose type and save to update this list)", options: notificationTypeEvents[getNotificationType(param32type)], defaultValue: notificationTypeEvents[getNotificationType(param32type)][0xFF], required: false, displayDuringSetup: false)
            input(name: "param32parameter", type: "number", title: decorateString("Alarm state parameter (3rd slot)", 32), description: "Set alarm state parameter value to react to<br>(see z-wave notification spec for details)", range: "0..255", defaultValue: "0", required: false, displayDuringSetup: false)
            input(name: "param32action", type: "enum", title: decorateString("Alarm reaction (3rd slot)", 32), description: "", options: param30options, defaultValue: param30options[0], required: false, displayDuringSetup: false)
        }
        
        input(name: "param33type", type: "enum", title: getParameterTitle(33), description: getParameterHelp(33), options: notificationTypes, defaultValue: notificationTypes[0], required: false, displayDuringSetup: false)
        if(getNotificationTypeByLabel(param33type) > 0) {
            input(name: "param33state", type: "enum", title: decorateString("Alarm state (4th slot)", 33), description: "Choose alarm state to react to<br>(choose type and save to update this list)", options: notificationTypeEvents[getNotificationType(param33type)], defaultValue: notificationTypeEvents[getNotificationType(param33type)][0xFF], required: false, displayDuringSetup: false)
            input(name: "param33parameter", type: "number", title: decorateString("Alarm state parameter (4th slot)", 33), description: "Set alarm state parameter value to react to<br>(see z-wave notification spec for details)", range: "0..255", defaultValue: "0", required: false, displayDuringSetup: false)
            input(name: "param33action", type: "enum", title: decorateString("Alarm reaction (4th slot)", 33), description: "", options: param30options, defaultValue: param30options[0], required: false, displayDuringSetup: false)
        }
        
        input(name: "param34type", type: "enum", title: getParameterTitle(34), description: getParameterHelp(34), options: notificationTypes, defaultValue: notificationTypes[0], required: false, displayDuringSetup: false)
        if(getNotificationTypeByLabel(param34type) > 0) {
            input(name: "param34state", type: "enum", title: decorateString("Alarm state (5th slot)", 34), description: "Choose alarm state to react to<br>(choose type and save to update this list)", options: notificationTypeEvents[getNotificationType(param34type)], defaultValue: notificationTypeEvents[getNotificationType(param34type)][0xFF], required: false, displayDuringSetup: false)
            input(name: "param34parameter", type: "number", title: decorateString("Alarm state parameter (5th slot)", 34), description: "Set alarm state parameter value to react to<br>(see z-wave notification spec for details)", range: "0..255", defaultValue: "0", required: false, displayDuringSetup: false)
            input(name: "param34action", type: "enum", title: decorateString("Alarm reaction (5th slot)", 34), description: "", options: param30options, defaultValue: param30options[0], required: false, displayDuringSetup: false)
        }
        
        if(getInputMode(param20) >= 2) {
            input(name: "param40keypressed1time", type: "bool", title: decorateString("Input 1: ", 40) + "<b style='color:dimgray;'>Send 'key pressed 1 time'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param40keypressed2time", type: "bool", title: decorateString("Input 1: ", 40) + "<b style='color:dimgray;'>Send 'key pressed 2 times'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param40keypressed3time", type: "bool", title: decorateString("Input 1: ", 40) + "<b style='color:dimgray;'>Send 'key pressed 3 times'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param40keyheldreleased", type: "bool", title: decorateString("Input 1: ", 40) + "<b style='color:dimgray;'>Send 'key hold down' and 'key released'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
        }
        if(getInputMode(param21) >= 2) {
            input(name: "param41keypressed1time", type: "bool", title: decorateString("Input 2: ", 41) + "<b style='color:dimgray;'>Send 'key pressed 1 time'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param41keypressed2time", type: "bool", title: decorateString("Input 2: ", 41) + "<b style='color:dimgray;'>Send 'key pressed 2 times'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param41keypressed3time", type: "bool", title: decorateString("Input 2: ", 41) + "<b style='color:dimgray;'>Send 'key pressed 3 times'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param41keyheldreleased", type: "bool", title: decorateString("Input 2: ", 41) + "<b style='color:dimgray;'>Send 'key hold down' and 'key released'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
        }
        if(getInputMode(param22) >= 2) {
            input(name: "param42keypressed1time", type: "bool", title: decorateString("Input 3: ", 42) + "<b style='color:dimgray;'>Send 'key pressed 1 time'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param42keypressed2time", type: "bool", title: decorateString("Input 3: ", 42) + "<b style='color:dimgray;'>Send 'key pressed 2 times'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param42keypressed3time", type: "bool", title: decorateString("Input 3: ", 42) + "<b style='color:dimgray;'>Send 'key pressed 3 times'</b>", description: "",defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param42keyheldreleased", type: "bool", title: decorateString("Input 3: ", 42) + "<b style='color:dimgray;'>Send 'key hold down' and 'key released'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
        }
        if(getInputMode(param23) >= 2) {
            input(name: "param43keypressed1time", type: "bool", title: decorateString("Input 4: ", 43) + "<b style='color:dimgray;'>Send 'key pressed 1 time'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param43keypressed2time", type: "bool", title: decorateString("Input 4: ", 43) + "<b style='color:dimgray;'>Send 'key pressed 2 times'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param43keypressed3time", type: "bool", title: decorateString("Input 4: ", 43) + "<b style='color:dimgray;'>Send 'key pressed 3 times'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
            input(name: "param43keyheldreleased", type: "bool", title: decorateString("Input 4: ", 43) + "<b style='color:dimgray;'>Send 'key hold down' and 'key released'</b>", description: "", defaultValue: false, required: false, displayDuringSetup: false)
        }        
        
        input(name: "param62", type: "number", title: getParameterTitle(62), description: getParameterHelp(62), range: "30..32400", defaultValue: "3600", required: false, displayDuringSetup: false)
        
        input(name: "param63", type: "number", title: getParameterTitle(63), description: getParameterHelp(63), range: "0..2300", defaultValue: "0", required: false, displayDuringSetup: false)
        input(name: "param64", type: "number", title: getParameterTitle(64), description: getParameterHelp(64), range: "30..32400", defaultValue: "3600", required: false, displayDuringSetup: false)
        
        input(name: "param65", type: "number", title: getParameterTitle(65), description: getParameterHelp(65), range: "0..10000", defaultValue: "0", required: false, displayDuringSetup: false)
        input(name: "param66", type: "number", title: getParameterTitle(66), description: getParameterHelp(66), range: "30..32400", defaultValue: "3600", required: false, displayDuringSetup: false)
        
        inputLogLevel()
    }
}

String getInputDeviceLabel(Short inputNumber)        { return "${device.label} input ${inputNumber}" }
String getInputDeviceName(Short inputNumber)         { return "${device.name}-input-${inputNumber}" }
String getInputDeviceNetworkId(Short inputNumber)    { return "${device.deviceNetworkId}-input-${inputNumber}" }
def    getInputDevice(Short inputNumber)             { return getChildDevice(getInputDeviceNetworkId(inputNumber)) }

//["channel1", "channel2", "channel3", "channel4"]
//["Dimmer x4", "CT", "CT/RGB", "CT/RGBW"]
String getOutputDeviceLabel(Short outputNumber = 0) {
    int outputMode = 0
    if(null != paramOutputMode) {
        outputMode = outputModes.indexOf(paramOutputMode)
    }
    
    switch(outputMode) {
        case 0:
            return "${device.label} output ${colorChannelNames[outputNumber]}"            
        case 1:            
            return "${device.label} output CT"
        case 2:            
            return "${device.label} output RGB"
        case 3:
            return "${device.label} output RGBW"
        default:
            logWarn "Unexpected output mode: ${paramOutputMode}. No output devices configured."
            return ""
    }    
}
String getOutputDeviceName(Short outputNumber = 1) {
    int outputMode = 0
    if(null != paramOutputMode) {
        outputMode = outputModes.indexOf(paramOutputMode)
    }
    
    switch(outputMode) {
        case 0:
            return "${device.name}-output-${colorChannelNames[outputNumber]}"
        case 1:            
            return "${device.name}-output-CT"
        case 2:            
            return "${device.name}-output-RGB"
        case 3:
            return "${device.name}-output-RGBW"
        default:
            logWarn "Unexpected output mode: ${paramOutputMode}. No output devices configured."
            return ""
    }
}
String getOutputDeviceNetworkId(Short outputNumber = 1) {
    int outputMode = 0
    if(null != paramOutputMode) {
        outputMode = outputModes.indexOf(paramOutputMode)
    }
    
    switch(outputMode) {
        case 0:
            return "${device.deviceNetworkId}-output-${colorChannelNames[outputNumber]}"
        case 1:            
            return "${device.deviceNetworkId}-output-CT"
        case 2:            
            return "${device.deviceNetworkId}-output-RGB"
        case 3:
            return "${device.deviceNetworkId}-output-RGBW"
        default:
            logWarn "Unexpected output mode: ${paramOutputMode}. No output devices configured."
            return ""
    }
    return "${device.deviceNetworkId}-output-${colorChannelNames[outputNumber]}"
}
def    getOutputDevice(Short outputNumber = 1)           { return getChildDevice(getOutputDeviceNetworkId(outputNumber)) }

void configureChildInputDevices() {
    // Configure inputs (add or remove depending on setup)
    def params = [param20, param21, param22, param23]
    for(Short inputIndex = 1; inputIndex < 5; ++inputIndex) {
        def inputDevice = getInputDevice(inputIndex)
        def inputDeviceMode = getInputMode(params[inputIndex - 1])
        
        // Check if device is configured as a voltage sensor (needs corresponding wrapper)
        if(null == inputDevice && 2 > inputMode) {            
            inputDevice = addChildDevice("hubitat", "Generic Component Voltage Sensor", getInputDeviceNetworkId(inputIndex),  [isComponent: true, name: getInputDeviceName(inputIndex), label: getInputDeviceLabel(inputIndex)])
            inputDevice.updateDataValue("EP", (inputIndex + 5) as String)
            List<Map> defaultValues = [
                [name: "voltage", value: 0, descriptionText: "Initial voltage", unit: "V"]
            ]
            inputDevice.parse(defaultValues)
            logInfo("Spawned child device for the input ${inputIndex} (analog input; voltage)")
        }
    }
}

void configureChildOutputRGBW() {
    def outputDevice = addChildDevice("hubitat", "Generic Component RGBW", getOutputDeviceNetworkId(),  [isComponent: true, name: getOutputDeviceName(), label: getOutputDeviceLabel()])
    outputDevice.updateDataValue("EP", "1")
    List<Map> defaultValues = [
        [name: "colorMode", value: "CT", descriptionText: "Initial color mode"],
        [name: "colorName", value: "White", descriptionText: "Initial color name"],
        [name: "colorTemperature", value: 4100, descriptionText: "Initial temperature color value", unit: "°K"],
        [name: "hue", value: "0", descriptionText: "Initial hue"],
        [name: "level", value: 50, descriptionText: "Initial level value", unit: "%"],
        [name: "saturation", value: "0", descriptionText: "Initial saturation", unit: "%"],
        [name: "switch", value: "off", descriptionText: "Initial switch value"]
    ]
    outputDevice.parse(defaultValues)
    logInfo("Spawned RGBW child device")
}

void configureChildOutputRGB() {
    def outputDevice = addChildDevice("hubitat", "Generic Component RGBW", getOutputDeviceNetworkId(),  [isComponent: true, name: getOutputDeviceName(), label: getOutputDeviceLabel()])
    outputDevice.updateDataValue("EP", "1")
    List<Map> defaultValues = [
        [name: "colorMode", value: "CT", descriptionText: "Initial color mode"],
        [name: "colorName", value: "White", descriptionText: "Initial color name"],
        [name: "colorTemperature", value: 4100, descriptionText: "Initial temperature color value", unit: "°K"],
        [name: "hue", value: "0", descriptionText: "Initial hue"],
        [name: "level", value: 50, descriptionText: "Initial level value", unit: "%"],
        [name: "saturation", value: "0", descriptionText: "Initial saturation", unit: "%"],
        [name: "switch", value: "off", descriptionText: "Initial switch value"]
    ]
    outputDevice.parse(defaultValues)
    logInfo("Spawned RGB child device")
}

void configureChildOutputCT() {
    def colorOutputDevice = addChildDevice("hubitat", "Generic Component CT", getOutputDeviceNetworkId(),  [isComponent: true, name: getOutputDeviceName(), label: getOutputDeviceLabel()])
    colorOutputDevice.updateDataValue("EP", "1")
    List<Map> defaultValues = [
        [name: "colorName", value: "", descriptionText: "Initial color name"],
        [name: "colorTemperature", value: 4100, descriptionText: "Initial temperature color value", unit: "°K"],
        [name: "level", value: 50, descriptionText: "Initial level value", unit: "%"],
        [name: "switch", value: "off", descriptionText: "Initial switch value"]
    ]
    colorOutputDevice.parse(defaultValues)
    logInfo("Spawned CT child device ${colorChannelNames[deviceType]}")
}

void configureChildOutputMultichannel() {
    for(Short deviceType = 0; deviceType < 4; ++deviceType) {
        def outputDevice = addChildDevice("hubitat", "Generic Component Dimmer", getOutputDeviceNetworkId(deviceType),  [isComponent: true, name: getOutputDeviceName(deviceType), label: getOutputDeviceLabel(deviceType)])
        outputDevice.updateDataValue("EP", "${2 + deviceType}")
        List<Map> defaultValues = [
            [name: "level", value: 50, descriptionText: "Initial level value", unit: "%"],
            [name: "switch", value: "off", descriptionText: "Initial switch value"]
        ]
        outputDevice.parse(defaultValues)
        logInfo("Spawned Dimmer child device ${colorChannelNames[deviceType]}")
    }
}

void configureChildOutputDevices() {
    // Outputs can be: 
    //    0: "Dimmer x4" - 4 isolated dimmers
    //    1: "CT" - single 2-channel light with adjustable color temperature
    //    2: "CT/RGB" - RGBW color device with RGB color layout
    //    3: "CT/RGBW" - RGBW color device with RGB+W color layout
    
    int outputMode = 0
    if(null != paramOutputMode) {
        outputMode = outputModes.indexOf(paramOutputMode)
    }
    
    switch(outputMode) {
        case 0:
            configureChildOutputMultichannel()
            break
        case 1:            
            configureChildOutputCT()
            break
        case 2:            
            configureChildOutputRGB()
            break
        case 3:
            configureChildOutputRGBW()
            break
        default:
            logWarn "Unexpected output mode: ${paramOutputMode}. No output devices configured."
            break
    }
}

void configureChildDevices() {
    getChildDevices().each { deleteChildDevice("${it.deviceNetworkId}") }
    configureChildInputDevices()
    configureChildOutputDevices()
}

def getEndpointDevice(Short ep = 0) {
    // Single endpoint for now (used by 'meter1' and 'sensor1')
    return this
}

// ================  Parameters  =====================
void removeStateHint() {
    state.remove("hint")
}

String setParameter157() {
    def String cmd = ""
    if(param157 != null) {
        def options = param157options
        def Number value = options.indexOf(param157)
        
        if(state?.parameter157?.value != value) {
            cmd = setParameter( 157,  1,  value)
        }
    }    
    return cmd
}

//Z-Wave common

// Basic V1
void zwaveEvent(hubitat.zwave.commands.basicv1.BasicReport cmd, Short ep = 0) {
    logDebug("(${ep}) Basic V1 report: ${cmd}")
    
    if(ep < 2) {
        def eventList = []
        
        def outputDevice = getOutputDevice()
        Short endPoint = device.getDataValue("EP") as Short
        
        if(cmd.targetValue == 0) {
            eventList << [name: "switch", value: "off"]
        } else if(cmd.targetValue < 100) {
            eventList << [name: "switch", value: "on"]
        } else {
            logWarn "(${ep}) Basic report unknown state ${cmd.targetValue}"
        }
        
        if(1 == endPoint) {
            outputDevice.parse(eventList)
        }
        
        if(cmd.targetValue > 0 && cmd.targetValue < 100) {
            eventList << [name: "level", value: cmd.targetValue, descriptionText: "${outputDevice.displayName} level was set to ${cmd.targetValue}%", unit: "%"]
        }
        parse(eventList) 
    }
    else if (ep < 6) {
        def eventList = []
        
        outputDevice = getOutputDevice(ep - 2 as Short)
        
        if(null != outputDevice) {
            if(cmd.targetValue == 0) {
                eventList << [name: "switch", value: "off", descriptionText: "${outputDevice.displayName} was turned off"]                
            } else if(cmd.targetValue < 100) {
                eventList << [name: "switch", value: "on", descriptionText: "${outputDevice.displayName} was turned on"]
                eventList << [name: "level", value: cmd.targetValue, descriptionText: "${outputDevice.displayName} level was set to ${cmd.targetValue}%", unit: "%"]                
            } else {
                logWarn "(${ep}) Basic report unknown state ${cmd.targetValue}"
            }
            
            outputDevice.parse(eventList)
        }
    }
    else {
        logWarn "(${ep}) Basic report unknown endpoint ${cmd.targetValue}"
    }
}

void getBasicReport(Short ep = 1) {
    String cmd = encapsulate(zwave.basicV1.basicGet(), ep)
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

void setBasic(int state, Short ep = 1) {
    String cmd = encapsulate(zwave.basicV1.basicSet(value: state as Short), ep)
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

//Z-Wave device-specific

// Application status
//    see 'encapsulation1'

// Association grp info
//    not used atm

// Association V2
//    see 'association1'

// Central scene V3
void zwaveEvent(hubitat.zwave.commands.centralscenev3.CentralSceneNotification cmd, Short ep = 0) {
    logDebug(cmd)
    
    // Short sceneNumber
    //  input device number
    
    // Short sequenceNumber
    // Boolean slowRefresh
    
    // Short keyAttributes
    // KEY_PRESSED_1_TIME  = 0
    // KEY_RELEASED        = 1
    // KEY_HELD_DOWN       = 2
    // KEY_PRESSED_2_TIMES = 3
    // KEY_PRESSED_3_TIMES = 4
    // KEY_PRESSED_4_TIMES = 5
    // KEY_PRESSED_5_TIMES = 6    
}

// Configuration V1
void zwaveEvent(hubitat.zwave.commands.configurationv1.ConfigurationReport cmd, Short ep = 0) {
    if(cmd.parameterNumber == 1) {
        def options = param1options
        state["parameter${cmd.parameterNumber}"] = [desc:"Action in case of power out: ${options[cmd.scaledConfigurationValue]}", value: cmd.scaledConfigurationValue]
        device.updateSetting("param01", [type: "enum", value: options[cmd.scaledConfigurationValue]])
    } else if(cmd.parameterNumber >= 20 && cmd.parameterNumber <= 23) {
        def options = param20options
        state["parameter${cmd.parameterNumber}"] = [desc:"<b style='color:mediumorchid;'>Input ${cmd.parameterNumber % 10 + 1} mode: ${options[cmd.scaledConfigurationValue]}</b>", value: cmd.scaledConfigurationValue]
        device.updateSetting("param${cmd.parameterNumber}", [type: "enum", value: options[cmd.scaledConfigurationValue]])
    } else if(cmd.parameterNumber >= 30 && cmd.parameterNumber <= 34) {        
        def notificationType = ((cmd.scaledConfigurationValue as int) >> 24) & 0xFF
        def notificationState = ((cmd.scaledConfigurationValue as int) >> 16) & 0xFF
        def notificationParam = ((cmd.scaledConfigurationValue as int) >> 8) & 0xFF
        def action = (cmd.scaledConfigurationValue as int) & 0xFF
        state["parameter${cmd.parameterNumber}"] = [desc:"<b style='color:peru;'>Alarm slot ${cmd.parameterNumber % 10 + 1} configuration: ${notificationTypeEvents[notificationType].label} : ${notificationTypeEvents[notificationType].events[notificationState]} : ${notificationParam} : ${param30options[action]}</b>", value: cmd.scaledConfigurationValue]
        device.updateSetting("param${cmd.parameterNumber}type", [type: "enum", value: notificationTypeEvents[notificationType].label])
        device.updateSetting("param${cmd.parameterNumber}state", [type: "enum", value: notificationTypeEvents[notificationType].events[notificationState]])
        device.updateSetting("param${cmd.parameterNumber}parameter", [type: "number", value: notificationParam])
        device.updateSetting("param${cmd.parameterNumber}action", [type: "enum", value: param30options[action]])
    } else if(cmd.parameterNumber == 35) {
        // 0 equals 'disabled'
        state["parameter${cmd.parameterNumber}"] = [desc:"<b style='color:peru;'>Alarm duration ${cmd.scaledConfigurationValue} S</b>", value: cmd.scaledConfigurationValue]
    } else if(cmd.parameterNumber >= 40 && cmd.parameterNumber <= 43) {
        def boolean keypress1 = (cmd.scaledConfigurationValue & 1) > 0
        def boolean keypress2 = (cmd.scaledConfigurationValue & 2) > 0
        def boolean keypress3 = (cmd.scaledConfigurationValue & 4) > 0
        def boolean keyhold   = (cmd.scaledConfigurationValue & 8) > 0
        state["parameter${cmd.parameterNumber}"] = [desc:"<b style='color:mediumorchid;'>Input ${cmd.parameterNumber % 10 + 1} sent scenes:<b style='color:dimgray;'> single key press (${keypress1}), double key press (${keypress2}), tripple key press (${keypress3}), key hold/release (${keyhold})</b></b>", value: cmd.scaledConfigurationValue]
        device.updateSetting("param${cmd.parameterNumber}keypressed1time", [type: "bool", value: keypress1])
        device.updateSetting("param${cmd.parameterNumber}keypressed2time", [type: "bool", value: keypress2])
        device.updateSetting("param${cmd.parameterNumber}keypressed3time", [type: "bool", value: keypress3])
        device.updateSetting("param${cmd.parameterNumber}keyheldreleased", [type: "bool", value: keyhold])
    } else if(cmd.parameterNumber == 62) {
        // 0 equals 'disabled'
        state["parameter${cmd.parameterNumber}"] = [desc:"<b style='color:green;'>Timed report interval (S)</b>", value: cmd.scaledConfigurationValue]
    } else if(cmd.parameterNumber == 63) {
        // 0 equals 'disabled'
        state["parameter${cmd.parameterNumber}"] = [desc:"<b style='color:green;'>Analog inputs voltage report threshold ${scaledConfigurationValue} (x100mV) </b>", value: cmd.scaledConfigurationValue]
    } else if(cmd.parameterNumber == 64) {
        // 0 equals 'disabled'
        state["parameter${cmd.parameterNumber}"] = [desc:"<b style='color:green;'>Analog input voltage timed report interval (S)</b>", value: cmd.scaledConfigurationValue]
    } else if(cmd.parameterNumber == 65) {
        // 0 equals 'disabled'
        state["parameter${cmd.parameterNumber}"] = [desc:"<b style='color:green;'>Energy report threshold ${scaledConfigurationValue} (x100Wh) </b>", value: cmd.scaledConfigurationValue]
    } else if(cmd.parameterNumber == 66) {
        // 0 equals 'disabled'
        state["parameter${cmd.parameterNumber}"] = [desc:"<b style='color:green;'>Energy timed report interval (S)</b>", value: cmd.scaledConfigurationValue]
    } else if(cmd.parameterNumber == 150) {
        def options = param150options
        state["parameter${cmd.parameterNumber}"] = [desc:"<b>LED color control mode: ${options[cmd.scaledConfigurationValue]}</b>", value: cmd.scaledConfigurationValue]
    } else if(cmd.parameterNumber == 151) {
        state["parameter${cmd.parameterNumber}"] = [desc:"<b>LED color transition time (Local control; S)</b>", value: cmd.scaledConfigurationValue]
    } else if(cmd.parameterNumber == 152) {
        state["parameter${cmd.parameterNumber}"] = [desc:"<b>LED color transition time (Remote control; S)</b>", value: cmd.scaledConfigurationValue]
    } else if(cmd.parameterNumber == 154) {
        state["parameter${cmd.parameterNumber}"] = [desc:"<b>ON frame value for single click</b>", value: cmd.scaledConfigurationValue]
    } else if(cmd.parameterNumber == 155) {
        state["parameter${cmd.parameterNumber}"] = [desc:"<b>OFF frame value for single click</b>", value: cmd.scaledConfigurationValue]
    } else if(cmd.parameterNumber == 156) {
        state["parameter${cmd.parameterNumber}"] = [desc:"<b>ON frame value for double click</b>", value: cmd.scaledConfigurationValue]
    } else if(cmd.parameterNumber == 157) {
        def options = param157options
        state["parameter${cmd.parameterNumber}"] = [desc:"<b>Active programmed sequence: ${options[cmd.scaledConfigurationValue]}</b>", value: cmd.scaledConfigurationValue]
    } else {
        state["parameter${cmd.parameterNumber}"] = [value: cmd.scaledConfigurationValue]
    }
}

String setParameter(paramId) {
    def options = null
    if(paramId == 1) {
        options = param1options
    } else if(paramId >= 20 && paramId <= 23) {
        options = param20options
    } else if(paramId == 150) {
        options = param150options
    } else if(paramId == 157) {
        options = param157options
    }
    
    def String cmd = ""
    //{
        def inputValue = this.settings["param${paramId}"]
        if(inputValue != null) {
            if(options != null) {
                def Number value = options.indexOf(inputValue)
                if(state["parameter${paramId}"]?.value != value) {
                    def Number size = parameterDescription[paramId].size
                    cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
                }
            } else {
                def Number value = scaleParameterFromInput(paramId, inputValue as BigDecimal)
                if(state["parameter${paramId}"]?.value != value) {
                    def Number size = parameterDescription[paramId].size
                    cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
                }
            }
        }
    //}
    
    return cmd
}

// used above; added to command list
String setParameter(paramId, size, value) {
    return encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
}

String getParameterReport(paramId) {
    return encapsulate(zwave.configurationV1.configurationGet(parameterNumber: paramId))    
}

// Meter V3
void getMeterReport() {
    def cmds = delayBetween([
        encapsulate(zwave.meterV3.meterGet(scale: 2)),
        encapsulate(zwave.meterV3.meterGet(scale: 0))
    ], 500)
    sendHubCommand(new hubitat.device.HubMultiAction(cmds, hubitat.device.Protocol.ZWAVE))
}

void resetMeter() {
    runIn(1, getMeterReport)
    String cmd = encapsulate(zwave.meterV3.meterReset())
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))    
}

// Notification V8

// NOTIFICATION_TYPE_POWER_MANAGEMENT
//    Over-current detected 0x06
// NOTIFICATION_TYPE_SYSTEM
//     System hardware failure (manufacturer proprietary failure code provided)	    0x03
void zwaveEvent(hubitat.zwave.commands.notificationv8.NotificationReport cmd) {
    zwaveNotificationEvent("V8", cmd, ep)
    
    if(cmd.notificationType == cmd.NOTIFICATION_TYPE_POWER_MANAGEMENT) {
        if(cmd.event == 0x00) {
            parse([[name:"alarmOverCurrent", value: "idle"]])
        } else if(cmd.event == 0x06) {
            parse([[name:"alarmOverCurrent", value: "alarm"]])
        }
    } else if (cmd.notificationType == cmd.NOTIFICATION_TYPE_SYSTEM) {
        if(cmd.event == 0x03) {
            // ?? Issued when built-in unrecoverable temperature fuse detected the internal temperature exceeds the limit and disconnect.
            // overheat
            parse([[name:"alarmHardwareFailure", value: "failure"]])
        }
    }
}

private void zwaveAlarmSendNotification(def type, def event) {
    def typeIndex = type
    def stateIndex = getNotificationEvent(typeIndex, event)
    
    String cmd = encapsulate(zwave.notificationV4.notificationReport(notificationType: typeIndex, event: stateIndex))
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

// ToDo: Add missing functions
void zwaveSendSmokeAlarmNotification(def event)    { zwaveAlarmSendNotification(1, event) }
void zwaveSendCOAlarmNotification(def event)       { zwaveAlarmSendNotification(2, event) }
void zwaveSendCO2AlarmNotification(def event)      { zwaveAlarmSendNotification(3, event) }
void zwaveSendHeatAlarmNotification(def event)     { zwaveAlarmSendNotification(4, event) }
void zwaveSendWaterAlarmNotification(def event)    { zwaveAlarmSendNotification(5, event) }
void zwaveSendAccessControlNotification(def event) { zwaveAlarmSendNotification(6, event) }
void zwaveSendHomeSecurityNotification(def event)  { zwaveAlarmSendNotification(7, event) }

// Security 2
//    not used atm

// Sensor multilevel V11
//    see 'sensor1'

// Switch color V3
@Field static Short idWhite = 0
@Field static Short idRed = 2
@Field static Short idGreen = 3
@Field static Short idBlue = 4

void zwaveEvent(hubitat.zwave.commands.switchcolorv3.SwitchColorReport cmd, Short ep = 0) {
    logDebug("(${ep}) Switch color V3 report ${cmd}")
    
    if(ep < 2) {
        // The state is set with delay. So it is safer to update current component 
        // manually than rely on 'latestValue' right after sending related event
        def r = device.latestValue(colorChannelNames[0])
        def g = device.latestValue(colorChannelNames[1])
        def b = device.latestValue(colorChannelNames[2])
        def w = device.latestValue(colorChannelNames[3])
        
        def outputDevice = null
        switch(cmd.colorComponentId) {
            case idWhite:
                w = cmd.targetValue
                parse([[name: colorChannelNames[3], value: cmd.targetValue]])
                outputDevice = getOutputDevice(3 as Short)
                break            
            case idRed:
                r = cmd.targetValue
                parse([[name: colorChannelNames[0], value: cmd.targetValue]])
                outputDevice = getOutputDevice(0 as Short)
                break
            case idGreen:
                g = cmd.targetValue
                parse([[name: colorChannelNames[1], value: cmd.targetValue]])
                outputDevice = getOutputDevice(1 as Short)
                break
            case idBlue:
                b = cmd.targetValue
                parse([[name: colorChannelNames[2], value: cmd.targetValue]])
                outputDevice = getOutputDevice(2 as Short)
                break
            default:
                break
        }
        
        if(outputDevice != null) {
            List<Map> events = []
            
            Short endPoint = outputDevice.getDataValue("EP") as Short
            if(endPoint > 1) { // 2, 3, 4 or 5
                if(cmd.targetValue > 0) {
                    def level = cmd.targetValue * (99.0 / 255.0)
                    events += [name: "level", value: level, unit: "%", descriptionText: "Set 'level' to ${level}%"]
                    events += [name: "switch", value: "on", descriptionText: "Turn 'switch' on"]
                }
                else {
                    events += [name: "switch", value: "off", descriptionText: "Turn 'switch' off"]
                }
            }
            else {
                def rgb = [r, g, b]
                if(null != paramWhiteBalance) {
                    // Revert white-balance to get correct color/temperature names                    
                    def whiteBalance = paramWhiteBalance as int
                    rgb = changeWhiteBalance(rgb, CalibrationWhite(), temperatureColorRGB(whiteBalance as int, 100))
                }
                                
                def colormap = hubitat.helper.ColorUtils.rgbToHSV(rgb)
                if(null != colormap[0]) {
                    def colorName
                    def hue = ((colormap[0] as float) * 3.6) as int // [0..100.4999] 
                    def ct = (outputDevice.latestValue("colorMode") == "CT")
                    def fx = (outputDevice.latestValue("colorMode") == "EFFECTS")
                    if(ct) {
                        logDebug(colormap)
                        colorName = temperatureName(colormap)
                    }
                    else if(!fx) {
                        colorName = getColorName(colormap)
                    }
                    else {
                        colorName = "none"
                    }
                    
                    logInfo("switchColorReport RGB(${rgb[0]}, ${rgb[1]}, ${rgb[2]}) => HSL(${colormap[0]}, ${colormap[1]}, ${colormap[2]})")
                    
                    events += [name: "hue", value: colormap[0].round(0) as int, descriptionText: ""]
                    events += [name: "colorName", value: colorName, descriptionText: ""]
                }
                events += [name: "level", value: colormap[2].round(0) as int, descriptionText: ""]
                events += [name: "saturation", value: colormap[1].round(0) as int, descriptionText: ""]
            }
            
            outputDevice.parse(events)
        }
    }
    else {
        logWarn "(${ep}) Switch color report unknown endpoint ${cmd}"
    }
}

List<String> getSwitchColorReportStringList() {
    return [
        encapsulate(zwave.switchColorV3.switchColorGet(colorComponentId: idWhite)),
        encapsulate(zwave.switchColorV3.switchColorGet(colorComponentId: idRed)),
        encapsulate(zwave.switchColorV3.switchColorGet(colorComponentId: idGreen)),
        encapsulate(zwave.switchColorV3.switchColorGet(colorComponentId: idBlue))
    ]    
}

void setSwitchColor(h, s, b) {
    int outputMode = 0
    if(null != paramOutputMode) {
        outputMode = outputModes.indexOf(paramOutputMode)
    }

    // color mode is not supported for two channel LED strips
    switch(outputMode + 1) {
        case 3:
            def rgb = hubitat.helper.ColorUtils.hsvToRGB([h, s, b])
                                         
            if(null != paramWhiteBalance) {
                // LEDs are expected to have non pure white color
                // For example they might give 6500K white at [255.0, 255.0, 255.0]
                def whiteBalance = paramWhiteBalance as int
                rgb = changeWhiteBalance(rgb, temperatureColorRGB(whiteBalance as int, 100), CalibrationWhite())
            }
        
            logInfo("setSwitchColor HSL(${h}, ${s}, ${b}) => RGB(${rgb[0]}, ${rgb[1]}, ${rgb[2]})")
        
            def cmd = encapsulate(zwave.switchColorV3.switchColorSet(colorComponents: [(idRed): rgb[0] as int, (idGreen): rgb[1] as int, (idBlue): rgb[2] as int]), 1)
            sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
            break
        case 4:
            // ToDo:
            //    4th channel has some white temperature we need to know
            //    Having two whites we can readjust color

            //def cmd = encapsulate(zwave.switchColorV3.switchColorSet(colorComponents: [(idWhite): Math.max(rgb[0], 1), (idRed): Math.max(rgb[0], 1), (idGreen): Math.max(rgb[1], 1), (idBlue): Math.max(rgb[2], 1)]), 1)
            //sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))            
            break
        default:
            break
    }    
}

void setSwitchTemp(temp, level) {
    int outputMode = 0
    if(null != paramOutputMode) {
        outputMode = outputModes.indexOf(paramOutputMode)
    }
    
    switch(outputMode + 1) {
        case 2:
            // ToDo:
            //    4th channel has some white temperature we need to know
            //    Having two whites we can readjust color

            //def cmd = encapsulate(zwave.switchColorV3.switchColorSet(colorComponents: [(idRed): Math.max(rgb[0], 1), (idGreen): Math.max(rgb[1], 1)]), 1)
            //sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))            
            break
        case 3:
            def rgb = temperatureColorRGB(temp, level)
        
            if(null != paramWhiteBalance) {
                // LEDs are expected to have non pure white color
                // For example they might give 6500K white at [255.0, 255.0, 255.0]
                def whiteBalance = paramWhiteBalance as int
                rgb = changeWhiteBalance(rgb, temperatureColorRGB(whiteBalance as int, 100), CalibrationWhite())
            }
        
            logInfo("setSwitchColor TL(${temp}, ${level}) => RGB(${rgb[0]}, ${rgb[1]}, ${rgb[2]})")
        
            def cmd = encapsulate(zwave.switchColorV3.switchColorSet(colorComponents: [(idRed): rgb[0] as int, (idGreen): rgb[1] as int, (idBlue): rgb[2] as int]), 1)
            sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
            break
        case 4:
            // ToDo:
            //    4th channel has some white temperature we need to know
            //    Having two whites we can readjust color

            //def cmd = encapsulate(zwave.switchColorV3.switchColorSet(colorComponents: [(idWhite): Math.max(rgb[0], 1), (idRed): Math.max(rgb[0], 1), (idGreen): Math.max(rgb[1], 1), (idBlue): Math.max(rgb[2], 1)]), 1)
            //sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))            
            break
        default:
            break
    }    
}

// Switch multilevel V4
void zwaveEvent(hubitat.zwave.commands.switchmultilevelv4.SwitchMultilevelReport cmd, Short ep = 0) {
    logDebug("(${ep}) Switch multilevel V4 report: ${cmd}")
    
    if(ep < 2) {
        def eventList = []
        
        def outputDevice = getOutputDevice()
        Short endPoint = outputDevice.getDataValue("EP") as Short
        
        // Color device is on/off only (while dimming is technically available)
        if(cmd.targetValue == 0) {
            eventList << [name: "switch", value: "off"]
        } else if(cmd.targetValue < 100) {            
            eventList << [name: "switch", value: "on"]
        } else {
            logWarn "(${ep}) Switch multilevel report unknown state ${cmd.targetValue}"
        }
        
        if(1 == endPoint) {
            outputDevice.parse(eventList)
        }
        
        if(cmd.targetValue > 0 && cmd.targetValue < 100) {
            eventList << [name: "level", value: cmd.targetValue, descriptionText: "${outputDevice.displayName} level was set to ${cmd.targetValue}%", unit: "%"]
        }
        parse(eventList)        
    }
    else if (ep < 6) {
        def eventList = []
        
        def outputDevice = getOutputDevice(ep - 2 as Short)
        
        if(null != outputDevice) {
            if(cmd.targetValue == 0) {
                eventList << [name: "switch", value: "off", descriptionText: "${outputDevice.displayName} was turned off"]
            } else if(cmd.targetValue < 100) {
                eventList << [name: "level", value: cmd.targetValue, descriptionText: "${outputDevice.displayName} level was set to ${cmd.targetValue}%", unit: "%"]
                eventList << [name: "switch", value: "on", descriptionText :"${outputDevice.displayName} was turned on"]
            } else {
                logWarn "(${ep}) Switch multilevel report unknown state ${cmd.targetValue}"
            }
        }
        
        outputDevice.parse(eventList)
    }
    else {
        logWarn "(${ep}) Switch multilevel report unknown endpoint ${cmd.targetValue}"
    }
}


void setSwitchMultilevel(state, Short ep = 1) {
    Short value = Math.min(state as Short, 99)
    String cmd = encapsulate(zwave.switchMultilevelV4.switchMultilevelSet(value: value), ep)
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

String getSwitchMultilevelReportString(Short ep = 1) {
    return encapsulate(zwave.switchMultilevelV4.switchMultilevelGet(), ep)
}

// Version V2
//    see 'version1'
void updateVersionInfo() {
    sendHubCommand(new hubitat.device.HubAction(getVersionReportCommand(), hubitat.device.Protocol.ZWAVE))
}

void zwaveGroupAddNodeToGroup(String enumValue, def nodeId, def endpointId) {
    int groupNumber = (nodeGroups.indexOf(enumValue) as int) + 2 // offset by one (groups start from '1') and skip 'Lifeline' group
    if(null == endpointId) {
        addDeviceNodeToGroup(groupNumber, nodeId)
    }
    else {
        addDeviceEndpointToGroup(groupNumber, nodeId, endpointId)
    }
}

void zwaveGroupRemoveNodeFromGroup(String enumValue, def nodeId, def endpointId) {
    int effectnumber = (nodeGroups.indexOf(enumValue) as int) + 2 // offset by one (groups start from '1') and skip 'Lifeline' group
    if(null == endpointId) {
        removeDeviceNodeFromGroup(groupNumber, nodeId)
    }
    else {
        removeDeviceEndpointFromGroup(groupNumber, nodeId, endpointId)
    }
}  

void updatePreferencesFromDevice() {
    // Send all parameters one by one
    def configurationCommands = []
            
    for ( entry in parameterDescription ) {
        configurationCommands << getParameterReport(entry.key) 
    }
    
    configurationCommands.addAll(getDeviceMultiChannelGroupNodesCommandList())
    
    state.clear()
    state.hint = "<b style='color:red;'>Not all parameters are reported. Please, refresh this page in a few seconds</b>"
    runIn(20, removeStateHint)    
    
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(configurationCommands, 500), hubitat.device.Protocol.ZWAVE))
}

//=============================================================================================================================================================
// Own interface
//=============================================================================================================================================================

void configure() {
    updateVersionInfo()
    updatePreferencesFromDevice()
    configureChildDevices()
    parse([
        [name: "lightEffects", value: param157options, descriptionText: "Initial value"]
    ])
    
    // request state
}

def installed() {
    configure()
}

void on() {
    int value = Math.max(Math.min(device.latestValue("level", true) as int, 99), 1)
    setBasic(value)
}

void off() {
    setBasic(0)
}

void refresh() {
    def commandList = []
    commandList.addAll(getSwitchColorReportStringList())
    commandList << getSwitchMultilevelReportString()    
    
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(commandList, 500), hubitat.device.Protocol.ZWAVE))
}

void setEffect(String enumValue) {
    def options = param157options
    Number effectnumber = options.indexOf(enumValue)
    
    setEffect(effectnumber)
}

// capability "LightEffects"
void setEffect(Number effectnumber) {
    parse([
        [name: "effectName", value: param157options[effectnumber as int]]
    ])
    
    // ToDo: add event to child
    // [name: "colorMode", value: "EFFECTS"]
    
    cmd = setParameter( 157,  1,  effectnumber)
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

// capability "SwitchLevel"
def setLevel(level, duration = 0) {
    setSwitchMultilevel(level)
}

void updated() {
    // Send all parameters one by one
    def configurationCommands = []
    
    for ( entry in parameterDescription ) {
        configurationCommands << setParameter(entry.key)
    }
    
    configurationCommands.removeAll("")
            
    for ( entry in parameterDescription ) {
        configurationCommands << getParameterReport(entry.key) 
    }
    
    configurationCommands.addAll(getDeviceMultiChannelGroupNodesCommandList())
    
    state.clear()
    state.hint = "<b style='color:red;'>Not all parameters are reported. Please, refresh this page in a few seconds</b>"
    runIn(20, removeStateHint)
    
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(configurationCommands, 500), hubitat.device.Protocol.ZWAVE))
    
    configureChildDevices()
}

//=============================================================================================================================================================
// Component devices interface
//=============================================================================================================================================================

void componentParse(cd, List<Map> events) {
    // stub
}

void componentRefresh(cd) {
    logInfo("Received 'refresh' request from ${cd.displayName}")
}

void componentOn(cd) {
    logInfo("Received 'on' request from ${cd.displayName}")
    
    Short endPoint = cd.getDataValue("EP") as Short    
    if(endPoint > 1) { // 2, 3, 4 or 5
        Short value = cd.latestValue("level") as Short
        setSwitchMultilevel(value, endPoint)
    }
    else {
        Short value = device.latestValue("level") as Short
        setSwitchMultilevel(value, endPoint)
    }    
}

void componentOff(cd){
    logInfo("Received off request from ${cd.displayName}")
    
    Short endPoint = cd.getDataValue("EP") as Short
    setSwitchMultilevel(0, endPoint)
}

void componentSetColor(cd, colormap) { // [hue*:(0 to 100), saturation*:(0 to 100), level:(0 to 100)]
    logInfo("Received setColor(${level}, ${transitionTime}) request from ${cd.displayName}")
    
    setSwitchColor(colormap.hue, colormap.saturation, colormap.level)
    
    def switchState = (cd.latestValue("switch") == "on") as boolean
    
    def eventList = [
        [name: "colorMode", value: "RGB"]
    ]
    
    getDriver(cd)?.parse(eventList)    
}

void componentSetColorTemperature(cd, temp, level, duration) {
    logInfo("Received setColorTemperature(${temp}, ${level}, ${duration}) request from ${cd.displayName}")
    
    if(null == level) {
        level = cd.latestValue("level") as int
    }
    
    setSwitchTemp(temp, level)
    
    def eventList = [
        [name: "colorMode", value: "CT"]
    ]
    
    eventList << [name: "colorTemperature", value: temp, unit: "°K", descriptionText:""]
    getDriver(cd)?.parse(eventList)
}

void componentSetHue(cd, hue) {    
    logInfo("Received setHue(${saturation}) request from ${cd.displayName}")
    
    List<Map> events = [
        [name: "colorMode", value: "RGB"]
    ]
    
    def switchState = (cd.latestValue("switch") == "on") as boolean
    
    if(switchState) {
        Short endPoint = cd.getDataValue("EP") as Short
        if(endPoint < 2) {
                        
            def saturation = cd.latestValue("saturation") as int
            def level = cd.latestValue("level") as int
            setSwitchColor(hue, saturation, level)
        }
    }
    else {  
        events += [name: "hue", value: hue as int, descriptionText:"Set offline 'hue' to ${hue}"]
    }
    
    getDriver(cd)?.parse(events)
}

void componentSetLevel(cd, level, transitionTime = null) {
    logInfo("Received setLevel(${level}, ${transitionTime}) request from ${cd.displayName}")
   
    Short endPoint = cd.getDataValue("EP") as Short
    if(endPoint < 2) {
        def hue = cd.latestValue("hue") as int
        def saturation = cd.latestValue("saturation") as int
        setSwitchColor(hue, saturation, level)
    }
    else {    
        setSwitchMultilevel(level, endPoint)
    }
}

void componentSetSaturation(cd, saturation) {
    logInfo("Received setSaturation(${saturation}) request from ${cd.displayName}")
    
    List<Map> events = [
        [name: "colorMode", value: "RGB"]
    ]
    
    def switchState = (cd.latestValue("switch", true) == "on") as boolean
    
    if(switchState) {
        Short endPoint = cd.getDataValue("EP") as Short
        if(endPoint < 2) {
            def hue = cd.latestValue("hue") as int
            def level = cd.latestValue("level") as int
            setSwitchColor(hue, saturation, level)
        }
    }
    else {  
        events += [name: "saturation", value: saturation as int, descriptionText:"Set offline 'saturation' to ${saturation}%"]
    }
    
    getDriver(cd)?.parse(events)
}

void componentStartLevelChange(cd, direction) {
    logInfo("Received startLevelChange(${direction}) request from ${cd.displayName}")
    
    Short endPoint = cd.getDataValue("EP") as Short
    def down = (direction == "down") as boolean
    
    def cmd = encapsulate(zwave.switchMultilevelV4.switchMultilevelStartLevelChange(dimmingDuration: 10, ignoreStartLevel: true, startLevel: 0, upDown: down), endPoint)
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

void componentStopLevelChange(cd) {
    logInfo("Received stopLevelChange request from ${cd.displayName}")
    
    Short endPoint = cd.getDataValue("EP") as Short
    
    def cmd = encapsulate(zwave.switchMultilevelV4.switchMultilevelStopLevelChange(), endPoint)
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}
