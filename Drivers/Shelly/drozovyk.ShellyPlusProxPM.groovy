
#include drozovyk.common1
#include drozovyk.meter1
#include drozovyk.sensor1
#include drozovyk.version1

import groovy.transform.Field
import java.time.Instant

// Currently driver is expected to support
//  Shelly Plus 1
//  Shelly Plus 1 PM (tested)
//  Shelly Plus 2 PM (tested)
//  Shelly Plus H&T (while compatible, hardly applicable as device is usually in a sleep mode - cannot keep event stream connected)
//  Shelly Plus I4 (tested)
//  Shelly Plus Plug (IT, S, UK, US) (no color ring support)
//  Shelly Plus RGBW (not tested, light profile only atm.)
//  Shelly Plus Uni (tested)
//  Shelly Plus Wall Dimmer  (expected to work; not tested)
//  Shelly Plus Wall Display (no API documentation; thermostat on/off is not working)
//  Shelly Pro 1
//  Shelly Pro 1 Dimmer (not tested)
//  Shelly Pro 1 PM (tested)
//  Shelly Pro 2
//  Shelly Pro 2 Dimmer (not tested)
//  Shelly Pro 2 PM (tested)
//  Shelly Pro 3 (not tested)
//  Shelly Pro 3 EM (tested)
//  Shelly Pro 4 PM
//  Shelly Pro EM (not tested)
//  Shelly Add-on module (tested)

// Can be extended to support
//  Shelly Plus Smoke       (needs to add gas/smoke child device)

// Shelly protocol responses don't have full information about what query has triggered them
// Instead they have packet 'id' that is the same as in the issued query
// To make driver code easier all commands are placed in the table where id will represent encoded
// query type. The specific id value has no importance. Rather its uniqueness does matter
// This way it is possible quickly identify callback subroutine
// Keep in mind that some commands break WS event stream causing device to reconnect.
private static int CmdCloudGetStatus()       { return   0 }
private static int CmdCoverClose()           { return  10 }
private static int CmdCoverGetStatus()       { return  11 }
private static int CmdCoverGoToPosition()    { return  12 }
private static int CmdCoverOpen()            { return  13 }
private static int CmdCoverStop()            { return  14 }
private static int CmdDevicePowerGetStatus() { return  20 }     // battery object { V number or null, percent number or null }, external object or null { present bool }, errors array of strings
private static int CmdEMGetStatus()          { return  30 }
private static int CmdEMDataGetStatus()      { return  40 }
private static int CmdEM1GetStatus()         { return  50 }
private static int CmdEM1DataGetStatus()     { return  60 }
private static int CmdEthGetStatus()         { return  70 }
private static int CmdHumidityGetStatus()    { return  80 }     // rh number or null, errors
private static int CmdIlluminanceGetStatus() { return  90 }
private static int CmdInputGetStatus()       { return 100 }
private static int CmdInputResetCounters()   { return 101 }
private static int CmdLightGetStatus()       { return 110 }     // output boolean, brightness number, timer_started_at number, timer_duration number
private static int CmdLightSet()             { return 111 }
private static int CmdLightToggle()          { return 112 }
private static int CmdPM1GetStatus()         { return 120 }
private static int CmdRGBGetStatus()         { return 130 }
private static int CmdRGBSet()               { return 131 }
private static int CmdRGBToggle()            { return 132 }
private static int CmdRGBDimUp()             { return 133 }
private static int CmdRGBDimDown()           { return 134 }
private static int CmdRGBDimStop()           { return 135 }
private static int CmdRGBWGetStatus()        { return 140 }
private static int CmdRGBWSet()              { return 141 }
private static int CmdRGBWToggle()           { return 142 }
private static int CmdRGBWDimUp()            { return 143 }
private static int CmdRGBWDimDown()          { return 144 }
private static int CmdRGBWDimStop()          { return 145 }
private static int CmdScriptGetStatus()      { return 150 }
private static int CmdScriptStart()          { return 151 }
private static int CmdScriptStop()           { return 152 }
private static int CmdShellyCheckForUpdate() { return 160 }
private static int CmdShellyGetDeviceInfo()  { return 161 }
private static int CmdShellyGetStatus()      { return 162 }
private static int CmdShellyGetConfig()      { return 163 }
private static int CmdShellyListProfiles()   { return 164 }
private static int CmdShellyReboot()         { return 165 }
private static int CmdShellyUpdate()         { return 166 }
private static int CmdSmokeGetStatus()       { return 170 }     // alarm boolean, mute boolean
private static int CmdSwitchGetStatus()      { return 180 }
private static int CmdSwitchSet()            { return 181 }
private static int CmdTemperatureGetStatus() { return 190 }    // tC number or null, tF number or null, errors
private static int CmdThermostatGetStatus()  { return 200 }
private static int CmdThermostatSetConfig()  { return 201 }
private static int CmdVoltmeterGetStatus()   { return 210 }
private static int CmdWifiGetStatus()        { return 220 }

@Field static final Map shellyCommands = [
  (CmdCloudGetStatus()):       [method: "Cloud.GetStatus"],
  (CmdCoverClose()):           [method: "Cover.Close"],
  (CmdCoverGetStatus()):       [method: "Cover.GetStatus"],
  (CmdCoverGoToPosition()):    [method: "Cover.GoToPosition"],
  (CmdCoverOpen()):            [method: "Cover.Open"],
  (CmdCoverStop()):            [method: "Cover.Stop"],
  (CmdDevicePowerGetStatus()): [method: "DevicePower.GetStatus"],
  (CmdEMGetStatus()):          [method: "EM.GetStatus"],
  (CmdEMDataGetStatus()):      [method: "EMData.GetStatus"],
  (CmdEM1GetStatus()):         [method: "EM1.GetStatus"],
  (CmdEM1DataGetStatus()):     [method: "EMData1.GetStatus"],
  (CmdEthGetStatus()):         [method: "Eth.GetStatus"],
  (CmdHumidityGetStatus()):    [method: "Humidity.GetStatus"],
  (CmdIlluminanceGetStatus()): [method: "Illuminance.GetStatus"],
  (CmdInputGetStatus()):       [method: "Input.GetStatus"],
  (CmdInputResetCounters()):   [method: "Input.ResetCounters"],
  (CmdLightGetStatus()):       [method: "Light.GetStatus"],
  (CmdLightSet()):             [method: "Light.Set"],
  (CmdLightToggle()):          [method: "Light.Toggle"],
  (CmdPM1GetStatus()):         [method: "PM1.GetStatus"],
  (CmdRGBGetStatus()):         [method: "RGB.GetStatus"],
  (CmdRGBSet()):               [method: "RGB.Set"],
  (CmdRGBToggle()):            [method: "RGB.Toggle"],
  (CmdRGBDimUp()):             [method: "RGB.DimUp"], 
  (CmdRGBDimDown()):           [method: "RGB.DimDown"], 
  (CmdRGBDimStop()):           [method: "RGB.DimStop"],
  (CmdRGBWGetStatus()):        [method: "RGBW.GetStatus"],
  (CmdRGBWSet()):              [method: "RGBW.Set"],
  (CmdRGBWToggle()):           [method: "RGBW.Toggle"],
  (CmdRGBWDimUp()):            [method: "RGBW.DimUp"], 
  (CmdRGBWDimDown()):          [method: "RGBW.DimDown"], 
  (CmdRGBWDimStop()):          [method: "RGBW.DimStop"], 
  (CmdScriptGetStatus()):      [method: "Script.GetStatus"],
  (CmdScriptStart()):          [method: "Script.Start"],
  (CmdScriptStop()):           [method: "Script.Stop"],
  (CmdShellyCheckForUpdate()): [method: "Shelly.CheckForUpdate"],
  (CmdShellyGetDeviceInfo()):  [method: "Shelly.GetDeviceInfo"],
  (CmdShellyGetStatus()):      [method: "Shelly.GetStatus"],
  (CmdShellyGetConfig()):      [method: "Shelly.GetConfig"],  
  (CmdShellyListProfiles()):   [method: "Shelly.ListProfiles"],   
  (CmdShellyReboot()):         [method: "Shelly.Reboot"],
  (CmdShellyUpdate()):         [method: "Shelly.Update"],
  (CmdSmokeGetStatus()):       [method: "Smoke.GetStatus"],
  (CmdSwitchGetStatus()):      [method: "Switch.GetStatus"],
  (CmdSwitchSet()):            [method: "Switch.Set"],
  (CmdTemperatureGetStatus()): [method: "Temperature.GetStatus"],
  (CmdThermostatGetStatus()):  [method: "Thermostat.GetStatus"],
  (CmdThermostatSetConfig()):  [method: "Thermostat.SetConfig"],
  (CmdVoltmeterGetStatus()):   [method: "Voltmeter.GetStatus"],
  (CmdWifiGetStatus()):        [method: "Wifi.GetStatus"],
]

@Field static final def devicePath = "/rpc"

metadata {
    definition (name: "Shelly Plus/Pro xPM", namespace: "drozovyk", author: "Dmytro Rozovyk") {
        capability("Actuator")              // To run custom actions in RM rules
        capability("Initialize")            // add 'initialize' command
        capability("Refresh")

        // Can be used by RM to send user notifications
        attribute("cloud",       "STRING")  // n/a, online, offline
        attribute("ethernet",    "STRING")  // n/a, ip, offline
        attribute("fwUpdate",    "STRING")  // 'none' or version
        attribute("healthStatus","ENUM", ["offline", "online"]) // basically a "link" attribute with shorter options list
        attribute("link",        "STRING")  // online, offline, failure (auto reconnect in 30s on failure)
        attribute("modbus",      "STRING")  // n/a, online, offline
        attribute("mqtt",        "STRING")  // n/a, online, offline
        attribute("rssi",        "STRING")  // n/a or percentage; not using "SignalStrength" capability as this attribute is not updated authomatically (needs polling)
        attribute("wifi",        "STRING")  // n/a, ip, offline

        // temporary attribute to hold error state for now
        attribute("error", "string")

        command("checkForUpdates")
        command("configure")
        command("connect")                  // open link
        command("disconnect")               // close link
        command("reboot")
        command("updateFirmware", [[name: "stage", description: "", type: "ENUM", constraints: ["stable", "beta"]]])
        command("updateVersionInfo")
    }

    preferences {
        input(name: "deviceURL",     type: "string",   title:"Address",                     description: "Device host http://, ws://", defaultValue: "192.168.33.1",  required: true,  displayDuringSetup: true)
        input(name: "devicePassword",type: "password", title:"Password",                    description: "Device password",            defaultValue: "",              required: false, displayDuringSetup: true)
        input(name: "retryIn",       type: "number",   title:"Reconnect in (S)",            description: "if link failed",             defaultValue: 30,              required: false, displayDuringSetup: true)
        input(name: "alarmIn",       type: "number",   title:"Update health status in (S)", description: "if link failed",             defaultValue: 90,              required: false, displayDuringSetup: true)
        inputLogLevel()
        
        input(name: "numVirtualTemperatureSensors", type: "number",   title: "Virtual scripted temperature sensors", defaultValue: 0, required: false, displayDuringSetup: false)
        input(name: "numVirtualHumiditySensors",    type: "number",   title: "Virtual scripted humidity sensors",    defaultValue: 0, required: false, displayDuringSetup: false)
    }
}

//=============================================================================================================================================================
// Common
//=============================================================================================================================================================
String getBatteryDeviceLabel(Short batteryNumber)        { return "${device.label} battery ${batteryNumber}" }
String getBatteryDeviceName(Short batteryNumber)         { return "${device.name}-battery-${batteryNumber}" }
String getBatteryDeviceNetworkId(Short batteryNumber)    { return "${device.deviceNetworkId}-battery-${batteryNumber}" }
def    getBatteryDevice(Short batteryNumber)             { return getChildDevice(getBatteryDeviceNetworkId(batteryNumber)) }

String getCoverDeviceLabel(Short coverNumber)            { return "${device.label} cover ${coverNumber}" }
String getCoverDeviceName(Short coverNumber)             { return "${device.name}-cover-${coverNumber}" }
String getCoverDeviceNetworkId(Short coverNumber)        { return "${device.deviceNetworkId}-cover-${coverNumber}" }
def    getCoverDevice(Short coverNumber)                 { return getChildDevice(getCoverDeviceNetworkId(coverNumber)) }

String getDimmerDeviceLabel(Short dimmerNumber)          { return "${device.label} dimmer ${dimmerNumber}" }
String getDimmerDeviceName(Short dimmerNumber)           { return "${device.name}-dimmer-${dimmerNumber}" }
String getDimmerDeviceNetworkId(Short dimmerNumber)      { return "${device.deviceNetworkId}-dimmer-${dimmerNumber}" }
def    getDimmerDevice(Short dimmerNumber)               { return getChildDevice(getDimmerDeviceNetworkId(dimmerNumber)) }

String getIlluminanceSensorDeviceLabel(Short lumNumber)      { return "${device.label} illuminance sensor ${lumNumber}" }
String getIlluminanceSensorDeviceName(Short lumNumber)       { return "${device.name}-lsensor-${lumNumber}" }
String getIlluminanceSensorDeviceNetworkId(Short lumNumber)  { return "${device.deviceNetworkId}-lsensor-${lumNumber}" }
def    getIlluminanceSensorDevice(Short lumNumber)           { return getChildDevice(getIlluminanceSensorDeviceNetworkId(lumNumber)) }

String getHumiditySensorDeviceLabel(Short humNumber)     { return "${device.label} humidity sensor ${humNumber}" }
String getHumiditySensorDeviceName(Short humNumber)      { return "${device.name}-hsensor-${humNumber}" }
String getHumiditySensorDeviceNetworkId(Short humNumber) { return "${device.deviceNetworkId}-hsensor-${humNumber}" }
def    getHumiditySensorDevice(Short humNumber)          { return getChildDevice(getHumiditySensorDeviceNetworkId(humNumber)) }

String getInputDeviceLabel(Short inputNumber)            { return "${device.label} input ${inputNumber}" }
String getInputDeviceName(Short inputNumber)             { return "${device.name}-input-${inputNumber}" }
String getInputDeviceNetworkId(Short inputNumber)        { return "${device.deviceNetworkId}-input-${inputNumber}" }
def    getInputDevice(Short inputNumber)                 { return getChildDevice(getInputDeviceNetworkId(inputNumber)) }

@Field static final int channelPhaseA = 0
@Field static final int channelPhaseB = 1
@Field static final int channelPhaseC = 2
@Field static final int channelTotal  = 3
@Field static Map meterChannels = [
    (0): [name: "a", label: "Phase A"],
    (1): [name: "b", label: "Phase B"],
    (2): [name: "c", label: "Phase C"],
    (3): [name: "total", label: "Total"]
]
String getMeterDeviceLabel(Short meterNumber, int phase)     { return "${device.label} meter ${meterNumber} ${meterChannels[phase].label}" }
String getMeterDeviceName(Short meterNumber, int phase)      { return "${device.name}-meter-${meterNumber}-${meterChannels[phase].name}" }
String getMeterDeviceNetworkId(Short meterNumber, int phase) { return "${device.deviceNetworkId}-meter-${meterNumber}-${meterChannels[phase].name}" }
def    getMeterDevices(Short meterNumber)                    { return [
    getChildDevice(getMeterDeviceNetworkId(meterNumber, channelPhaseA)),
    getChildDevice(getMeterDeviceNetworkId(meterNumber, channelPhaseB)),
    getChildDevice(getMeterDeviceNetworkId(meterNumber, channelPhaseC)),
    getChildDevice(getMeterDeviceNetworkId(meterNumber, channelTotal))
]}

String getMeterDeviceLabel(Short meterNumber)     { return "${device.label} meter ${meterNumber}" }
String getMeterDeviceName(Short meterNumber)      { return "${device.name}-meter-${meterNumber}" }
String getMeterDeviceNetworkId(Short meterNumber) { return "${device.deviceNetworkId}-meter-${meterNumber}" }
def    getMeterDevice(Short meterNumber)          { return getChildDevice(getMeterDeviceNetworkId(meterNumber)) }

String getPowerMeterDeviceLabel(Short pmNumber)      { return "${device.label} power meter ${pmNumber}" }
String getPowerMeterDeviceName(Short pmNumber)       { return "${device.name}-powermeter-${pmNumber}" }
String getPowerMeterDeviceNetworkId(Short pmNumber)  { return "${device.deviceNetworkId}-powermeter-${pmNumber}" }
def    getPowerMeterDevice(Short pmNumber)           { return getChildDevice(getPowerMeterDeviceNetworkId(pmNumber)) }

String getRGBDeviceLabel(Short switchNumber)      { return "${device.label} rgb ${switchNumber}" }
String getRGBDeviceName(Short switchNumber)       { return "${device.name}-rgb-${switchNumber}" }
String getRGBDeviceNetworkId(Short switchNumber)  { return "${device.deviceNetworkId}-rgb-${switchNumber}" }
def    getRGBDevice(Short switchNumber)           { return getChildDevice(getRGBDeviceNetworkId(switchNumber)) }

String getRGBWDeviceLabel(Short switchNumber)      { return "${device.label} rgbw ${switchNumber}" }
String getRGBWDeviceName(Short switchNumber)       { return "${device.name}-rgbw-${switchNumber}" }
String getRGBWDeviceNetworkId(Short switchNumber)  { return "${device.deviceNetworkId}-rgbw-${switchNumber}" }
def    getRGBWDevice(Short switchNumber)           { return getChildDevice(getRGBWDeviceNetworkId(switchNumber)) }

String getScriptDeviceLabel(Short scriptNumber)      { return "${device.label} script ${scriptNumber}" }
String getScriptDeviceName(Short scriptNumber)       { return "${device.name}-script-${scriptNumber}" }
String getScriptDeviceNetworkId(Short scriptNumber)  { return "${device.deviceNetworkId}-script-${scriptNumber}" }
def    getScriptDevice(Short scriptNumber)           { return getChildDevice(getScriptDeviceNetworkId(scriptNumber)) }

String getSysBtnDeviceLabel(Short btnNumber)            { return "${device.label} system button ${btnNumber}" }
String getSysBtnDeviceName(Short btnNumber)             { return "${device.name}-sysbtn-${btnNumber}" }
String getSysBtnDeviceNetworkId(Short btnNumber)        { return "${device.deviceNetworkId}-sysbtn-${btnNumber}" }
def    getSysBtnDevice(Short btnNumber)                 { return getChildDevice(getSysBtnDeviceNetworkId(btnNumber)) }

String getSwitchDeviceLabel(Short switchNumber)      { return "${device.label} switch ${switchNumber}" }
String getSwitchDeviceName(Short switchNumber)       { return "${device.name}-switch-${switchNumber}" }
String getSwitchDeviceNetworkId(Short switchNumber)  { return "${device.deviceNetworkId}-switch-${switchNumber}" }
def    getSwitchDevice(Short switchNumber)           { return getChildDevice(getSwitchDeviceNetworkId(switchNumber)) }

String getTemperatureSensorDeviceLabel(tempNumber)      { return "${device.label} temperature sensor ${tempNumber}" }
String getTemperatureSensorDeviceName(tempNumber)       { return "${device.name}-tsensor-${tempNumber}" }
String getTemperatureSensorDeviceNetworkId(tempNumber)  { return "${device.deviceNetworkId}-tsensor-${tempNumber}" }
def    getTemperatureSensorDevice(tempNumber)           { return getChildDevice(getTemperatureSensorDeviceNetworkId(tempNumber)) }

String getThermostatDeviceLabel(Short tempNumber)      { return "${device.label} thermostat ${tempNumber}" }
String getThermostatDeviceName(Short tempNumber)       { return "${device.name}-thermostat-${tempNumber}" }
String getThermostatDeviceNetworkId(Short tempNumber)  { return "${device.deviceNetworkId}-thermostat-${tempNumber}" }
def    getThermostatDevice(Short tempNumber)           { return getChildDevice(getThermostatDeviceNetworkId(tempNumber)) }

String getVoltageSensorDeviceLabel(voltNumber)      { return "${device.label} voltage sensor ${voltNumber}" }
String getVoltageSensorDeviceName(voltNumber)       { return "${device.name}-vsensor-${voltNumber}" }
String getVoltageSensorDeviceNetworkId(voltNumber)  { return "${device.deviceNetworkId}-vsensor-${voltNumber}" }
def    getVoltageSensorDevice(voltNumber)           { return getChildDevice(getVoltageSensorDeviceNetworkId(voltNumber)) }

String getVirtualHumiditySensorDeviceLabel(Short humNumber)      { return "${device.label} virtual humidity sensor ${humNumber}" }
String getVirtualHumiditySensorDeviceName(Short humNumber)       { return "${device.name}-vhsensor-${humNumber}" }
String getVirtualHumiditySensorDeviceNetworkId(Short humNumber)  { return "${device.deviceNetworkId}-vhsensor-${humNumber}" }
def    getVirtualHumiditySensorDevice(Short humNumber)           { return getChildDevice(getVirtualHumiditySensorDeviceNetworkId(humNumber)) }

String getVirtualTemperatureSensorDeviceLabel(Short tempNumber)      { return "${device.label} virtual temperature sensor ${tempNumber}" }
String getVirtualTemperatureSensorDeviceName(Short tempNumber)       { return "${device.name}-vtsensor-${tempNumber}" }
String getVirtualTemperatureSensorDeviceNetworkId(Short tempNumber)  { return "${device.deviceNetworkId}-vtsensor-${tempNumber}" }
def    getVirtualTemperatureSensorDevice(Short tempNumber)           { return getChildDevice(getVirtualTemperatureSensorDeviceNetworkId(tempNumber)) }

private void configureChildDevices() {
    components = getDataValue("components")
    if(null != components) {
        def componentMap = [:]
        // [ble:[1], cloud:[1], eth:[1], input:[1, 2], mqtt:[1], script:[1, 2, 3, 4, 5, 6], switch:[1, 2], sys:[1], wifi:[1], ws:[1]]
        components = components.substring(1, components.length() - 1).split("\\][,]?[ ]?").each({
            def entry = it.trim().split(":\\[")
            componentMap.put(entry[0], [])            
            entry[1].split(",").each({
                  componentMap[entry[0]] << (it.trim() as int)
            })                       
        })
        
        logDebug(componentMap.toString())

        numTSensors     = componentMap.temperature
        numThermostats  = componentMap.thermostat
        
        numVHSensors = numVirtualHumiditySensors
        numVTSensors = numVirtualTemperatureSensors
        
        validDeviceNetworksIDs = []

        componentMap.powerdevice?.each({
            Short batteryIndex = it
            batteryDevice = getBatteryDevice(batteryIndex)
            if(null == batteryDevice) {
                batteryDevice = addChildDevice("drozovyk", "Virtual Battery Powered Device", getBatteryDeviceNetworkId(batteryIndex),  [isComponent: true, name: getBatteryDeviceName(batteryIndex), label: getBatteryDeviceLabel(batteryIndex)])
                batteryDevice.updateDataValue("component", "devicepower")
                batteryDevice.updateDataValue("EP", batteryIndex as String)
                logInfo("Spawned child device for the battery ${batteryIndex}")
            }

            validDeviceNetworksIDs << getBatteryDeviceNetworkId(batteryIndex)
        })
        
        componentMap.cover?.each({
            Short coverIndex = it
            coverDevice = getCoverDevice(coverIndex)
            if(null == coverDevice) {
                coverDevice = addChildDevice("drozovyk", "Generic Component Metered Window Shade", getCoverDeviceNetworkId(coverIndex),  [isComponent: true, name: getCoverDeviceName(coverIndex), label: getCoverDeviceLabel(coverIndex)])
                coverDevice.updateDataValue("component", "cover")
                coverDevice.updateDataValue("EP", coverIndex as String)
                logInfo("Spawned child device for the cover ${coverIndex}")
            }

            validDeviceNetworksIDs << getCoverDeviceNetworkId(coverIndex)
        })
        
        componentMap.em?.each({ meterIndex ->
            meterIndex = meterIndex as Short
            meterDevices = getMeterDevices(meterIndex)
            meterDevices.eachWithIndex({ it, channelIdx ->
                if(null == it) {
                    it = addChildDevice("drozovyk", "Generic Component Energy Meter", getMeterDeviceNetworkId(meterIndex, channelIdx),  [isComponent: true, name: getMeterDeviceName(meterIndex, channelIdx), label: getMeterDeviceLabel(meterIndex, channelIdx)])                                // ToDo:
                    it.updateDataValue("component", "em")
                    it.updateDataValue("EP", meterIndex as String)
                }

                validDeviceNetworksIDs << getMeterDeviceNetworkId(meterIndex, channelIdx)
            })
            logInfo("Spawned child devices for the energy meter ${meterIndex}")
        })        

        componentMap.em1?.each({
            Short meterIndex = it
            meterDevice = getMeterDevice(meterIndex)
            if(null == meterDevice) {
                meterDevice = addChildDevice("drozovyk", "Generic Component Energy Meter", getMeterDeviceNetworkId(meterIndex),  [isComponent: true, name: getMeterDeviceName(meterIndex), label: getMeterDeviceLabel(meterIndex)])                                // ToDo:
                meterDevice.updateDataValue("component", "em1")
                meterDevice.updateDataValue("EP", meterIndex as String)
                logInfo("Spawned child device for the energy meter ${meterIndex}")
            }

            validDeviceNetworksIDs << getMeterDeviceNetworkId(meterIndex)                
        })        

        componentMap.humidity?.each({
            Short hSensorIndex = it
            hSensorDevice = getHumiditySensorDevice(hSensorIndex)
            if(null == hSensorDevice) {
                hSensorDevice = addChildDevice("hubitat", "Generic Component Humidity Sensor", getHumiditySensorDeviceNetworkId(hSensorIndex),  [isComponent: true, name: getHumiditySensorDeviceName(hSensorIndex), label: getHumiditySensorDeviceLabel(hSensorIndex)])
                hSensorDevice.updateDataValue("component", "humidity")
                hSensorDevice.updateDataValue("EP", hSensorIndex as String)
                logInfo("Spawned child device for the relative humidity sensor ${hSensorIndex}")
            }

            validDeviceNetworksIDs << getHumiditySensorDeviceNetworkId(hSensorIndex)
        })
        
        componentMap.illuminance?.each({
            Short lSensorIndex = it
            lSensorDevice = getIlluminanceSensorDevice(lSensorIndex)
            if(null == lSensorDevice) {
                lSensorDevice = addChildDevice("drozovyk", "Generic Component Illuminance Sensor", getIlluminanceSensorDeviceNetworkId(lSensorIndex),  [isComponent: true, name: getIlluminanceSensorDeviceName(lSensorIndex), label: getIlluminanceSensorDeviceLabel(lSensorIndex)])
                lSensorDevice.updateDataValue("component", "illuminance")
                lSensorDevice.updateDataValue("EP", hSensorIndex as String)
                logInfo("Spawned child device for the illuminance sensor ${lSensorIndex}")
            }

            validDeviceNetworksIDs << getIlluminanceSensorDeviceNetworkId(lSensorIndex)
        })
        
        componentMap.input?.each({
            Short inputIndex = it
            inputDevice = getInputDevice(inputIndex)
            if(null == inputDevice) {
                inputDevice = addChildDevice("drozovyk", "Generic Component Input Event", getInputDeviceNetworkId(inputIndex),  [isComponent: true, name: getInputDeviceName(inputIndex), label: getInputDeviceLabel(inputIndex)])
                inputDevice.updateDataValue("component", "input")
                inputDevice.updateDataValue("EP", inputIndex as String)
                logInfo("Spawned child device for the input ${inputIndex}")
            }

            validDeviceNetworksIDs << getInputDeviceNetworkId(inputIndex)
        })
        
        componentMap.light?.each({
            Short dimmerIndex = it
            dimmerDevice = getDimmerDevice(dimmerIndex)
            if(null == dimmerDevice) {
                dimmerDevice = addChildDevice("drozovyk", "Generic Component Metered Dimmer", getDimmerDeviceNetworkId(dimmerIndex),  [isComponent: true, name: getDimmerDeviceName(dimmerIndex), label: getDimmerDeviceLabel(dimmerIndex)])
                dimmerDevice.updateDataValue("component", "light")
                dimmerDevice.updateDataValue("EP", dimmerIndex as String)
                logInfo("Spawned child device for the dimmer ${dimmerIndex}")
            }

            validDeviceNetworksIDs << getDimmerDeviceNetworkId(dimmerIndex)
            
            tSensorIndex = "light" + dimmerIndex
            tSensorDevice = getTemperatureSensorDevice(tSensorIndex)
            if(null == tSensorDevice) {
                tSensorDevice = addChildDevice("hubitat", "Generic Component Temperature Sensor", getTemperatureSensorDeviceNetworkId(tSensorIndex),  [isComponent: true, name: getTemperatureSensorDeviceName(tSensorIndex), label: getTemperatureSensorDeviceLabel(tSensorIndex)])
                // redirect 'refresh' call to the 'light' component
                // see comment in the switch child devices section
                tSensorDevice.updateDataValue("component", "light")
                tSensorDevice.updateDataValue("EP", dimmerIndex as String)
                logInfo("Spawned child device for the dimmer temperature sensor ${dimmerIndex}")
            }

            validDeviceNetworksIDs << getTemperatureSensorDeviceNetworkId(tSensorIndex)
        })

        componentMap.pm1?.each({
            Short meterIndex = it
            powerMeterDevice = getPowerMeterDevice(meterIndex)
            if(null == powerMeterDevice) {
                powerMeterDevice = addChildDevice("drozovyk", "Generic Component Energy Meter", getPowerMeterDeviceNetworkId(meterIndex),  [isComponent: true, name: getPowerMeterDeviceName(meterIndex), label: getPowerMeterDeviceLabel(meterIndex)])                                // ToDo:
                powerMeterDevice.updateDataValue("component", "pm1")
                powerMeterDevice.updateDataValue("EP", meterIndex as String)
                logInfo("Spawned child device for the power meter ${meterIndex}")
            }

            validDeviceNetworksIDs << getPowerMeterDeviceNetworkId(meterIndex)
        })
        
        componentMap.rgb?.each({
            Short dimmerIndex = it
            dimmerDevice = getRGBDevice(dimmerIndex)
            if(null == dimmerDevice) {
                dimmerDevice = addChildDevice("hubitat", "Generic Component RGB", getRGBDeviceNetworkId(dimmerIndex),  [isComponent: true, name: getRGBDeviceName(dimmerIndex), label: getRGBDeviceLabel(dimmerIndex)])
                dimmerDevice.updateDataValue("component", "rgb")
                dimmerDevice.updateDataValue("EP", dimmerIndex as String)
                logInfo("Spawned child device for the rgb controller ${dimmerIndex}")
            }

            validDeviceNetworksIDs << getRGBDeviceNetworkId(dimmerIndex)
            
            tSensorIndex = "rgb" + dimmerIndex
            tSensorDevice = getTemperatureSensorDevice(tSensorIndex)
            if(null == tSensorDevice) {
                tSensorDevice = addChildDevice("hubitat", "Generic Component Temperature Sensor", getTemperatureSensorDeviceNetworkId(tSensorIndex),  [isComponent: true, name: getTemperatureSensorDeviceName(tSensorIndex), label: getTemperatureSensorDeviceLabel(tSensorIndex)])
                // redirect 'refresh' call to the 'rgb' component
                // see comment in the switch child devices section
                tSensorDevice.updateDataValue("component", "rgb")
                tSensorDevice.updateDataValue("EP", dimmerIndex as String)
                logInfo("Spawned child device for the rgb controller temperature sensor ${dimmerIndex}")
            }

            validDeviceNetworksIDs << getTemperatureSensorDeviceNetworkId(tSensorIndex)
        })
        
        componentMap.rgbw?.each({
            Short dimmerIndex = it
            dimmerDevice = getRGBWDevice(dimmerIndex)
            if(null == dimmerDevice) {
                dimmerDevice = addChildDevice("hubitat", "Generic Component RGBW", getRGBWDeviceNetworkId(dimmerIndex),  [isComponent: true, name: getRGBWDeviceName(dimmerIndex), label: getRGBWDeviceLabel(dimmerIndex)])
                dimmerDevice.updateDataValue("component", "rgbw")
                dimmerDevice.updateDataValue("EP", dimmerIndex as String)
                logInfo("Spawned child device for the rgbw controller ${dimmerIndex}")
            }

            validDeviceNetworksIDs << getRGBWDeviceNetworkId(dimmerIndex)
            
            tSensorIndex = "rgbw" + dimmerIndex
            tSensorDevice = getTemperatureSensorDevice(tSensorIndex)
            if(null == tSensorDevice) {
                tSensorDevice = addChildDevice("hubitat", "Generic Component Temperature Sensor", getTemperatureSensorDeviceNetworkId(tSensorIndex),  [isComponent: true, name: getTemperatureSensorDeviceName(tSensorIndex), label: getTemperatureSensorDeviceLabel(tSensorIndex)])
                // redirect 'refresh' call to the 'rgbw' component
                // see comment in the switch child devices section
                tSensorDevice.updateDataValue("component", "rgbw")
                tSensorDevice.updateDataValue("EP", dimmerIndex as String)
                logInfo("Spawned child device for the rgbw controller temperature sensor ${dimmerIndex}")
            }

            validDeviceNetworksIDs << getTemperatureSensorDeviceNetworkId(tSensorIndex)
        })
        
        componentMap.script?.each({
            Short scriptIndex = it
            scriptDevice = getScriptDevice(scriptIndex)
            if(null == scriptDevice) {
                scriptDevice = addChildDevice("drozovyk", "Generic Component Script", getScriptDeviceNetworkId(scriptIndex),  [isComponent: true, name: getScriptDeviceName(scriptIndex), label: getScriptDeviceLabel(scriptIndex)])
                scriptDevice.updateDataValue("component", "script")
                scriptDevice.updateDataValue("EP", scriptIndex as String)
                logInfo("Spawned child device for the script ${scriptIndex}")
            }

            validDeviceNetworksIDs << getScriptDeviceNetworkId(scriptIndex)
        })
        
        componentMap.sysbtn?.each({
            Short btnIndex = it
            btnDevice = getSysBtnDevice(btnIndex)
            if(null == btnDevice) {
                btnDevice = addChildDevice("hubitat", "Generic Component Button Controller", getSysBtnDeviceNetworkId(btnIndex),  [isComponent: true, name: getSysBtnDeviceName(btnIndex), label: getSysBtnDeviceLabel(btnIndex)])
                btnDevice.updateDataValue("component", "sysbtn")
                btnDevice.updateDataValue("EP", btnIndex as String)
                logInfo("Spawned child device system button ${btnIndex}")
            }

            validDeviceNetworksIDs << getSysBtnDeviceNetworkId(btnIndex)
        })
        
        componentMap.switch?.each({
            Short switchIndex = it
            switchDevice = getSwitchDevice(switchIndex)
            if(null == switchDevice) {
                // using custom component device instead of hubitat "Generic Component Metering Switch" to have amperage and voltage
                switchDevice = addChildDevice("drozovyk", "Generic Component Metered Switch", getSwitchDeviceNetworkId(switchIndex),  [isComponent: true, name: getSwitchDeviceName(switchIndex), label: getSwitchDeviceLabel(switchIndex)])
                switchDevice.updateDataValue("component", "switch")
                switchDevice.updateDataValue("EP", switchIndex as String)
                logInfo("Spawned child device for the switch ${switchIndex}")
            }

            validDeviceNetworksIDs << getSwitchDeviceNetworkId(switchIndex)
            
            tSensorIndex = "switch" + switchIndex
            tSensorDevice = getTemperatureSensorDevice(tSensorIndex)
            if(null == tSensorDevice) {
                tSensorDevice = addChildDevice("hubitat", "Generic Component Temperature Sensor", getTemperatureSensorDeviceNetworkId(tSensorIndex),  [isComponent: true, name: getTemperatureSensorDeviceName(tSensorIndex), label: getTemperatureSensorDeviceLabel(tSensorIndex)])
                // redirect 'refresh' call to the 'switch' component
                // see comment in the switch child devices section
                tSensorDevice.updateDataValue("component", "switch") 
                tSensorDevice.updateDataValue("EP", switchIndex as String)
                logInfo("Spawned child device for the switch temperature sensor ${switchIndex}")
            }

            validDeviceNetworksIDs << getTemperatureSensorDeviceNetworkId(tSensorIndex)
            
        })

        numTSensors?.each({
            Short tSensorIndex = it
            tSensorDevice = getTemperatureSensorDevice(tSensorIndex)
            if(null == tSensorDevice) {
                tSensorDevice = addChildDevice("hubitat", "Generic Component Temperature Sensor", getTemperatureSensorDeviceNetworkId(tSensorIndex),  [isComponent: true, name: getTemperatureSensorDeviceName(tSensorIndex), label: getTemperatureSensorDeviceLabel(tSensorIndex)])
                tSensorDevice.updateDataValue("component", "temperature")
                tSensorDevice.updateDataValue("EP", tSensorIndex as String)
                logInfo("Spawned child device for the temperature sensor ${tSensorIndex}")
            }

            validDeviceNetworksIDs << getTemperatureSensorDeviceNetworkId(tSensorIndex)
        })
        
        componentMap.thermostat?.each({
            Short thermostatIndex = it
            thermostatDevice = getThermostatDevice(thermostatIndex)
            if(null == thermostatDevice) {
                thermostatDevice = addChildDevice("hubitat", "Generic Component Thermostat", getThermostatDeviceNetworkId(thermostatIndex),  [isComponent: true, name: getThermostatDeviceName(thermostatIndex), label: getThermostatDeviceLabel(thermostatIndex)])
                thermostatDevice.updateDataValue("component", "thermostat")
                thermostatDevice.updateDataValue("EP", thermostatIndex as String)
                thermostatDevice.parse([
                    [name: "supportedThermostatModes",    value: ["heat", "off"], descriptionText: "Initial value"],
                    // no fan; so only auto
                    [name: "supportedThermostatFanModes", value: ["auto"],        descriptionText: "Initial value"],
                    [name: "thermostatFanMode",           value: "auto",          descriptionText: "Initial value"],
                    // other attributes
                    [name: "heatingSetpoint",             value: "unknown",       descriptionText: "Initial value"],
                    [name: "temperature",                 value: "unknown",       descriptionText: "Initial value"],
                    [name: "thermostatMode",              value: "unknown",       descriptionText: "Initial value"],
                    [name: "thermostatOperatingState",    value: "unknown",       descriptionText: "Initial value"],
                    [name: "thermostatSetpoint",          value: "unknown",       descriptionText: "Initial value"]
                ])
                logInfo("Spawned child device for the thermostat ${thermostatIndex}")
            }

            validDeviceNetworksIDs << getThermostatDeviceNetworkId(thermostatIndex)
        })
        
        componentMap.voltmeter?.each({
            Short voltmeterIndex = it
            voltmeterDevice = getVoltageSensorDevice(voltmeterIndex)
            if(null == voltmeterDevice) {
                voltmeterDevice = addChildDevice("hubitat", "Generic Component Voltage Sensor", getVoltageSensorDeviceNetworkId(voltmeterIndex),  [isComponent: true, name: getVoltageSensorDeviceName(voltmeterIndex), label: getVoltageSensorDeviceLabel(voltmeterIndex)])
                voltmeterDevice.updateDataValue("component", "voltmeter")
                voltmeterDevice.updateDataValue("EP", voltmeterIndex as String)
                logInfo("Spawned child device for the voltage sensor ${voltmeterIndex}")
            }

            validDeviceNetworksIDs << getVoltageSensorDeviceNetworkId(voltmeterIndex)
        })
        
        if(null != numVHSensors) {
            numVHSensors = numVHSensors as int
            for(Short hSensorIndex = 1; hSensorIndex < numVHSensors + 1; ++hSensorIndex) {
                hSensorDevice = getVirtualHumiditySensorDevice(hSensorIndex)
                if(null == hSensorDevice) {
                    hSensorDevice = addChildDevice("hubitat", "Generic Component Humidity Sensor", getVirtualHumiditySensorDeviceNetworkId(hSensorIndex),  [isComponent: true, name: getVirtualHumiditySensorDeviceName(hSensorIndex), label: getVirtualHumiditySensorDeviceLabel(hSensorIndex)])
                    hSensorDevice.updateDataValue("component", "none")
                    hSensorDevice.updateDataValue("EP", hSensorIndex as String)
                    logInfo("Spawned child device for the virtual relative humidity sensor ${hSensorIndex}")
                }
                
                validDeviceNetworksIDs << getVirtualHumiditySensorDeviceNetworkId(hSensorIndex)
            }
        }
        
        if(null != numVTSensors) {
            numVTSensors = numVTSensors as int
            for(Short tSensorIndex = 1; tSensorIndex < numVTSensors + 1; ++tSensorIndex) {
                tSensorDevice = getVirtualTemperatureSensorDevice(tSensorIndex)
                if(null == tSensorDevice) {
                    tSensorDevice = addChildDevice("hubitat", "Generic Component Temperature Sensor", getVirtualTemperatureSensorDeviceNetworkId(tSensorIndex),  [isComponent: true, name: getVirtualTemperatureSensorDeviceName(tSensorIndex), label: getVirtualTemperatureSensorDeviceLabel(tSensorIndex)])
                    tSensorDevice.updateDataValue("component", "none")
                    tSensorDevice.updateDataValue("EP", tSensorIndex as String)
                    logInfo("Spawned child device for the virtual temperature sensor ${tSensorIndex}")
                }
                
                validDeviceNetworksIDs << getVirtualTemperatureSensorDeviceNetworkId(tSensorIndex)
            }
        }
        
        getChildDevices().each {
            if(!validDeviceNetworksIDs.contains(it.deviceNetworkId)) {
                deleteChildDevice("${it.deviceNetworkId}")
            }
        }
    }
}

private static int extractNanosecondDecimal(BigDecimal value, long integer) {
    def BigDecimal ONE_BILLION = new BigDecimal(1000000000L)
    return value.subtract(new BigDecimal(integer)).multiply(ONE_BILLION).intValue()
}

private def toInstant(BigDecimal value) {
    if(null != value) {
        long seconds = value.longValue()
        int nanoseconds = extractNanosecondDecimal(value, seconds)
        return Instant.ofEpochSecond(seconds, nanoseconds).atZone(location.timeZone.toZoneId())
    }
    return null
}

private static List CalibrationWhite() {
    // Should be 6500K or [255, 255, 255]. But 4100 gives better result
    // ToDo: check for different RGB LED strips; check for RGBW strips 
    return temperatureColorRGB(4100, 100)
}

//=============================================================================================================================================================
// Digest
//=============================================================================================================================================================

private encode(String algorithm, String value) {
	java.security.MessageDigest.getInstance(algorithm).digest(value.getBytes("UTF-8")).encodeHex().toString()
}

private Map getDigestObject(params) {
    def ha1         = encode(params.algorithm, "${params.username}:${params.realm}:${params.password}")
    def ha2         = encode(params.algorithm, params.method + ":" + params.uri)
    params.response = encode(params.algorithm, ha1 + ":" + params.nonce + ":" + params.nc + ":" + params.cnonce + ":" + params.qop + ":" + ha2)
    return params
}

// For HTTP
private Map getDigestHeader(params) {
    params = getDigestObject(params)
    return [ Authorization :
            "Digest " + 
            "username=\"${params.username}\", realm=\"${params.realm}\", qop=${params.qop}, algorithm=${params.algorithm}, " +
            "uri=\"${params.uri}\", " + 
            "nonce=\"${params.nonce}\", nc=${params.nc}, cnonce=\"${params.cnonce}\", opaque=\"\", " +
            "response=\"${params.response}\""
    ]
}

// For WS
private String getDigestObjectString(params) {
    params = getDigestObject(params)
    return "{\"username\": \"${params.username}\", \"realm\": \"${params.realm}\", \"algorithm\": \"${params.algorithm}\", \"nonce\": \"${params.nonce}\", \"cnonce\": \"${params.cnonce}\", \"response\": \"${params.response}\"}"
}

private Map getShellyDigestParamsHTTP(challenge, int action, query) {
    def queryStr = ""
    
    if(null != query) {
        query.eachWithIndex { entry, index ->
            queryStr += "${(index == 0) ? "?" : "&"}${entry.key}=${entry.value}"
        }
    }
    
    return [
        method: "GET",
        uri: devicePath + "/" + shellyCommands[action].method + queryStr,
        username: "admin",
        password: devicePassword,
        realm: challenge.realm,
        qop: "auth",                             // one of challenge.qop
        algorithm: challenge.algorithm,
        nonce: challenge.nonce,
        nc: "1",
        cnonce: challenge.nonce // lazy cnounce generation instead of java.util.UUID.randomUUID().toString().replace('-', '').substring(0, 8)
    ]
}

private Map getShellyDigestParamsWS(challenge) {
    return [
        method: "dummy_method",
        uri: "dummy_uri",
        username: "admin",
        password: devicePassword,
        realm: challenge.realm,
        qop: "auth",                             // one of challenge.qop
        algorithm: challenge.algorithm,
        nonce: challenge.nonce,
        nc: "1",
        cnonce: challenge.nonce // lazy cnounce generation instead of java.util.UUID.randomUUID().toString().replace('-', '').substring(0, 8)
    ]
}

//=============================================================================================================================================================
// Shelly HTTP/WS
//=============================================================================================================================================================

void parseAsyncResponse(response, data) {
	if(response.hasError()) {
        if(response.getStatus() == 401) {
            if(null == data.auth) {
                logInfo("HTTP authtentification sequence... ")
            
                Map challenge = response.getHeaders()["WWW-Authenticate"].replace(",", "").split(" ").collectEntries({
                    def entry = it.replace("\"", "").trim().split("=")
                    entry.length > 1 
                        ? [(entry[0]) : entry[1]] 
                        : [authorization : entry[0]]
                })
            
                data.request.headers = getDigestHeader(getShellyDigestParamsHTTP(challenge, data.id as int, data.request.query))
                
                // Any value. Just to identify if cretential were already provided (to avoid infinite loop)
                data.auth = "Digest"
                
                asynchttpGet("parseAsyncResponse", data.request, data)
            }
            else {
                logError("Unauthorized HTTP query. Please, check your password.")
            }
        }
        else {
            logError("Can't get device status for ${device.label}: ${response.getErrorMessage()}")
        }
    }
    else if(response.getStatus() != 200) {
        logError("Got rejected by ${device.label}")
    }
    else if (response.getHeaders()["Content-Type"].contains('application/json') == false) {
        logError("Got ${response.getHeaders()["Content-Type"]} instead of 'application/json' from ${device.label}")
    }    
    else {
        logDebug("HTTP Async Response: ${data} ::: ${response.getJson()}")
        if(null != data?.id) {
            if(null != response.getJson()) {
                parseShellyResponse(data.id as int, response.getJson())
            }
        }
        else {
            logError("HTTP Response: missing query id ${data} for ${notification}")
        }
    }
}

private void sendShellyCommandHTTP(int action, arguments = [:]) {
    logDebug("HTTP Async query: " + shellyCommands[action].method + "(${arguments})")
    
    def request = [
        uri: "http://" + deviceURL, path: devicePath + "/" + shellyCommands[action].method,
        requestContentType: "application/json"
    ]
    
    if(!arguments.isEmpty()) {
        request.query = arguments
    }

    asynchttpGet("parseAsyncResponse", request, [id: action, request: request])
}

private void syncShellyCommandHTTP(int action, arguments = [:]) {
    logDebug("HTTP Sync query: " + shellyCommands[action].method + "(${arguments})")

    def request = [
        uri: "http://" + deviceURL, path: devicePath + "/" + shellyCommands[action].method,        
        requestContentType: "application/json"
    ]
    
    if(!arguments.isEmpty()) {
        request.query = arguments
    }

    try {
        httpGet(request, { response ->
            if(!response.isSuccess()) {
                logError("Can't get device status for ${device.label}: ${response.getErrorMessage()}")
            }
            else if(response.getStatus() != 200) {
                logError("Got rejected by ${device.label}")
            }
            else {
                logDebug("HTTP Sync Response: ${response.getData()}")
                if(null != response.getData()) {
                    parseShellyResponse(action, response.getData())
                }
            }
        })
    }
    catch(Exception e) {
        logDebug("httpGet incident: ${e}")
        
        def response = e.getResponse()
        if(response.getStatus() == 401) {
            logDebug("HTTP authtentification sequence... ")
            
            Map challenge = [:]
            response.getHeaders("WWW-Authenticate").each({
                // Digest qop="auth", realm="shellypro2pm-xxxxxxxx", nonce="6453f80a", algorithm=SHA-256
                challenge << it.getValue().replace(",", "").split(" ").collectEntries({ 
                    def entry = it.replace("\"", "").trim().split("=")
                    entry.length > 1 
                        ? [(entry[0]) : entry[1]] 
                        : [authorization : entry[0]]
                })
            })
            
            request.headers = getDigestHeader(getShellyDigestParamsHTTP(challenge, action, request.query))
            
            httpGet(request, { authresponse ->
                if(!authresponse.isSuccess()) {
                    logError("Can't get authorized device status for ${device.label}: ${response.getErrorMessage()}")
                }
                else if(authresponse.getStatus() != 200) {
                    logError("Got rejected by ${device.label}")
                }
                else {
                    if(null != authresponse.getData()) {
                        parseShellyResponse(action, authresponse.getData())
                    }
                }
            })
        }
        else {
            throw e
        }        
    }
}

private void sendShellyCommandWS(int action, arguments = [:]) {
    if(device.currentValue("link", true) == "online") {
        query =  ['"params"' : arguments]              // switch id
        query << ['"id"'     : "${action}"]            // request id (ideally should be different each time to identify which response it to which query)
        query << ['"method"' : '"' + shellyCommands[action].method + '"']
        query << ['"src"'    : '"hub"']
    
        if(null != state.wsChallenge) {
            query << ['"auth"' : getDigestObjectString(getShellyDigestParamsWS(state.wsChallenge))]
        }
    
        queryString = query as String
    
        // Expects objects, not arrays (so a bit of cheating)
        queryString = queryString.replace('[','{').replace(']','}')
    
        logDebug("WS query: " + queryString)
        interfaces.webSocket.sendMessage(queryString)
    }
    else {
        logWarn("Skipping WS query: Link is offline")
    }
}

//=============================================================================================================================================================
// Own interface
//=============================================================================================================================================================

void checkForUpdates() {
    sendShellyCommandHTTP(CmdShellyCheckForUpdate())
}

void configure() {
    disconnect()

    if(configuration == "clean") {
        // Most of the values are set to "n/a". The proper state will arrive right after the first refresh call.
        def preset = [
            [name: "cloud",       value: "n/a",     descriptionText: "Initial value"],
            [name: "ethernet",    value: "n/a",     descriptionText: "Initial value"],
            [name: "fwUpdate",    value: "none",    descriptionText: "Initial value"],
            [name: "healthStatus",value: "offline", descriptionText: "Initial value"],
            [name: "link",        value: "offline", descriptionText: "Initial value"],
            [name: "mqtt",        value: "n/a",     descriptionText: "Initial value"],
            [name: "modbus",      value: "n/a",     descriptionText: "Initial value"],
            [name: "rssi",        value: "n/a",     descriptionText: "Initial value", unit: "%"],
            [name: "wifi",        value: "n/a",     descriptionText: "Initial value"],
        ]
        parse(preset)
    }
    
    try {
        // synchronous commands. Each step requires results of the previous one. 
        //    So response needs to arrive before the subsequent query!
        
        // get general public device info
        syncShellyCommandHTTP(CmdShellyGetDeviceInfo())

        // gather components; login here if needed
        syncShellyCommandHTTP(CmdShellyGetConfig())

        configureChildDevices()
        
        // propagate component configurations to the spawned child devices
        syncShellyCommandHTTP(CmdShellyGetConfig())
    }
    catch (Exception e) {
        logError("configure: ${e.message}. Can't configure child devices. Please, re-try.")
    }
    
    updateVersionInfo()

    updated() // -> connect -> refresh
}

void connect() {
    unschedule("connect")
    state.wsChallenge = null
    state.wsKeepAlive = true
    interfaces.webSocket.connect("ws://" + deviceURL + devicePath)
}

void disconnect() {
    unschedule("connect")
    state.wsKeepAlive = null
    interfaces.webSocket.close()
}

void initialize() {    
    connect()
}

def installed() {
    configure()
}

def refresh() {
    // Get full status instead of spamming with small commads
    sendShellyCommandHTTP(CmdShellyGetStatus())
}

def reboot() {
    sendShellyCommandHTTP(CmdShellyReboot())
}

def uninstalled() {
}

def updated() {
    disconnect()
    
    configureChildDevices()
        
    // delay call; connection and disconnection calls are async. State might not get updated in between if both are called directly near to each other 
    runIn(2, "connect")
    
    state.help = "Press 'Configure' after changing device profile to adjust driver children accordingly. <a href='http://${deviceURL}'>Go to device</a>"
    state.info1 = "To get instant device events it needs to have local connection from the hub live: link=online"
    
    state.testScript = """
<pre>
// =====================================================================================
// Use this script to test driver-ShellyScript interation (and as a base)
// Script child devices appear after creating script and 'configuring' driver afterwards

let alertTimer = null;
let eventCount = 0;

function startTimer() {
  // 1S step is extremely aggressive for Hubitat. Set only for debugging/testing!
  let timeStep = 1 * 1000; // 1S
  alertTimer = Timer.set(timeStep, true, function (userData) {
    ++eventCount;
    // Would be nice to use restricted event name 'variable'. 
    // This will distinguish internal events and events adressed to the Hubitat hub

    // data can be of any JSON type. But will be passed as String
    Shelly.emitEvent("variable", eventCount);
    Shelly.emitEvent("number", eventCount);
    Shelly.emitEvent("string", eventCount);
    Shelly.emitEvent("text", eventCount);

    // ToDo:
    // Shelly.emitEvent("virtual", {topic: temperature, id: 1, unit: C, value: eventCount});
  }, null/*userData*/ );
}

startTimer()
// =====================================================================================
</pre>
"""
}

void updateFirmware(def stage) {
    sendShellyCommandHTTP(CmdShellyUpdate(), [stage: stage])
}

void updateVersionInfo() {
    sendShellyCommandHTTP(CmdShellyGetDeviceInfo())
}

//=============================================================================================================================================================
// Component devices interface
//=============================================================================================================================================================

void componentParse(cd, List<Map> events) {
    // stub (in case if there is a need to intercept component attribute change)
}

void componentRefresh(cd) {
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'refresh' request from ${cd.displayName} (EP${endPoint})")

        component = cd.getDataValue("component")
        if(null != component) { 
            if(component == "cover") {
                sendShellyCommandWS(CmdCoverGetStatus(), [id:endPoint - 1])
            }
            else if(component == "devicepower") {
                sendShellyCommandWS(CmdDevicePowerGetStatus(), [id:endPoint - 1])
            }
            else if(component == "em") {
                sendShellyCommandWS(CmdEMGetStatus(), [id:endPoint - 1])
                sendShellyCommandWS(CmdEMDataGetStatus(), [id:endPoint - 1])    
            }
            else if(component == "em1") {
                sendShellyCommandWS(CmdEM1GetStatus(), [id:endPoint - 1])
                sendShellyCommandWS(CmdEM1DataGetStatus(), [id:endPoint - 1])    
            }
            else if(component == "humidity") {
                sendShellyCommandWS(CmdHumidityGetStatus(), [id:endPoint - 1])
            }
            else if(component == "illuminance") {
                sendShellyCommandWS(CmdIlluminanceGetStatus(), [id:endPoint - 1])
            }
            else if(component == "input") {
                sendShellyCommandWS(CmdInputGetStatus(), [id:endPoint - 1])
            }
            else if(component == "light") {
                sendShellyCommandWS(CmdLightGetStatus(), [id:endPoint - 1])
            }
            else if(component == "rgb") {
                sendShellyCommandWS(CmdRGBGetStatus(), [id:endPoint - 1])
            }
            else if(component == "rgbw") {
                sendShellyCommandWS(CmdRGBWGetStatus(), [id:endPoint - 1])
            }
            else if(component == "pm1") {
                sendShellyCommandWS(CmdPM1GetStatus(), [id:endPoint - 1])
            }
            else if(component == "script") {
                sendShellyCommandWS(CmdScriptGetStatus(), [id:endPoint])
            }
            else if(component == "switch") {
                sendShellyCommandWS(CmdSwitchGetStatus(), [id:endPoint - 1])
            }
            else if(component == "temperature") {
                sendShellyCommandWS(CmdTemperatureGetStatus(), [id:endPoint - 1])
            }
            else if(component == "thermostat") {
                sendShellyCommandWS(CmdThermostatGetStatus(), [id:endPoint - 1])
            }
            else if(component == "voltmeter") {
                sendShellyCommandWS(CmdVoltmeterGetStatus(), [id:endPoint - 1])
            }
        }
    }
}

// Components interface

void componentOn(cd) {
    // endPoint = '0' - self, '1..' - children
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'on' request from ${cd.displayName} (EP${endPoint})")        
        component = cd.getDataValue("component")
        if(null != component) { 
            if(component == "light") {
                sendShellyCommandHTTP(CmdLightSet(), [id:endPoint - 1, on: true])
            }
            else if(component == "rgb") {        
                sendShellyCommandHTTP(CmdRGBSet(), [id:endPoint - 1, on: true])
            }
            else if(component == "rgbw") {        
                sendShellyCommandHTTP(CmdRGBWSet(), [id:endPoint - 1, on: true])
            }
            else if(component == "switch") {        
                sendShellyCommandHTTP(CmdSwitchSet(), [id:endPoint - 1, on: true])
            }
        }
    }
}

void componentOff(cd){
    // endPoint = '0' - self, '1..' - children
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'off' request from ${cd.displayName} (EP${endPoint})")
        component = cd.getDataValue("component")
        if(null != component) { 
            if(component == "light") {
                sendShellyCommandHTTP(CmdLightSet(), [id:endPoint - 1, on: false])
            }
            else if(component == "rgb") {        
                sendShellyCommandHTTP(CmdRGBSet(), [id:endPoint - 1, on: false])
            }
            else if(component == "rgbw") {        
                sendShellyCommandHTTP(CmdRGBWSet(), [id:endPoint - 1, on: false])
            }
            else if(component == "switch") {        
                sendShellyCommandHTTP(CmdSwitchSet(), [id:endPoint - 1, on: false])
            }
        }        
    }
}

void componentSetLevel(cd, level, transitionTime = null) {
    // endPoint = '0' - self, '1..' - children
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received setLevel(${level}, ${transitionTime}) request from ${cd.displayName} (EP${endPoint})")
        // ToDo:
        //    transition_duration: transitionTime
        component = cd.getDataValue("component")
        if(null != component) { 
            if(component == "light") {
                sendShellyCommandHTTP(CmdLightSet(), [id:endPoint - 1, brightness: level])
            }
            else if(component == "rgb") {        
                sendShellyCommandHTTP(CmdRGBSet(), [id:endPoint - 1, brightness: level])
            }
            else if(component == "rgbw") {        
                sendShellyCommandHTTP(CmdRGBWSet(), [id:endPoint - 1, brightness: level])
            }
        }
    }
}

void componentStartLevelChange(cd, direction) {
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received startLevelChange(${direction}) request from ${cd.displayName} (EP${endPoint})")
        
        def down = (direction == "down") as boolean
        
        component = cd.getDataValue("component")
        if(null != component) { 
            if(component == "rgb") {        
                sendShellyCommandHTTP(down ? CmdRGBDimDown() : CmdRGBDimUp(), [id:endPoint - 1])
            }
            else if(component == "rgbw") {        
                sendShellyCommandHTTP(down ? CmdRGBWDimDown() : CmdRGBWDimUp(), [id:endPoint - 1])
            }
        }
    }
}

void componentStopLevelChange(cd) {
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received stopLevelChange request from ${cd.displayName} (EP${endPoint})")
        
        component = cd.getDataValue("component")
        if(null != component) { 
            if(component == "rgb") {        
                sendShellyCommandHTTP(CmdRGBDimStop(), [id:endPoint - 1])
            }
            else if(component == "rgbw") {        
                sendShellyCommandHTTP(CmdRGBWDimStop(), [id:endPoint - 1])
            }
        }
    }
}


void componentSetColor(cd, colormap) { // [hue*:(0 to 100), saturation*:(0 to 100), level:(0 to 100)]
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received setColor(${colormap}) request from ${cd.displayName} (EP${endPoint})")
        
        component = cd.getDataValue("component")
        if(null != component) { 
            if(component == "rgb") {
                //sendShellyCommandHTTP(CmdRGBDimStop(), [id:endPoint - 1])
                //setSwitchColor(colormap.hue, colormap.saturation, colormap.level)
            }
            else if(component == "rgbw") {        
                //sendShellyCommandHTTP(CmdRGBWDimStop(), [id:endPoint - 1])
                //setSwitchColor(colormap.hue, colormap.saturation, colormap.level)
            }
        }
    }
    
    //getDriver(cd)?.parse([[name: "colorMode", value: "RGB"]])    
}

void componentSetColorTemperature(cd, temp, level, duration) {
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received setColorTemperature(${temp}, ${level}, ${duration}) request from ${cd.displayName} (EP${endPoint})")
    }
    
    //if(null == level) {
    //    level = cd.latestValue("level") as int
    //}
    
    //setSwitchTemp(temp, level)
    
    //getDriver(cd)?.parse([
    //    [name: "colorMode", value: "CT"],
    //    [name: "colorTemperature", value: temp, unit: "°K", descriptionText:""]
    //])
}

void componentSetHue(cd, hue) {
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received setHue(${saturation}) request from ${cd.displayName} (EP${endPoint})")
    }
    
    //List<Map> events = [
    //    [name: "colorMode", value: "RGB"]
    //]
    
    //def switchState = (cd.latestValue("switch") == "on") as boolean
    
    //if(switchState) {
    //    Short endPoint = cd.getDataValue("EP") as Short
    //    if(endPoint < 2) {
                        
    //        def saturation = cd.latestValue("saturation") as int
    //        def level = cd.latestValue("level") as int
    //        setSwitchColor(hue, saturation, level)
    //    }
    //}
    //else {  
    //    events += [name: "hue", value: hue as int, descriptionText:"Set offline 'hue' to ${hue}"]
    //}
    
    //getDriver(cd)?.parse(events)
}

void componentSetSaturation(cd, saturation) {
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received setSaturation(${saturation}) request from ${cd.displayName} (EP${endPoint})")
    }
    
    //List<Map> events = [
    //    [name: "colorMode", value: "RGB"]
    //]
    
    //def switchState = (cd.latestValue("switch", true) == "on") as boolean
    
    //if(switchState) {
    //    Short endPoint = cd.getDataValue("EP") as Short
    //    if(endPoint < 2) {
    //        def hue = cd.latestValue("hue") as int
    //        def level = cd.latestValue("level") as int
    //        setSwitchColor(hue, saturation, level)
    //    }
    //}
    //else {  
    //    events += [name: "saturation", value: saturation as int, descriptionText:"Set offline 'saturation' to ${saturation}%"]
    //}
    
    //getDriver(cd)?.parse(events)
}

void componentOpen(cd) {
    // endPoint = '0' - self, '1..' - children
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'open' request from ${cd.displayName} (EP${endPoint})")
        sendShellyCommandHTTP(CmdCoverOpen(), [id:endPoint - 1])
    }
}

void componentClose(cd) {
    // endPoint = '0' - self, '1..' - children
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'close' request from ${cd.displayName} (EP${endPoint})")
        sendShellyCommandHTTP(CmdCoverClose(), [id:endPoint - 1])
    }
}
// stop and gotopos require one of the arguments
//     pos number
//            Required and mutually exclusive (at least one of them pos/rel be provided, but not both at the same time). 
//            pos represents target position in %, allowed range [0..100]. If rel is provided, Cover will move to a 
//            target_position = current_position + rel. If the value of rel is so big that it results in overshoot 
//            (i.e. target_position is beyond fully open / fully closed), target_position will be silently capped to 
//            fully open / fully closed
//    rel number
//            Required and mutually exclusive (at least one of them pos/rel be provided, but not both at the same time).
//            rel represents relative move in %, allowed range [-100..100]

// (position) position required (NUMBER) - Shade position (0 to 100)
void componentSetPosition(cd, pos) {
    // endPoint = '0' - self, '1..' - children
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'setPosition(${pos})' request from ${cd.displayName} (EP${endPoint}); will fail if device is not calibrated")
        sendShellyCommandHTTP(CmdCoverGoToPosition(), [id:endPoint - 1, pos: pos])
    }
}
// (direction) direction required (ENUM) - Direction for position change request ["open", "close"]
void componentStartPositionChange(cd, dir) {
    // endPoint = '0' - self, '1..' - children
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'startPositionChange(${dir})' request from ${cd.displayName} (EP${endPoint}); will fail if device is not calibrated")
        sendShellyCommandHTTP(CmdCoverGoToPosition(), [id:endPoint - 1, rel: (dir=="open" ? 100 : -100)])
    }
}

void componentStopPositionChange(cd) {
    // endPoint = '0' - self, '1..' - children
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'stopPositionChange' request from ${cd.displayName} (EP${endPoint})")
        sendShellyCommandHTTP(CmdCoverStop(), [id:endPoint - 1])
    }
}

void componentStartScript(cd) {
    // endPoint = '0' - self, '1..' - children
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'startScript' request from ${cd.displayName} (EP${endPoint})")
        sendShellyCommandHTTP(CmdScriptStart(), [id:endPoint])
    }
}

void componentStopScript(cd) {
    // endPoint = '0' - self, '1..' - children
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'stopScript' request from ${cd.displayName} (EP${endPoint})")
        sendShellyCommandHTTP(CmdScriptStop(), [id:endPoint])
    }
}

void componentSetHeatingSetpoint(cd, temp) {
    // endPoint = '0' - self, '1..' - children
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'componentSetHeatingSetpoint' request from ${cd.displayName} (EP${endPoint})")
        
        temperatureScale = getTemperatureScale()

        if(temperatureScale == "C") {
            sendShellyCommandHTTP(CmdThermostatSetConfig(), [id:endPoint - 1, config:"{target_C:${temp as Float}}"])
        }
        else if(temperatureScale == "F") {
            sendShellyCommandHTTP(CmdThermostatSetConfig(), [id:endPoint - 1, config:"{target_F:${temp as Float}}"])
        }
    }
}

void componentSetThermostatFanMode(cd, mode) {
    // nothing to do here
}

void componentSetThermostatMode(cd, mode) {
    // endPoint = '0' - self, '1..' - children
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'componentSetThermostatMode' request from ${cd.displayName} (EP${endPoint})")
        if(mode == "off") {
            sendShellyCommandHTTP(CmdThermostatSetConfig(), [id:endPoint - 1, config:"{enable:false}"])
        }
        else if(mode == "auto" || mode == "heat") {
            sendShellyCommandHTTP(CmdThermostatSetConfig(), [id:endPoint - 1, config:"{enable:true}"])
        }
    }
}

//======================================================================================================================
@Field static final List inputEvents = ["btn_down", "btn_up", "single_push", "double_push", "triple_push", "long_push"]

private void parseShellyEvent(event) {
    logDebug("parseShellyEvent ${event}")
    
    def timestamp = toInstant( event.ts )
    
    String componentType = event.component?.split(':').first().trim()
    if(componentType == "cover") {
        def endPointDevice = getCoverDevice((event.id + 1) as Short)
        logDebug("${timestamp}: (${endPointDevice}) Cover event ${event}")
    }
    else if(componentType == "devicepower") {
        def endPointDevice = getBatteryDevice((event.id + 1) as Short)
        logDebug("${timestamp}: (${endPointDevice}) Battery event ${event}")
    }
    else if(componentType == "em") {
        //def endPointDevice = getPowerMeterDevice((event.id + 1) as Short)
        logDebug("${timestamp}: Energy meter event ${event}")
    }
    else if (componentType == "emdata") {
        logInfo("${timestamp}: EMData event ${event}")

        if(null != event.event && event.event == "data") {
            // component:emdata:0, id:0, event:data, ts:1631266595.43, data:[
            //  { ts:0, period:60, values:[[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]] }
            // ], } 
        }
    }
    else if(componentType == "em1") {
        //def endPointDevice = getPowerMeterDevice((event.id + 1) as Short)
        logDebug("${timestamp}: Energy meter 1 event ${event}")
    }
    else if (componentType == "em1data") {
        logInfo("${timestamp}: EM1Data event ${event}")

        if(null != event.event && event.event == "data") {
            // component:emdata:0, id:0, event:data, ts:1631266595.43, data:[
            //  { ts:0, period:60, values:[[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]] }
            // ], } 
        }
    }
    else if(componentType == "humidity") {
        def endPointDevice = getHumiditySensorDevice((event.id + 1) as Short)
        logDebug("${timestamp}: (${endPointDevice}) Humidity sensor event ${event}")        
    }
    else if(componentType == "illuminance") {
        def endPointDevice = getIlluminanceSensorDevice((event.id + 1) as Short)
        logDebug("${timestamp}: (${endPointDevice}) Illuminance sensor event ${event}")
    }
    else if(componentType == "input") {
        def endPointDevice = getInputDevice((event.id + 1) as Short)
        logDebug("${timestamp}: (${endPointDevice}) Input event ${event}")

        def endPointEvents = []
        if(null != event.event) {
            String inputEvent = (event.event as String).replace('[','').replace(']','')
            // Filter out unrelated default events. Pass only input events.
            if(inputEvents.contains(inputEvent)) {                
                if(inputEvent == "btn_down") {
                    endPointEvents << [name: "contact", value: "closed", descriptionText: "Contact was closed"]
                } 
                else if(inputEvent == "btn_up") {
                    endPointEvents << [name: "contact", value: "open", descriptionText: "Contact was open"]
                    endPointEvents << [name: "released", value: 1, descriptionText: "Button was released [device]", isStateChange: true]
                }
                else if(inputEvent == "single_push") {
                    endPointEvents << [name: "pushed", value: 1, descriptionText: "Button was pushed [device]", isStateChange: true]
                }
                else if(inputEvent == "double_push") {
                    endPointEvents << [name: "doubleTapped", value: 1, descriptionText: "Button was tapped twice [device]", isStateChange: true]
                    endPointEvents << [name: "pushed", value: 2, descriptionText: "Button was pushed [device]", isStateChange: true]
                }
                else if(inputEvent == "triple_push") {
                    endPointEvents << [name: "pushed", value: 3, descriptionText: "Button was pushed [device]", isStateChange: true]
                }
                else if(inputEvent == "long_push") {
                    endPointEvents << [name: "held", value: 1, descriptionText: "Button is held [device]", isStateChange: true]
                    endPointEvents << [name: "pushed", value: 4, descriptionText: "Button was pushed [device]", isStateChange: true]
                }
                // send event always
                endPointEvents << [name: "event", value: inputEvent, isStateChange: true, descriptionText: "Got event: ${inputEvent}"]
            }
        }
        parse(endPointEvents, endPointDevice)
    }
    else if(componentType == "light") {
        def endPointDevice = getDimmerDevice((event.id + 1) as Short)
        logDebug("${timestamp}: (${endPointDevice}) Dimmer event ${event}")
    }
    else if(componentType == "rgb") {
        def endPointDevice = getRGBDevice((event.id + 1) as Short)
        logDebug("${timestamp}: (${endPointDevice}) RGB event ${event}")
    }
    else if(componentType == "rgbw") {
        def endPointDevice = getRGBWDevice((event.id + 1) as Short)
        logDebug("${timestamp}: (${endPointDevice}) RGBW event ${event}")
    }
    else if(componentType == "pm1") {
        def endPointDevice = getPowerMeterDevice((event.id + 1) as Short)
        logDebug("${timestamp}: (${endPointDevice}) Power meter event ${event}")
    }
    else if (componentType == "script") {
        def endPointDevice = getScriptDevice(event.id as Short)
        if(null != endPointDevice) {
            logDebug("${timestamp}: (${endPointDevice}) Script event ${event}")

            // { "component": "script:1", "id": 1, "event": "this_happened", "data": { "why": 42, "what": "when" }, "ts": 1657878122.44 }
            if(null != event.event) {
                if(event.event == "config_changed") {
                    logInfo("${timestamp}: (${endPointDevice}) Script configuration changed. Configuration revision ${event.cfg_rev}")
                }
                else {
                    // ignore 'config_changed' to keep attribute clean
                    
                    if(event.event == "number") {
                        parse([[name: "customNumericAttribute", value: event.data as Number, descriptionText: "Scripted event numeric custom data [device]"]], endPointDevice)
                    }
                    else if (event.event == "string" || event.event == "text") {
                        parse([[name: "customTextAttribute", value: event.data as String, descriptionText: "Scripted event text custom data [device]"]], endPointDevice)
                    }
                    else if (event.event == "variable") {
                        parse([[name: "variable", value: event.data as String, isStateChange: true, descriptionText: "Scripted event [device]"]], endPointDevice)
                    }
                }
            }
        }
        else {
            logWarn("${timestamp}: Unconfigured (${event.id}) script instance event ${event}")
        }
    }
    else if(componentType == "switch") {
        def endPointDevice = getSwitchDevice((event.id + 1) as Short)
        logDebug("${timestamp}: (${endPointDevice}) Switch event ${event}")
    }
    else if (componentType == "sys") {
        logDebug("${timestamp}: System event ${event}")
            
        if(null != event.event) {
            // msg:Waiting for data, progress_percent:12, component:sys, event:ota_progress, ts:1682244255.37
            // msg:Waiting for data, progress_percent:87, component:sys, event:ota_progress, ts:1682244273.96
            // msg:Update applied, rebooting, component:sys, event:ota_success, ts:1682244277.73
            // component:sys, restart_required:false, cfg_rev:66, event:config_changed, ts:1695456845.32
            if(event.event == "ota_error") {
                parse([[name: "fwUpdate", value: "error"]])
            }
            else if(event.event == "ota_progress") {
                parse([[name: "fwUpdate", value: event.progress_percent, unit: "%"]])
            }
            else if(event.event == "ota_success") {
                parse([[name: "fwUpdate", value: "success"]])
                updateVersionInfo()
            }
            else if(event.event == "sys_btn_down") {
                def endPointDevice = getSysBtnDevice(1 as Short)
                logDebug("${timestamp}: (${endPointDevice}) System button event ${event}")
                parse([[name: "held", value: 1, descriptionText: "Button is held [device]", isStateChange: true]], endPointDevice)
            }
            else if(event.event == "sys_btn_up") {
                def endPointDevice = getSysBtnDevice(1 as Short)
                logDebug("${timestamp}: (${endPointDevice}) System button event ${event}")
                parse([[name: "released", value: 1, descriptionText: "Button was released [device]", isStateChange: true]], endPointDevice)
            }
            else if(event.event == "sys_btn_push") {
                def endPointDevice = getSysBtnDevice(1 as Short)
                logDebug("${timestamp}: (${endPointDevice}) System button event ${event}")
                parse([[name: "pushed", value: 1, descriptionText: "Button was pushed [device]", isStateChange: true]], endPointDevice)
            }
            else if(event.event == "config_changed") {
                logInfo("${timestamp}: Device configuration changed. Configuration revision ${event.cfg_rev}")
            }
        }
    }
    else if(componentType == "temperature") {
        def endPointDevice = getTemperatureSensorDevice((event.id + 1) as Short)
        logDebug("${timestamp}: (${endPointDevice}) Temperature sensor event ${event}")
    }
    else if(componentType == "thermostat") {
        def endPointDevice = getThermostatDevice((event.id + 1) as Short)
        logDebug("${timestamp}: (${endPointDevice}) Thermostat event ${event}")
    }
    else if (componentType == "wifi") {
        logDebug("${timestamp}: WiFi event ${event}")
        // wifi
        // event: "sta_connect_fail",
        // event: "sta_disconnected",
        // reason: https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/network/esp_wifi.html#_CPPv417wifi_err_reason_t
    }
    else {
        logWarn("${timestamp}: Unknown NotifyEvent event ${event}")
    }
}

private void parseShellyEvents(params) {
    logDebug("parseShellyEvents ${params}")
    
    def timestamp = toInstant( params.ts )
    logDebug("Timestamp: ${timestamp}")
    
    params?.each({
        String componentType = it.key.split(':').first().trim()
        if (componentType != "ts") {
            it.value?.each({
                parseShellyEvent(it)
            })
        }
    })
}

//======================================================================================================================

private void parseShellyCommonStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyCommonStatus ${params}")
    
    if(null != params.aenergy) {
        genericMeterEvent((params.aenergy.total as BigDecimal) / 1000, meterElectricEnergy, endPointDevice)
    }
    if(null != params.apower) {
        genericMeterEvent(params.apower as Float, meterElectricPower, endPointDevice)
    }
    if(null != params.current) {
        genericMeterEvent(params.current as Float, meterElectricCurrent, endPointDevice)
    }
    if(null != params.errors) {
        logWarn("(${endPointDevice}) Errors ${params.errors}")
        parse([[name: "error", value: params.errors]])
    }
    if(null != params.freq) {
        genericSensorEvent(params.freq as Float, 0x2000, endPointDevice)
    }
    if(null != params.pf) {
        genericMeterEvent(params.pf as Float, meterElectricPowerFactor, endPointDevice)
    }
    if(null != params.voltage) {
        genericMeterEvent(params.voltage as Float, meterElectricVoltage, endPointDevice)
    }
}

private void parseShellyCloudStatus(params) {
    logDebug("parseShellyCloudStatus ${params}")

    if(null != params.connected) {
        parse([[name: "cloud", value: params.connected ? "online" : "offline"]])
    }
}

private void parseShellyCoverStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyCoverStatus ${params}")
    
    def events = []
    
    if(null != params.current_pos) {
        events << [name: "position", value: params.current_pos, unit: "%"]
    }
    if(null != params.state) {
        if(params.state == "stopped") {
            params.state = "partially open"
        }
        events << [name: "windowShade", value: params.state]
    }
    if(null != params.pos_control) {
        logInfo("(${endPointDevice}) Cover Position control ${params.pos_control}")
    }
    if(null != params.move_timeout) {
        logInfo("(${endPointDevice}) Cover Move timeout ${params.move_timeout}")
    }
    if(null != params.errors) {
        logInfo("(${endPointDevice}) Cover Errors ${params.errors}")
        parse([[name: "error", value: params.errors]])
    }
    
    parse(events, endPointDevice)
    
    parseShellyCommonStatus(params, endPointDevice)
}

private void parseShellyDevicePowerStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyDevicePowerStatus ${params}")
    
    def events = []
    
    if(null != params.battery) {
        if(null != params.battery.V) {
            logInfo("(${endPointDevice}) Battery voltage ${params.battery.V}")
            events << [name: "batteryVoltage", value: params.battery.V as Float, unit: "V"]
        }
        if(null != params.battery.percent) {
            logInfo("(${endPointDevice}) Battery level ${params.battery.percent}")
            events << [name: "battery", value: params.battery.percent as Float, unit: "%"]
        }
    }
    if(null != params.errors) {
        logInfo("(${endPointDevice}) Device power errors ${params.errors}")
        parse([[name: "error", value: params.errors]])
    }
    if(null != params.external) {
        def batVal = (params.external as boolean) ? "idle" : "discharging"
        logInfo("(${endPointDevice}) Battery is ${batVal}")
        events << [name: "batteryState", value: batVal]
        events << [name: "powerSource", value: (params.external as boolean) ? "dc" : "battery"]
    }
    
    parse(events, endPointDevice)
}

private void parseShellyEnergyMeterStatus(params, endPointDevices) {
    logDebug("(${endPointDevices}) parseShellyEnergyMeterStatus ${params}")
    
    // Active power
    if(null != params.a_act_power) {
        genericMeterEvent(params.a_act_power as Float, meterElectricPower, endPointDevices[channelPhaseA])
    }
    if(null != params.b_act_power) {
        genericMeterEvent(params.b_act_power as Float, meterElectricPower, endPointDevices[channelPhaseB])
    }
    if(null != params.c_act_power) {
        genericMeterEvent(params.c_act_power as Float, meterElectricPower, endPointDevices[channelPhaseC])
    }
    if(null != params.total_act_power) {
        genericMeterEvent(params.total_act_power as Float, meterElectricPower, endPointDevices[channelTotal])
    }

    // Current
    if(null != params.a_current) {
        genericMeterEvent(params.a_current as Float, meterElectricCurrent, endPointDevices[channelPhaseA])
    }
    if(null != params.b_current) {
        genericMeterEvent(params.b_current as Float, meterElectricCurrent, endPointDevices[channelPhaseB])
    }
    if(null != params.c_current) {
        genericMeterEvent(params.c_current as Float, meterElectricCurrent, endPointDevices[channelPhaseC])
    }
    if(null != params.total_current) {
        genericMeterEvent(params.total_current as Float, meterElectricCurrent, endPointDevices[channelTotal])
    }
    if(null != params.n_current) {
        logInfo("(${endPointDevices}) Neutral line current: ${params.n_current}")
    }

    if(null != params.errors) {
        logWarn("(${endPointDevices}) Errors ${params.errors}")
        parse([[name: "error", value: params.errors]])
    }
    
    // Frequency
    if(null != params.a_freq) {
        genericSensorEvent(params.a_freq as Float, 0x2000, endPointDevices[channelPhaseA])
    }
    if(null != params.b_freq) {
        genericSensorEvent(params.b_freq as Float, 0x2000, endPointDevices[channelPhaseB])
    }
    if(null != params.c_freq) {
        genericSensorEvent(params.c_freq as Float, 0x2000, endPointDevices[channelPhaseC])
    }

    // Power factor
    if(null != params.a_pf) {
        genericMeterEvent(params.a_pf as Float, meterElectricPowerFactor, endPointDevices[channelPhaseA])
    }
    if(null != params.b_pf) {
        genericMeterEvent(params.b_pf as Float, meterElectricPowerFactor, endPointDevices[channelPhaseB])        
    }
    if(null != params.c_pf) {
        genericMeterEvent(params.c_pf as Float, meterElectricPowerFactor, endPointDevices[channelPhaseC])
    }

    // Voltage
    if(null != params.a_voltage) {
        genericMeterEvent(params.a_voltage as Float, meterElectricVoltage, endPointDevices[channelPhaseA])
    }
    if(null != params.b_voltage) {
        genericMeterEvent(params.b_voltage as Float, meterElectricVoltage, endPointDevices[channelPhaseB])
    }
    if(null != params.c_voltage) {
        genericMeterEvent(params.c_voltage as Float, meterElectricVoltage, endPointDevices[channelPhaseC])
    }
// More parameters
//  "a_aprt_power"
//  "b_aprt_power"
//  "c_aprt_power"
//  "total_aprt_power"
}

private void parseShellyEnergyMeterDataStatus(params, endPointDevices) {
    logDebug("(${endPointDevices}) parseShellyEnergyMeterDataStatus ${params}")

    if(null != params.a_total_act_energy) {
        genericMeterEvent((params.a_total_act_energy as BigDecimal) / 1000, meterElectricEnergy, endPointDevices[channelPhaseA])
    }
    if(null != params.b_total_act_energy) {
        genericMeterEvent((params.b_total_act_energy as BigDecimal) / 1000, meterElectricEnergy, endPointDevices[channelPhaseB])
    }
    if(null != params.c_total_act_energy) {
        genericMeterEvent((params.c_total_act_energy as BigDecimal) / 1000, meterElectricEnergy, endPointDevices[channelPhaseC])
    }
    if(null != params.total_act) {
        genericMeterEvent((params.total_act as BigDecimal) / 1000, meterElectricEnergy, endPointDevices[channelTotal])
    }
// More parameters
//  "a_total_act_ret_energy"
//  "b_total_act_ret_energy"
//  "c_total_act_ret_energy"
//  "total_act_ret"
}

private void parseShellyEnergyMeterStatus1(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyEnergyMeterStatus1 ${params}")
    
    // Active power
    if(null != params.act_power) {
        genericMeterEvent(params.act_power as Float, meterElectricPower, endPointDevice)
    }
    
    parseShellyCommonStatus(params, endPointDevice)
    
// More parameters
//  "aprt_power"
}

private void parseShellyEnergyMeterDataStatus1(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyEnergyMeterDataStatus1 ${params}")

    if(null != params.total_act_energy) {
        genericMeterEvent((params.total_act_energy as BigDecimal) / 1000, meterElectricEnergy, endPointDevice)
    }
    
// More parameters
//  "total_act_ret_energy"
}

private void parseShellyEthernetStatus(params) {
    logDebug("parseShellyEthernetStatus ${params}")
    
    def events = []

    if(null != params.ip) {
        events << [name: "ethernet", value: params.ip as String]
    }
    else {
        events << [name: "ethernet", value: "offline"]
    }
    
    parse(events)
}

private void parseShellyHumiditySensorStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyHumiditySensorStatus ${params}")
    
    if(null != params.rh) {
        genericSensorEvent(params.rh as Float, 0x0500, endPointDevice)
    }  
}

private void parseShellyIlluminanceSensorStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyIlluminanceSensorStatus ${params}")
    
    if(null != params.lux) {
        genericSensorEvent(params.lux as Float, 0x0301, endPointDevice)
    }
    if(null != params.illumination) {        
        //parse([[name: "illumination", value: params.illumination]])
        logInfo("(${endPointDevice}) Illumination ${params.illumination}")
    } 
}

private void parseShellyInputStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyInputStatus ${params}")
    // type: analog, button, count, switch
    def events = []

    if(null != params.state) {
        if(params.state) {
            events << [name: "contact", value: "closed", descriptionText: "Contact was closed"]
        } 
        else {
            events << [name: "contact", value: "open", descriptionText: "Contact was open"]
        }
    }
   
    if(null != params.counts) {
        if(null != params.total) {
            events << [name: "pulses", value: params.total, descriptionText: "Pulse count"]
        }
        
        if(null != params.xtotal) {
            events << [name: "pulsesEvaluated", value: params.xtotal, descriptionText: "Evaluated pulse count"]
        }
    }
    
    if(null != params.freq) {
        events << [name: "frequency", value: params.freq, descriptionText: "Pulse frquency"]
    }
    
    if(null != params.xfreq) {
        events << [name: "frequencyEvaluated", value: params.xfreq, descriptionText: "Evaluated pulse frequency"]
    }
    
    if(null != params.percent) {
        events << [name: "level", value: params.percent, descriptionText: "Signal percentage"]
    }
    
    if(null != params.xpercent) {
        events << [name: "levelEvaluated", value: params.xpercent, descriptionText: "Signal evaluated percentage"]
    }

    parse(events, endPointDevice)
    
    // state - boolean or null - (only for type switch, button) State of the input (null if the input instance is stateless, i.e. for type button) 
    // percent - number or null - (only for type analog) Analog value in percent (null if valid value could not be obtained)
    // errors - array of type string - Shown only if at least one error is present. May contain out_of_range, read
    
    parseShellyCommonStatus(params, endPointDevice)
}

private void parseShellyLightStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyLightStatus ${params}")
    
    def events = []
    
    if(null != params.brightness) {        
        events << [name: "level", value: params.brightness, unit: "%", descriptionText: "Dimmer was set to ${params.brightness}%"]
    }
    if(null != params.output) {        
        events << [name: "switch", value: params.output ? "on" : "off", descriptionText: "Dimmer was ${params.output ? "on" : "off"}"]
    }
    if(null != params.timer_duration) {
        logInfo("(${endPointDevice}) Timer duration ${params.timer_duration}")
    }
    if(null != params.timer_started_at) {
        logInfo("(${endPointDevice}) Timer started at ${params.timer_started_at}")
    }
    
    parse(events, endPointDevice)
    
    parseShellyCommonStatus(params, endPointDevice)
}

private void parseShellyRGBStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyRGBStatus ${params}")
    
    if(null != params.rgb) {
        def events = []
        
         if(null != paramWhiteBalance) {
            // LEDs are expected to have non pure white color
            // For example they might give 6500K white at [255.0, 255.0, 255.0]
            def whiteBalance = paramWhiteBalance as int
            params.rgb = changeWhiteBalance(params.rgb, CalibrationWhite(), temperatureColorRGB(whiteBalance as int, 100))
            // changeWhiteBalance(params.rgb, temperatureColorRGB(whiteBalance as int, 100), CalibrationWhite())
        }
        
        def hsv = hubitat.helper.ColorUtils.rgbToHSV(params.rgb)
        logInfo("RGB(${rgb[0]}, ${rgb[1]}, ${rgb[2]}) => HSV(${hsv[0]}, ${hsv[1]}, ${hsv[2]})")
        
        events += [name: "hue",        value: hsv[0].round(0) as int, descriptionText: ""]
        events += [name: "saturation", value: hsv[1].round(0) as int, descriptionText: ""]
        //events += [name: "level", value: hsv[2].round(0) as int, descriptionText: ""]
        
        
        parse(events, endPointDevice)
    }
    
    parseShellyLightStatus(params, endPointDevice)
}

private void parseShellyRGBWStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyRGBWStatus ${params}")
    
    def events = []
    
    if(null != params.rgb) {
        if(null != params.white) { 
        }
        //events << [name: "switch", value: params.output ? "on" : "off", descriptionText: "RGBW dimmer was ${params.output ? "on" : "off"}"]
    }
    
    parse(events, endPointDevice)
    
    parseShellyLightStatus(params, endPointDevice)
}

private void parseShellyScriptStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyScriptStatus ${params}")
    
    if(null != params.running) {
        parse([[name: "switch", value: params.running ? "on" : "off", descriptionText: "Script was ${params.running ? "started" : "stopped"}"]], endPointDevice)
        logInfo("(${endPointDevice}) Script running ${params.running}")
    }    
}

private void parseShellySwitchStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellySwitchStatus ${params}")
    
    if(null != params.output) {        
        parse([[name: "switch", value: params.output ? "on" : "off", descriptionText: "Switch was ${params.output ? "on" : "off"}"]], endPointDevice)
    }
    if(null != params.timer_duration) {
        logInfo("(${endPointDevice}) Timer duration ${params.timer_duration}")
    }
    if(null != params.timer_started_at) {
        logInfo("(${endPointDevice}) Timer started at ${params.timer_started_at}")
    }
    
    parseShellyCommonStatus(params, endPointDevice)
}

private void parseShellyTemperatureSensorStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyTemperatureSensorStatus ${params}")
   
    temperatureScale = getTemperatureScale()
    if(null != params.tF && temperatureScale == "F") {
        genericSensorEvent(params.tF as Float, 0x0101, endPointDevice)        
    }
    else if(null != params.tC && temperatureScale == "C") {
        genericSensorEvent(params.tC as Float, 0x0100, endPointDevice)
    }
}

private void parseShellyThermostatStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyThermostatStatus ${params}")
    //{ output=true, enable=true, schedules={enable=false}, id=0, target_C=23, current_C=18.7 }
    
    def events = []
    
    if(null != params.enable) {        
        events << [name: "thermostatMode", value: params.enable ? "heat" : "off", descriptionText: "Thermostat was ${params.enable ? "on" : "off"}"]
    }
    
    if(null != params.output) {        
        events << [name: "thermostatOperatingState", value: params.output ? "heating" : "idle", descriptionText: "Thermostat state is ${params.output ? "heating" : "idle"}"]
    }
   
    temperatureScale = getTemperatureScale()
    
    if(null != params.target_F && temperatureScale == "F") {
        events << [name: "heatingSetpoint", value: params.target_F as Float, unit: "°F", descriptionText: "Thermostat heating setpoint was set to ${params.target_F}"]
        events << [name: "thermostatSetpoint", value: params.target_F as Float, unit: "°F", descriptionText: "Thermostat setpoint was set to ${params.target_F}"]
    }
    else if(null != params.target_C && temperatureScale == "C") {
        events << [name: "heatingSetpoint", value: params.target_C as Float, unit: "°C", descriptionText: "Thermostat heating setpoint was set to ${params.target_C}"]
        events << [name: "thermostatSetpoint", value: params.target_C as Float, unit: "°C", descriptionText: "Thermostat setpoint was set to ${params.target_C}"]
    }
    
    if(null != params.current_F && temperatureScale == "F") {
        genericSensorEvent(params.current_F as Float, 0x0101, endPointDevice)        
    }
    else if(null != params.current_C && temperatureScale == "C") {
        genericSensorEvent(params.current_C as Float, 0x0100, endPointDevice)
    }
    
    parse(events, endPointDevice)
}

private void parseShellyVoltageSensorStatus(params, endPointDevice) {
    logDebug("(${endPointDevice}) parseShellyVoltageSensorStatus ${params}")
   
    if(null != params.xvoltage) {
        genericSensorEvent(params.xvoltage as Float, 0x0F00, endPointDevice)
    }
    else if(null != params.voltage) {
        genericSensorEvent(params.voltage as Float, 0x0F00, endPointDevice)
    }
}

private void parseShellyWifiStatus(params) {
    logDebug("parseShellyWifiStatus ${params}")

    // [rssi:-74, sta_ip:192.168.0.105, ssid:ORBI56, status:got ip]
    
    def events = []

    if(null != params.rssi) {
        // dBm to percentage
        // A value of 0 implies an actual RSSI signal strength of -100 dbm. A value of 100 implies an actual RSSI signal strength of -50 dbm
        int rssi = Math.min(Math.max(2 * ((params.rssi as int) + 100), 0), 100)
        events << [name: "rssi", value: rssi, unit: "%"]
    }

    if(null != params.sta_ip) {
        events << [name: "wifi", value: params.sta_ip as String]
    }
    else {
        events << [name: "wifi", value: "offline"]
    }
    
    parse(events)
}

private void parseShellyConfig(params) {
    logDebug("parseShellyConfig ${params}")
    
    def components = [:]
    
    params?.each({
        def componentEntry = it.key.split(':')
        String componentType = componentEntry.first().trim()
        def componentInstanceIndex = ((componentEntry.size() > 1) ? componentEntry.last().trim() as int : 0) + 1
       
        if(it.value instanceof Map) {            
            if(componentType == "ble" ) {
            }
            else if(componentType == "cloud" ) {
            }
            else if(componentType == "cover") {
            }
            else if(componentType == "devicepower") {
            }
            else if(componentType == "em") {
            }
            else if(componentType == "emdata") {
            }
            else if(componentType == "em1") {
            }
            else if(componentType == "em1data") {
            }
            else if (componentType == "eth") {
            }
            else if (componentType == "humidity") {
            }
            else if(componentType == "illuminance") {
            }
            else if(componentType == "input") {
            }
            else if(componentType == "light") {
            }
            else if(componentType == "rgb") {
            }
            else if(componentType == "rgbw") {
            }
            else if(componentType == "modbus" ) {
            }
            else if(componentType == "mqtt" ) {
            }
            else if(componentType == "script") {
                componentInstanceIndex -= 1
                
                def childDevice = getScriptDevice(componentInstanceIndex as Short)
                if(null != childDevice) {
                    childDevice.setLabel(device.label + " script: " + it.value.name)
                }
            }
            else if(componentType == "switch") {
            }
            else if (componentType == "sys"|| componentType == "Shelly") {
                if(null != it.value?.device?.sys_btn_toggle) {
                    components["sysbtn"] = [1]
                }
            }
            else if(componentType == "temperature") {
            }
            else if(componentType == "thermostat") {
            }
            else if(componentType == "voltmeter") {
            }
            else if(componentType == "wifi" ) {
            }
            else if(componentType == "bthome" ) {
            }
            else if(componentType == "knx" ) {
            }
            else if(componentType == "ws" ) {
            }
            else {
                logWarn("Unknown component config ${it}")
            }
            
            if(components[componentType] == null) {
                components[componentType] = []
            }
            
            components[componentType] << componentInstanceIndex
        }
        else if (componentType != "ts") {
            logWarn("Unknown component config ${it}")
        }
    })
    
    updateDataValue("components",  components as String)
}

private void parseShellyStatus(params) {
    logDebug("parseShellyStatus ${params}")
      
    params?.each({
        String componentType = it.key.split(':').first().trim()
        
        if(it.value instanceof Map) {
            it.value.ts = params.ts
            
            if(componentType == "ble" ) {
                // The status of the BLE component contains information about the bluetooth on/off state and does not own any status properties.
            }
            else if(componentType == "cloud" ) {
                parseShellyCloudStatus(it.value)
            }
            else if(componentType == "cover") {
                def device = getCoverDevice((it.value.id + 1) as Short)
                parseShellyCoverStatus(it.value, device)
            }
            else if(componentType == "devicepower") {
                def device = getBatteryDevice((it.value.id + 1) as Short)
                parseShellyDevicePowerStatus(it.value, device)
            }
            else if(componentType == "em") {
                def devices = getMeterDevices((it.value.id + 1) as Short)
                parseShellyEnergyMeterStatus(it.value, devices)
            }
            else if(componentType == "emdata") {
                def devices = getMeterDevices((it.value.id + 1) as Short)
                parseShellyEnergyMeterDataStatus(it.value, devices)
            }
            else if(componentType == "em1") {
                def device = getMeterDevice((it.value.id + 1) as Short)
                parseShellyEnergyMeterStatus1(it.value, device)
            }
            else if(componentType == "em1data") {
                def device = getMeterDevice((it.value.id + 1) as Short)
                parseShellyEnergyMeterDataStatus1(it.value, device)
            }
            else if (componentType == "eth") {
                parseShellyEthernetStatus(it.value)
            }
            else if (componentType == "humidity") {
                def device = getHumiditySensorDevice((it.value.id + 1) as Short)
                parseShellyHumiditySensorStatus(it.value, device)
            }
            else if(componentType == "illuminance") {
                def device = getIlluminanceSensorDevice((it.value.id + 1) as Short)
                parseShellyIlluminanceSensorStatus(it.value, device)
            }
            else if(componentType == "input") {
                def device = getInputDevice((it.value.id + 1) as Short)
                parseShellyInputStatus(it.value, device)
            }
            else if(componentType == "light") {
                Short endPoint = (it.value.id + 1) as Short
                def device = getDimmerDevice(endPoint)
                parseShellyLightStatus(it.value, device)
                
                if(null != it.value.temperature) {
                    def tDevice = getTemperatureSensorDevice("light" + endPoint)
                    parseShellyTemperatureSensorStatus(it.value.temperature, tDevice)
                }
            }
            else if(componentType == "rgb") {
                Short endPoint = (it.value.id + 1) as Short
                def device = getRGBDevice(endPoint)
                parseShellyRGBStatus(it.value, device)
                
                if(null != it.value.temperature) {
                    def tDevice = getTemperatureSensorDevice("rgb" + endPoint)
                    parseShellyTemperatureSensorStatus(it.value.temperature, tDevice)
                }
            }
            else if(componentType == "rgbw") {
                Short endPoint = (it.value.id + 1) as Short
                def device = getRGBWDevice(endPoint)
                parseShellyRGBWStatus(it.value, device)
                
                if(null != it.value.temperature) {
                    def tDevice = getTemperatureSensorDevice("rgbw" + endPoint)
                    parseShellyTemperatureSensorStatus(it.value.temperature, tDevice)
                }
            }
            else if(componentType == "modbus" ) {
                if(null != it.value.connected) {
                    parse([[name: "modbus", value: it.value.connected ? "online" : "offline"]])
                }
            }
            else if(componentType == "mqtt" ) {
                if(null != it.value.connected) {
                    parse([[name: "mqtt", value: it.value.connected ? "online" : "offline"]])
                }
            }
            else if(componentType == "pm1") {
                def device = getPowerMeterDevice(it.value.id + 1 as Short)
                parseShellyCommonStatus(it.value, device)
            }
            else if(componentType == "script") {
                def device = getScriptDevice(it.value.id as Short)
                parseShellyScriptStatus(it.value, device)
            }
            else if(componentType == "switch") {
                Short endPoint = (it.value.id + 1) as Short
                def device = getSwitchDevice(endPoint)
                parseShellySwitchStatus(it.value, device)
            
                if(null != it.value.temperature) {
                    def tDevice = getTemperatureSensorDevice("switch" + endPoint)
                    parseShellyTemperatureSensorStatus(it.value.temperature, tDevice)
                }
            }
            else if (componentType == "sys"|| componentType == "Shelly") {
                if (null != it.value.cfg_rev) {
                    logDebug("Configuration revision ${it.value.cfg_rev}")
                }
            }
            else if(componentType == "temperature") {
                def device = getTemperatureSensorDevice((it.value.id + 1) as Short)
                parseShellyTemperatureSensorStatus(it.value, device)
            }
            else if(componentType == "thermostat") {
                def device = getThermostatDevice((it.value.id + 1) as Short)
                parseShellyThermostatStatus(it.value, device)
            }
            else if(componentType == "voltmeter") {
                def device = getVoltageSensorDevice((it.value.id + 1) as Short)
                parseShellyVoltageSensorStatus(it.value, device)
            }
            else if(componentType == "wifi" ) {
                parseShellyWifiStatus(it.value)            
            }
            else if(componentType == "bthome" ) {
                //
            }
            else if(componentType == "knx" ) {
                //
            }
            else if(componentType == "ws" ) {
                //
            }
            else {
                logWarn("Unknown NotifyStatus ${it}")
            }
        }
        else if (componentType != "ts") {
            logWarn("Unknown NotifyStatus ${it}")
        }
    })
}

//======================================================================================================================

private void parseShellyResponse(int command, params) {
    switch(command) {
        case CmdCloudGetStatus():
            parseShellyCloudStatus(params)
            return
        case CmdCoverGetStatus():
            def device = getCoverDevice((params.id + 1) as Short)
            parseShellyCoverStatus(params, device)
            return
        case CmdDevicePowerGetStatus():
            def device = getBatteryDevice((params.id + 1) as Short)
            parseShellyDevicePowerStatus(params, device)
            return
        case CmdEMGetStatus():
            def devices = getMeterDevices((params.id + 1) as Short)
            parseShellyEnergyMeterStatus(params, devices)
            return
        case CmdEMDataGetStatus():
            def devices = getMeterDevices((params.id + 1) as Short)
            parseShellyEnergyMeterDataStatus(params, devices)
            return
        case CmdEM1GetStatus():
            def device = getMeterDevice((params.id + 1) as Short)
            parseShellyEnergyMeterStatus1(params, device)
            return
        case CmdEM1DataGetStatus():
            def device = getMeterDevice((params.id + 1) as Short)
            parseShellyEnergyMeterDataStatus1(params, device)
            return
        case CmdEthGetStatus():
            parseShellyEthernetStatus(params)
            return
        case CmdHumidityGetStatus():
            def device = getHumiditySensorDevice((params.id + 1) as Short)
            parseShellyHumiditySensorStatus(params, device)
            return
        case CmdIlluminanceGetStatus():
            def device = getIlluminanceSensorDevice((params.id + 1) as Short)
            parseShellyIlluminanceSensorStatus(params, device)
            return
        case CmdInputGetStatus():
            def device = getInputDevice((params.id + 1) as Short)
            parseShellyInputStatus(params, device)
            return
        case CmdLightGetStatus():
            def device = getDimmerDevice((params.id + 1) as Short)
            parseShellyLightStatus(params, device)
        
            if(null != params.temperature) {
                def tDevice = getTemperatureSensorDevice("light" + endPoint)
                parseShellyTemperatureSensorStatus(params.temperature, tDevice)
            }
            return
        case CmdRGBGetStatus():
            def device = getRGBDevice((params.id + 1) as Short)
            parseShellyRGBStatus(params, device)
        
            if(null != params.temperature) {
                def tDevice = getTemperatureSensorDevice("rgb" + endPoint)
                parseShellyTemperatureSensorStatus(params.temperature, tDevice)
            }
            return
        case CmdRGBWGetStatus():
            def device = getRGBWDevice((params.id + 1) as Short)
            parseShellyRGBWStatus(params, device)
        
            if(null != params.temperature) {
                def tDevice = getTemperatureSensorDevice("rgbw" + endPoint)
                parseShellyTemperatureSensorStatus(params.temperature, tDevice)
            }
            return
        case CmdShellyCheckForUpdate():
            logInfo("Device updates ${params}")
            
            // [stable:[version:0.14.1, build_id:20230308-091222/0.14.1-g22a4cb7]]
            def fwUpdateList = [:]
        
            if(null != params.stable) {
                fwUpdateList.stable = params.stable.version
            }
            if(null != params.beta) {
                fwUpdateList.beta = params.beta.version
            }
            
            if(!fwUpdateList.isEmpty()) {
                parse([[name: "fwUpdate", value: fwUpdateList]])
            }
            else {
                parse([[name: "fwUpdate", value: "none"]])
            }
            return
        case CmdShellyGetDeviceInfo():
            logInfo("Device info response ${params}")

            updateDataValue("auth",      params.auth_en ? "yes" : "no")
            updateDataValue("MAC",       params.mac)
            updateDataValue("type",      params.app)
            genericVersionEvent(params.ver, params.gen.toString(), params.model)
            return
        case CmdShellyGetStatus():
            parseShellyStatus(params)
            return
        case CmdShellyGetConfig():
            parseShellyConfig(params)
            return
        case CmdShellyListProfiles():
            logInfo("Device profiles ${params.profiles}")
            return
        case CmdShellyReboot():
            logInfo("Reboot response ${params}")
            return
        case CmdShellyUpdate():
            logInfo("Update response ${params}")
            return
        case CmdPM1GetStatus():
            def device = getPowerMeterDevice((params.id + 1) as Short)
            parseShellyCommonStatus(params, device)
            return
        case CmdScriptGetStatus():
            def device = getScriptDevice(params.id as Short)
            parseShellyScriptStatus(params, device)
            return
        case CmdScriptStart():
            // [was_running : false/true]
            logInfo("Script start response ${params}")
            return
        case CmdScriptStop():
            // [was_running : false/true]
            logInfo("Script stop response ${params}")
            return
        case CmdSwitchGetStatus():
            Short endPoint = (params.id + 1) as Short
            def device = getSwitchDevice(endPoint)
            parseShellySwitchStatus(params, device)

            if(null != params.temperature) {
                def tDevice = getTemperatureSensorDevice("switch" + endPoint)
                parseShellyTemperatureSensorStatus(params.temperature, tDevice)
            }
            return
        case CmdSwitchSet():
            // [was_on : false/true]
            return
        case CmdTemperatureGetStatus():
            def device = getTemperatureSensorDevice((params.id + 1) as Short)
            parseShellyTemperatureSensorStatus(params, device)
            return
        case CmdThermostatGetStatus():
            def device = getThermostatDevice((params.id + 1) as Short)
            parseShellyThermostatStatus(params, device)
            return
        case CmdThermostatSetConfig():
            logInfo("Thermostat set config response ${params}")
            return
        case CmdVoltmeterGetStatus():
            def device = getVoltageSensorDevice((params.id + 1) as Short)
            parseShellyVoltageSensorStatus(params, device)
            return
        case CmdWifiGetStatus():
            parseShellyWifiStatus(params)
            return
        default:
            logWarn("Unknown response ${params}")
            return
    }
}

//======================================================================================================================
// WS responses and notifications come here
void parse(String message) {
    def notification = parseJson(message)

    if(notification.method == "NotifyEvent") {
        parseShellyEvents(notification.params)
    }    
    else if(notification.method == "NotifyStatus") {
        parseShellyStatus(notification.params)
    }
    else if(null != notification.result) {        
        parseShellyResponse(notification.id, notification.result)
    }
    else if(null != notification.error) {
        if(null != notification.error.code) {
            switch(notification.error.code as int) {
                case 401:
                    // While WS will not accept commands it will still get events.
                    // So keeping WS in 'online' state but cooking credentials frame for the all subsequent WS commands
                    if(null != state.wsChallenge) {
                        logError("Unauthorized WS query. Please, check your password.")
                    }
                    else {
                        logInfo("WS authtentification sequence... ")
                    }
                
                    if(null != notification.error.message) {
                        state.wsChallenge = notification.error.message.replace("{", "").replace("}", "").split(",").collectEntries({ 
                            def entry = it.replace("\"", "").split(":")
                            entry.length > 1 
                                ? [(entry[0].trim()) : entry[1].trim()] 
                                : [authorization : entry[0].trim()]
                        })
                        logDebug(state.wsChallenge)
                    }                
                    return
                default:
                    break
            }
        }
        
        logWarn("Unknown WS error ${notification}")
        state.wsChallenge = null
    }
    else {
        logWarn("Unknown WS message ${notification}")
    }
}

private void checkHealthStatusCallback() {
    def linkStatus = device.currentValue("link")?.toString()
    
    //if(linkStatus != "online")
    //{
        parse([[name: "healthStatus", value: linkStatus, descriptionText: "Connection down."]])
        getChildDevices().each({ it -> if(it.hasAttribute("healthStatus")) it.parse([[name: "healthStatus", value: linkStatus, descriptionText: "Connection down."]]) })
    //}
}

private void checkHealthStatus() {
    if(null != alarmIn) {
        runIn(alarmIn, "checkHealthStatusCallback", [overwrite: false])
    }
}

private void cancelHealthStatusCheck(status) {
    unschedule("checkHealthStatusCallback")
    parse([[name: "healthStatus", value: status.value, descriptionText: status.descriptionText]])
    getChildDevices().each({ it -> if(it.hasAttribute("healthStatus")) it.parse([[name: "healthStatus", value: status.value, descriptionText: status.descriptionText]]) })
}

private void keepAlive() {
    // restore connection in 30s
    if(null == retryIn) {
        retryIn = 30
    }
        
    if(null != state.wsKeepAlive) {
        logWarn("Reconnecting in ${retryIn} seconds.")
        runIn(retryIn, "connect")
        checkHealthStatus()
    }
    else {
        cancelHealthStatusCheck([value: "offline", descriptionText: "Connection closed."])
    }
}

// WS stream status comes here
void webSocketStatus(String message) {
    logDebug("webSocketStatus ${message}; keepAlive=${state.wsKeepAlive}")
    
    def wsMessage = message.split(',').collectEntries { entry -> 
        def pair = entry.split(':')
        [(pair.first().trim()) : pair.last().trim()]
    }
    
    if(wsMessage.containsKey("status")) {
        if(wsMessage.status == "open") {
            cancelHealthStatusCheck([value: "online", descriptionText: "Connection open."])
            
            parse([
                [name: "link", value: "online", descriptionText: "Connection open."]
            ])
            
            // When socket open, send any valid request to initiate device events
            // Poll websocket (only once is needed to initiate all the events/notifications from device)
            getChildDevices().eachWithIndex({ it, channelIdx -> if(channelIdx == 0) it.refresh() })
            
            // Update driver state in case any state changes were missed 
            refresh()
        }
        else if(wsMessage.status == "closing") {
            parse([
                [name: "link", value: "offline", descriptionText: "Connection closed."]
            ])
            
            keepAlive()
        }
    }
    else if(wsMessage.containsKey("failure")) {
        logWarn("Connection failure: ${wsMessage.failure}")
        
        parse([
            [name: "link", value: "offline", descriptionText: "Connection failure: ${wsMessage.failure}"]
        ])
        
        keepAlive()
    }
}