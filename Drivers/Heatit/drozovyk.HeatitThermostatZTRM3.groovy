/**/

#include drozovyk.association1
#include drozovyk.common1
#include drozovyk.encapsulation1
#include drozovyk.meter1
#include drozovyk.sensor1
#include drozovyk.version1
#include drozovyk.zwave1

import groovy.transform.Field

// Sensor mode
@Field static List<String> param2options = ["Floor only","Air only","Air+Floor","External only", "External+Floor"]
// Sensor type
@Field static List<String> param3options = ["10K NTC","12K NTC","15K NTC","22K NTC","33K NTC","47K NTC"]
// Display mode
@Field static List<String> param13options = ["Setpoint temperature", "Sensor temperature"]

@Field static parameterDescription = [
    (  2): [title: "Sensor mode",
            help:  "",
            size:  2,
            state: "Sensor mode: ",
            style: "color:red; font-weight:bold;"],
    (  3): [title: "Sensor type",
            help:  "Floor and external",
            size:  1,
            state: "Sensor type: ",
            style: "color:red; font-weight:bold;"],
    (  4): [title: "Temperature control hysteresis",
            help:  "",
            scale: 10,
            size:  1,
            state: "Temperature control hysteresis: ",
            style: "color:darkred; font-weight:bold;"],
    (  5): [title: "Floor minimum temperature",
            help: "",
            scale: 10,
            size:  2,
            state: "Floor minimum temperature: ",
            style: "color:slateblue; font-weight:bold;"],
    (  6): [title: "Floor maximum temperature",
            help: "",
            scale: 10,
            size:  2,
            state: "Floor maximum temperature: ",
            style: "color:orangered; font-weight:bold;"],
    (  7): [title: "Air minimum temperature",
            help: "",
            scale: 10,
            size:  2,
            state: "Air minimum temperature: ",
            style: "color:slateblue; font-weight:bold;"],
    (  8): [title: "Air maximum temperature",
            help: "",
            scale: 10,
            size:  2,
            state: "Air maximum temperature: ",
            style: "color:orangered; font-weight:bold;"],    
    ( 10): [title: "Room sensor calibration",
            help: "",
            scale: 10,
            size:  1,
            state: "Room sensor calibration: ",
            style: "color:mediumorchid; font-weight:bold;"],
    ( 11): [title: "Floor sensor calibration",
            help: "",
            scale: 10,
            size:  1,
            state: "Floor sensor calibration: ",
            style: "color:mediumorchid; font-weight:bold;"],
    ( 12): [title: "External sensor calibration",
            help: "",
            scale: 10,
            size:  1,
            state: "External sensor calibration: ",
            style: "color:mediumorchid; font-weight:bold;"],
    ( 13): [title: "Temperature display",
            help: "",
            size:  1,
            state: "Temperature display: ",
            style: "font-weight:bold;"],
    ( 14): [title: "Button brightness dimmed state (%)",
            help: "",
            scale: 1,
            size:  1,
            state: "Button brightness dimmed state (%): ",
            style: "font-weight:bold;"],
    ( 15): [title: "Button brightness active state (%)",
            help: "",
            scale: 1,
            size:  1,
            state: "Button brightness active state (%): ",
            style: "font-weight:bold;"],
    ( 16): [title: "Display brightness dimmed state (%)",
            help: "",
            scale: 1,
            size:  1,
            state: "Display brightness dimmed state (%): ",
            style: "font-weight:bold;"],
    ( 17): [title: "Display brightness active state (%)",
            help: "",
            scale: 1,
            state: "Display brightness active state (%): ",
            style: "font-weight:bold;"],
    ( 18): [title: "Temp. report interval (S)",
            help: "",
            scale: 1,
            size:  2,
            state: "Temp. report interval (S): ",
            style: "color:green; font-weight:bold;"],
    ( 19): [title: "Temp. report hysteresis",
            help: "",
            scale: 10,
            size:  1,
            state: "Temp. report hysteresis: ",
            style: "color:green; font-weight:bold;"],
    ( 20): [title: "Meter report interval (S)",
            help: "",
            scale: 1,
            size:  2,
            state: "Meter report interval (S): ",
            style: "color:green; font-weight:bold;"],
    ( 21): [title: "Meter report data value (KWh)",
            help: "Delta value between consecutive meter reports.",
            scale: 10,
            size:  1,
            state: "Meter report data value (KWh): ",
            style: "color:green; font-weight:bold;"]
]

static def getParameterDesc(int parameter) {
    def parameterDesc = parameterDescription[parameter]
    
    if(null == parameterDesc) {
        return [title: "", help: "", state: "", style: ""]
    }
    
    return parameterDesc
}

static String getParameterHelp(int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return parameterDesc.help
}

static String decorateState(String text, int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return styleText(parameterDesc.state + text, parameterDesc.style)
}

static String getParameterTitle(int parameter) {
    def parameterDesc = getParameterDesc(parameter)
    return styleText(parameterDesc.title, parameterDesc.style)
}

static def scaleParameterFromInput(int parameter, def value) {
    def parameterDesc = getParameterDesc(parameter)
    return value * parameterDesc.scale
}

static def scaleParameterToInput(int parameter, def value) {
    def parameterDesc = getParameterDesc(parameter)
    return value / parameterDesc.scale
}

// Association group 1 is reserved for the Z-Wave Plus Lifeline association group. Group 1 MUST NOT be 
// assigned to any other use than the Lifeline group. The actual Device Type specifies a mandatory list of 
// commands which the device MUST send to all targets associated to the Lifeline group. A manufacturer 
// MAY add additional commands to the Lifeline group.
@Field static List<String> nodeGroups = [
    "Relay (Switch Binary)",
    "Internal temperature (Sensor Multilevel)",
    "External temperature (Sensor Multilevel)",
    "Floor temperature (Sensor Multilevel)"
]
@Field static Map<Short, String> associationGroups = [
    (1): "groupLifeline", 
    (2): "groupRelay",
    (3): "groupInt",
    (4): "groupExt",
    (5): "groupFloor"
]

metadata {
    definition (name: "Heatit Thermostat Z-TRM3", namespace: "drozovyk", author: "Dmytro Rozovyk") {
        capability("Actuator")
        capability("Configuration")
        capability("EnergyMeter")           // add 'energy' attribute
        capability("Initialize")            // add 'initialize' command
        capability("PowerMeter")            // add 'power' attribute
        capability("Refresh")
        capability("TemperatureMeasurement")// (duplicate)attribute temperature - NUMBER, unit:°F || °C
        capability("Thermostat")            // attribute coolingSetpoint - NUMBER, unit:°F || °C
                                            // attribute heatingSetpoint - NUMBER, unit:°F || °C
                                            // attribute schedule - JSON_OBJECT (Deprecated)
                                            // attribute supportedThermostatFanModes - JSON_OBJECT
                                            // attribute supportedThermostatModes - JSON_OBJECT
                                            // attribute temperature - NUMBER, unit:°F || °C
                                            // attribute thermostatFanMode - ENUM ["on", "circulate", "auto"]
                                            // attribute thermostatMode - ENUM ["auto", "off", "heat", "emergency heat", "cool"]
                                            // attribute thermostatOperatingState - ENUM ["heating", "pending cool", "pending heat", "vent economizer", "idle", "cooling", "fan only"]
                                            // attribute thermostatSetpoint - NUMBER, unit:°F || °C
        capability("VoltageMeasurement")    // add 'voltage' and 'frequency' attributes
        
        command("resetMeter")
        command("updatePreferencesFromDevice")
        command("updateVersionInfo")
        
        command("zwaveGroupAddNodeToGroup", [[name: "group", description: "", type: "ENUM", constraints: nodeGroups],[name: "node", description: "", type: "NUMBER"],[name: "enpoint", description: "", type: "NUMBER"]])
        command("zwaveGroupRemoveNodeFromGroup", [[name: "group", description: "", type: "ENUM", constraints: nodeGroups],[name: "node", description: "", type: "NUMBER"],[name: "endpoint", description: "", type: "NUMBER"]])
    }
    preferences {
        input(name: "param2", type: "enum", title: getParameterTitle(2), description: getParameterHelp(2), options: param2options, defaultValue: param2options[1], required: false, displayDuringSetup: false)
        input(name: "param3", type: "enum", title: getParameterTitle(3), description: getParameterHelp(3), options: param3options, defaultValue: param3options[0], required: false, displayDuringSetup: false)
        
        input(name: "param4", type: "number", title: getParameterTitle(4), description: getParameterHelp(4), range: "0.3..3.0", defaultValue: "0.5", required: false, displayDuringSetup: false)
        input(name: "param5", type: "number", title: getParameterTitle(5), description: getParameterHelp(5), range: "5..40", defaultValue: "5", required: false, displayDuringSetup: false)
        input(name: "param6", type: "number", title: getParameterTitle(6), description: getParameterHelp(6), range: "5..40", defaultValue: "40", required: false, displayDuringSetup: false)
        input(name: "param7", type: "number", title: getParameterTitle(7), description: getParameterHelp(7), range: "5..40", defaultValue: "5", required: false, displayDuringSetup: false)
        input(name: "param8", type: "number", title: getParameterTitle(8), description: getParameterHelp(8), range: "5..40", defaultValue: "40", required: false, displayDuringSetup: false)        
        input(name: "param10", type: "number", title: getParameterTitle(10), description: getParameterHelp(10), range: "-6.0..6.0", defaultValue: "0", required: false, displayDuringSetup: false)
        input(name: "param11", type: "number", title: getParameterTitle(11), description: getParameterHelp(11), range: "-6.0..6.6", defaultValue: "0", required: false, displayDuringSetup: false)
        input(name: "param12", type: "number", title: getParameterTitle(12), description: getParameterHelp(12), range: "-6.0..6.6", defaultValue: "0", required: false, displayDuringSetup: false)
        
        input(name: "param13", type: "enum", title: getParameterTitle(13), description: getParameterHelp(13), options: param13options, defaultValue: param13options[0], required: false, displayDuringSetup: false)
        
        input(name: "param14", type: "number", title: getParameterTitle(14), description: getParameterHelp(14), range: "0..100", defaultValue: "100", required: false, displayDuringSetup: false)
        input(name: "param15", type: "number", title: getParameterTitle(15), description: getParameterHelp(15), range: "0..100", defaultValue: "100", required: false, displayDuringSetup: false)
        input(name: "param16", type: "number", title: getParameterTitle(16), description: getParameterHelp(16), range: "0..100", defaultValue: "100", required: false, displayDuringSetup: false)
        input(name: "param17", type: "number", title: getParameterTitle(17), description: getParameterHelp(17), range: "0..100", defaultValue: "100", required: false, displayDuringSetup: false)        
        
        input(name: "param18", type: "number", title: getParameterTitle(18), description: getParameterHelp(18), range: "30..32767", defaultValue: "60", required: false, displayDuringSetup: false)
        input(name: "param19", type: "number", title: getParameterTitle(19), description: getParameterHelp(19), range: "0.1..10", defaultValue: "1", required: false, displayDuringSetup: false)
        input(name: "param20", type: "number", title: getParameterTitle(20), description: getParameterHelp(20), range: "30..32767", defaultValue: "90", required: false, displayDuringSetup: false)
        input(name: "param21", type: "number", title: getParameterTitle(21), description: getParameterHelp(21), range: "0..25.5", defaultValue: "1", required: false, displayDuringSetup: false)
        
        inputLogLevel()
    }
}

@Field static List<String> sensorNames = [
    "",
    "",
    "internal sensor",
    "external sensor",
    "floor sensor"
]
String getSensorDeviceLabel(Short sensorNumber)      { return "${device.label} ${sensorNames[sensorNumber]}" }
String getSensorDeviceName(Short sensorNumber)       { return "${device.name}-sensor-${sensorNumber}" }
String getSensorDeviceNetworkId(Short sensorNumber)  { return "${device.deviceNetworkId}-sensor-${sensorNumber}" }
def    getSensorDevice(Short sensorNumber)           { return getChildDevice(getSensorDeviceNetworkId(sensorNumber)) }

void configureChildDevices() {
    getChildDevices().each { deleteChildDevice("${it.deviceNetworkId}") }
    
    for(Short sensorIndex = 2; sensorIndex < 5; ++sensorIndex) {
        sensorDevice = getSensorDevice(sensorIndex)
        if(null == sensorDevice) {            
            sensorDevice = addChildDevice("hubitat", "Generic Component Temperature Sensor", getSensorDeviceNetworkId(sensorIndex),  [isComponent: true, name: getSensorDeviceName(sensorIndex), label: getSensorDeviceLabel(sensorIndex)])
            sensorDevice.updateDataValue("EP", sensorIndex as String)
            sensorDevice.refresh()
            logInfo "Spawned child device for the switch ${sensorIndex}"
        }          
    }    
}

def getEndpointDevice(Short ep = 0) {
    if(ep > 1) {
        return getSensorDevice(ep)
    }
    else {
        return this
    }    
}

// ================  Parameters  =====================
void removeStateHint() {
    state.remove("hint")
}

//=============================================================================================================================================================
// Z-Wave common
//=============================================================================================================================================================

// Basic V2
//    Not used atm

//=============================================================================================================================================================
// Z-Wave device-specific
//=============================================================================================================================================================

// Application status
//    see 'encapsulation1'

// Association grp info
//    Not used atm  

// Association V2
//    see 'association1'

// Configuration V1
void zwaveEvent(hubitat.zwave.commands.configurationv1.ConfigurationReport cmd) {
    def parameterState = [value: cmd.scaledConfigurationValue]
    
    def options = null
    if(cmd.parameterNumber == 2) {
        options = param2options
    } else if(cmd.parameterNumber == 3) {
        options = param3options
    } else if(cmd.parameterNumber == 13) {
        options = param13options
    }    
    
    if(options != null) {
        parameterState.desc = decorateState(options[cmd.scaledConfigurationValue], cmd.parameterNumber)
        device.updateSetting("param${cmd.parameterNumber}" as String, [type: "enum", value: options[cmd.scaledConfigurationValue]])
    }
    else {            
        parameterState.desc = decorateState("${scaleParameterToInput(cmd.parameterNumber, cmd.scaledConfigurationValue)}", cmd.parameterNumber)
        device.updateSetting("param${cmd.parameterNumber}" as String, [type: "number", value: scaleParameterToInput(cmd.parameterNumber, cmd.scaledConfigurationValue)])
    }
    
    state["parameter${cmd.parameterNumber}"] = parameterState
}

String setParameter(paramId) {
    def options = null
    if(paramId == 2) {
        options = param2options
    } else if(paramId == 3) {
        options = param3options
    } else if(paramId == 13) {
        options = param13options
    }
    
    def String cmd = ""
    def inputValue = this.settings["param${paramId}"]
    if(inputValue != null) { 
        if(options != null) {
            def Number value = options.indexOf(inputValue)
            if(state["parameter${paramId}"]?.value != value) {
                def Number size = parameterDescription[paramId].size
                cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
            }
        }
        else {            
            def Number value = scaleParameterFromInput(paramId, inputValue as BigDecimal)
            if(state["parameter${paramId}"]?.value != value) {
                def Number size = parameterDescription[paramId].size
                cmd = encapsulate(zwave.configurationV1.configurationSet(scaledConfigurationValue: value, parameterNumber: paramId, size: size))
            }
        }
    }
    
    return cmd
}

String getParameterReport(paramId) {
    return encapsulate(zwave.configurationV1.configurationGet(parameterNumber: paramId))    
}

// Device reset locally
//     not used atm

// Firmware update md V4
//     not used atm

// Manufacturer specific V2
//     not used atm

// Meter V3
void getMeterReport(int ep = 0) {
    def cmds = delayBetween([
        encapsulate(zwave.meterV3.meterGet(scale: 4), ep as Short),
        encapsulate(zwave.meterV3.meterGet(scale: 2), ep as Short),
        encapsulate(zwave.meterV3.meterGet(scale: 0), ep as Short)
    ], 500)
    sendHubCommand(new hubitat.device.HubMultiAction(cmds, hubitat.device.Protocol.ZWAVE))
}

void resetMeter() {
    runIn(1, refresh)
    String cmd = encapsulate(zwave.meterV3.meterReset())
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))    
}

// Multi channel V4
//    see 'encapsulation1' library

// Power level
//    not used atm (radio power level info)

// Protection V2
//    not used atm

// Security
//    see 'encapsulation1' library

// Security 2
//    not used atm

// Sensor multilevel V5
//    see 'sensor1'
List<String> getSensorMultilevelReportStringList() {
    return [
        // Main endpoint
        encapsulate(zwave.sensorMultilevelV5.sensorMultilevelGet(scale: 1, sensorType: hubitat.zwave.commands.sensormultilevelv5.SensorMultilevelGet.SENSOR_TYPE_TEMPERATURE_VERSION_1))
        // Sensor endpoint
        //encapsulate(zwave.sensorMultilevelV5.sensorMultilevelGet(scale: 1, sensorType: hubitat.zwave.commands.sensormultilevelv5.SensorMultilevelGet.SENSOR_TYPE_TEMPERATURE_VERSION_1), 2)
    ];
}

void getSensorMultilevelReport() {
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(getSensorMultilevelReportStringList(), 500), hubitat.device.Protocol.ZWAVE))    
}

// Supervision
//    see 'encapsulation1'

// Thermostat mode V2 (off:true, heat:true)
void zwaveEvent(hubitat.zwave.commands.thermostatmodev2.ThermostatModeReport cmd, Short ep = 0) {
    if(cmd.mode == hubitat.zwave.commands.thermostatmodev2.ThermostatModeReport.MODE_OFF) {
        parse([[name:"thermostatMode", value: "off"]])
    } else if(cmd.mode == hubitat.zwave.commands.thermostatmodev2.ThermostatModeReport.MODE_HEAT) {
        parse([[name:"thermostatMode", value: "heat"]])
    } 
    
    runIn(4, getMeterReport)
}

String getThermostatModeReportString() {
    return encapsulate(zwave.thermostatModeV2.thermostatModeGet())
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

void getThermostatModeReport() {
    String cmd = getThermostatModeReportString()
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

void setThermostatMode(Short state) {
    // approximating to 'basic' command class
    
    Short mode = (state == 0) ? hubitat.zwave.commands.thermostatmodev2.ThermostatModeSet.MODE_OFF : hubitat.zwave.commands.thermostatmodev2.ThermostatModeSet.MODE_HEAT

    String cmd = encapsulate(zwave.thermostatModeV2.thermostatModeSet(mode: mode))
    sendHubCommand(new hubitat.device.HubAction(cmd, hubitat.device.Protocol.ZWAVE))
}

// Thermostat operating state V1
void zwaveEvent(hubitat.zwave.commands.thermostatoperatingstatev1.ThermostatOperatingStateReport cmd, Short ep = 0) {
    if(cmd.operatingState == hubitat.zwave.commands.thermostatoperatingstatev1.ThermostatOperatingStateReport.OPERATING_STATE_IDLE) {
        parse([[name:"thermostatOperatingState", value: "idle"]])
    } else if(cmd.operatingState == hubitat.zwave.commands.thermostatoperatingstatev1.ThermostatOperatingStateReport.OPERATING_STATE_HEATING) {
        parse([[name:"thermostatOperatingState", value: "heating"]])
    } 
}
String getThermostatOperatingStateReportString() {
    return encapsulate(zwave.thermostatOperatingStateV1.thermostatOperatingStateGet())
}

// Thermostat setpoint V3
//    In fact while device accepts V3 commands it somehow replies with V2 report. So there is no point on using V3 command set
//    Only heating setpoint is supported
void zwaveEvent(hubitat.zwave.commands.thermostatsetpointv2.ThermostatSetpointReport cmd, Short ep = 0) {
    parse([
        [name: "heatingSetpoint", value: "${cmd.scaledValue}", unit: "°C"],
        [name: "thermostatSetpoint", value: "${cmd.scaledValue}", unit: "°C"]
    ])
}

String getThermostatSetpointReportString() {
    return encapsulate(zwave.thermostatSetpointV2.thermostatSetpointGet(setpointType: hubitat.zwave.commands.thermostatsetpointv2.ThermostatSetpointGet.SETPOINT_TYPE_HEATING_1))
}

void getThermostatSetpointReport() {
    sendHubCommand(new hubitat.device.HubAction(getThermostatSetpointReportString(), hubitat.device.Protocol.ZWAVE))
}
              
void setThermostatSetpoint(BigDecimal value) {
    // The actual range is [5..40]
    BigDecimal temperature = value
    if(temperature < 5) {
        temperature = 5
    }
    if(temperature > 40) {
        temperature = 40
    }
    
    def cmds = []
    cmds << encapsulate(zwave.thermostatSetpointV3.thermostatSetpointSet(setpointType: hubitat.zwave.commands.thermostatsetpointv3.ThermostatSetpointGet.SETPOINT_TYPE_HEATING_1, scaledValue: temperature))
    cmds << getThermostatSetpointReportString() 
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(cmds, 500), hubitat.device.Protocol.ZWAVE))
}

// Transport service V2

// Version V2
//    see 'version1'
void updateVersionInfo() {
    sendHubCommand(new hubitat.device.HubAction(getVersionReportCommand(), hubitat.device.Protocol.ZWAVE))
}

// Zwaveplus info V2
//    not used atm

void zwaveGroupAddNodeToGroup(String enumValue, def nodeId, def endpointId) {
    int groupNumber = (nodeGroups.indexOf(enumValue) as int) + 2 // offset by one (groups start from '1') and skip 'Lifeline' group
    if(null == endpointId) {
        addDeviceNodeToGroup(groupNumber, nodeId)
    }
    else {
        addDeviceEndpointToGroup(groupNumber, nodeId, endpointId)
    }
}

void zwaveGroupRemoveNodeFromGroup(String enumValue, def nodeId, def endpointId) {
    int effectnumber = (nodeGroups.indexOf(enumValue) as int) + 2 // offset by one (groups start from '1') and skip 'Lifeline' group
    if(null == endpointId) {
        removeDeviceNodeFromGroup(groupNumber, nodeId)
    }
    else {
        removeDeviceEndpointFromGroup(groupNumber, nodeId, endpointId)
    }
}

void updatePreferencesFromDevice() {
    def configurationCommands = []
    
    for ( entry in parameterDescription ) {
        configurationCommands << getParameterReport(entry.key) 
    }
    
    configurationCommands.addAll(getDeviceGroupNodesCommandList())
    
    state.clear()
    state.hint = "<b style='color:red;'>Not all parameters are reported. Please, refresh this page in a few seconds</b>"
    runIn(20, removeStateHint)    
    
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(configurationCommands, 500), hubitat.device.Protocol.ZWAVE))
}

//=============================================================================================================================================================
// Own interface
//=============================================================================================================================================================

void configure() {
    parse([
        [name: "supportedThermostatModes",    value: ["heat", "off"], descriptionText: "Initial value"],
        // no fan; so only auto
        [name: "supportedThermostatFanModes", value: ["auto"],        descriptionText: "Initial value"],
        [name: "thermostatFanMode",           value: "auto",          descriptionText: "Initial value"],
        
        [name: "energy",                      value: "0",             descriptionText: "Initial value"],
        [name: "power",                       value: "0",             descriptionText: "Initial value"],
        [name: "voltage",                     value: "0",             descriptionText: "Initial value"],        
        // other attributes
        [name: "heatingSetpoint",             value: "unknown",       descriptionText: "Initial value"],
        [name: "temperature",                 value: "unknown",       descriptionText: "Initial value"],
        [name: "thermostatMode",              value: "unknown",       descriptionText: "Initial value"],
        [name: "thermostatOperatingState",    value: "unknown",       descriptionText: "Initial value"],
        [name: "thermostatSetpoint",          value: "unknown",       descriptionText: "Initial value"]
    ])
    
    configureChildDevices()
    
    updatePreferencesFromDevice()
    updateVersionInfo()
}

void initialize() {    
    refresh()
}

def installed() {
    configure()
}

void refresh() {
    def commands = []
    commands << getThermostatModeReportString()
    commands << getThermostatOperatingStateReportString()
    commands << getThermostatSetpointReportString()
    commands.addAll(getSensorMultilevelReportStringList())
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(commands, 500), hubitat.device.Protocol.ZWAVE))
}

void off() {
    setThermostatMode(0 as Short)    
}

void heat() {
    setThermostatMode(1 as Short)
}

void auto() {
    heat()
}

void setHeatingSetpoint(temperature) {
    setThermostatSetpoint(temperature as BigDecimal)
}

void setThermostatMode(String mode) {
    if(mode == "heat") {
        heat()
    }
    else if(mode == "off") {
        off()
    }        
}

void updated() {
    // Send all parameters one by one
    def configurationCommands = []
    
    for ( entry in parameterDescription ) {
        configurationCommands << setParameter(entry.key)
    }
    
    configurationCommands.removeAll("")
            
    for ( entry in parameterDescription ) {
        configurationCommands << getParameterReport(entry.key) 
    }
    
    configurationCommands.addAll(getDeviceGroupNodesCommandList())
    
    state.clear()
    state.hint = "<b style='color:red;'>Not all parameters are reported. Please, refresh this page in a few seconds</b>"
    runIn(30, removeStateHint)
    
    sendHubCommand(new hubitat.device.HubMultiAction(delayBetween(configurationCommands, 1500), hubitat.device.Protocol.ZWAVE))
}

//=============================================================================================================================================================
// Component devices interface
//=============================================================================================================================================================

void componentParse(cd, List<Map> events) {
    Short endPoint = cd.getDataValue("EP") as Short

    if(null != endPoint) {
        // modes ["Floor only","Air only","Air+Floor","External only", "External+Floor"]
        // endpoints ["", "", "Air", "External", "Floor"]
        
        def Short    mainEndPoint = 2
        def Number   sensorMode = param2options.indexOf(param2)
        if(sensorMode < 1) {
            mainEndPoint = 4
        }
        else if(sensorMode > 2) {
            mainEndPoint = 3
        }
        
        if(endPoint == mainEndPoint) {
            logInfo("Received 'parse' request from ${cd.displayName} (EP${endPoint})")

            events.each {
                if (it.descriptionText) {
                    logInfo(it.descriptionText)
                }
                sendEvent(it)        
            }
        }
    }
}

void componentRefresh(cd) {
    Short endPoint = cd.getDataValue("EP") as Short
    if(null != endPoint) {
        logInfo("Received 'refresh' request from ${cd.displayName} (EP${endPoint})")
        
        def String command = encapsulate(zwave.sensorMultilevelV5.sensorMultilevelGet(scale: 0, sensorType: hubitat.zwave.commands.sensormultilevelv5.SensorMultilevelGet.SENSOR_TYPE_TEMPERATURE_VERSION_1), endPoint)
        sendHubCommand(new hubitat.device.HubAction(command, hubitat.device.Protocol.ZWAVE))
    }
}
